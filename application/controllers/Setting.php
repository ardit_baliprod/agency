<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
	{ 
		parent::__construct();
		$this->load->model('M_setting');
		$this->load->model('M_country');

    }

    
	public function index()
	{
		if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            //$data['type']	= "create";
			$data['setting'] = $this->M_setting->GetSetting()->result();
			$data['country'] = $this->M_country->GetCountry()->result();
           
            $this->session->set_userdata('page', 'Setting');
            $this->template->load('agency-admin/static','agency-admin/setting/setting',$data);
			

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function system()
	{
		if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
			$sitename		=strip_tags(addslashes($this->input->post('sitename',TRUE)));
			$companyname	=strip_tags(addslashes($this->input->post('companyname',TRUE)));
			$address		=strip_tags(addslashes($this->input->post('address',TRUE)));
			$zipcode		=strip_tags(addslashes($this->input->post('zipcode',TRUE)));
			$phone			=strip_tags(addslashes($this->input->post('phone',TRUE)));
			$email			=strip_tags(addslashes($this->input->post('email',TRUE)));
			$website		=strip_tags(addslashes($this->input->post('website',TRUE)));
			$textwebsite	=strip_tags(addslashes($this->input->post('textwebsite',TRUE)));
			$facebook		=strip_tags(addslashes($this->input->post('facebook',TRUE)));
			$ig				=strip_tags(addslashes($this->input->post('ig',TRUE)));
			$youtube		=strip_tags(addslashes($this->input->post('youtube',TRUE)));
			$twitter		=strip_tags(addslashes($this->input->post('twitter',TRUE)));
			$country 		=$this->input->post('country');
			

			$this->M_setting->update_setting($sitename,$companyname,$address,$zipcode,$phone,$email,$website,$textwebsite,$facebook,$ig,$youtube,$country,$twitter);
			$this->session->set_flashdata('sukses','setiing has been updated!!!.');
			$direct = base_url().'Setting';
			redirect($direct);
		}
		else{
			redirect(base_url('AdminLogin'));
		}
	}
	public function email()
	{
		if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
			$stmphost		=strip_tags(addslashes($this->input->post('stmphost',TRUE)));
			$stmpauth		=strip_tags(addslashes($this->input->post('stmpauth',TRUE)));
			$username		=strip_tags(addslashes($this->input->post('username',TRUE)));
			$password		=strip_tags(addslashes($this->input->post('password',TRUE)));
			$stmpsecure		=strip_tags(addslashes($this->input->post('stmpsecure',TRUE)));
			$stmpport		=strip_tags(addslashes($this->input->post('stmpport',TRUE)));
			$emailfrom		=strip_tags(addslashes($this->input->post('emailfrom',TRUE)));
			$emailreply		=strip_tags(addslashes($this->input->post('emailreply',TRUE)));
			$sendgrid		=strip_tags(addslashes($this->input->post('sendgrid',TRUE)));
			

			$this->M_setting->update_email($stmphost,$stmpauth,$username,$password,$stmpsecure,$stmpport,$emailfrom,$emailreply,$sendgrid);
			$this->session->set_flashdata('sukses','setiing has been updated!!!.');
			$direct = base_url().'Setting';
			redirect($direct);
		}
		else{
			redirect(base_url('AdminLogin'));
		}
	}
    
}
