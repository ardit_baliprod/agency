<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compelxion extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_compelxion');
	
	}
	public function index()
	{
		if($this->session->userdata('bma-agc')){
			$data['compelxion'] = $this->M_compelxion->GetCompelxion()->result();
			$this->session->set_userdata('page', 'Complexion');
			$this->template->load('agency-admin/static','agency-admin/compelxion/data-compelxion',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
    }
    public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "create";
            $this->session->set_userdata('page', 'Create Complexion');
			$this->template->load('agency-admin/static','agency-admin/compelxion/form-compelxion',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            
			$this->M_compelxion->create_compelxion($name);
            $this->session->set_flashdata('sukses','Complexion has been created!!!.');
            redirect(base_url('Compelxion'));

        } 
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['compelxion'] = $this->M_compelxion->compelxion($id)->row_array();
            $this->session->set_userdata('page', 'Update Complexion');
			$this->template->load('agency-admin/static','agency-admin/compelxion/form-compelxion',$data);

        }
        else{
        	redirect(base_url('Admin_login'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $id 			=$this->input->post('id');

			$this->M_compelxion->update_compelxion($name,$id);
            $this->session->set_flashdata('sukses','Complexion has been updated!!!.');
            redirect(base_url('Compelxion'));
            

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function delete($id=null)
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            //$this->load->view('welcome_message');
            $this->M_compelxion->delete_compelxion($id);
			$this->session->set_flashdata('sukses','Complexion has been deleted!!!.');
			redirect(base_url('Compelxion'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
}