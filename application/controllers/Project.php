<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_project');
		$this->load->model('M_client');
		$this->load->model('M_talent');
		$this->load->model('M_gender');
		$this->load->model('M_ethnicity'); 
		$this->load->model('M_category');
		$this->load->model('M_compelxion');
        
	}

	public function index()
	{
		//$this->load->view('welcome_message');
		if($this->session->userdata('bma-agc')){
			$data['project'] = $this->M_project->GetProject()->result();
			$this->session->set_userdata('page', 'Project');
			$this->template->load('agency-admin/static','agency-admin/project/data-project',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
	}
	public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "create";
			$data['client'] = $this->M_client->GetClientActive()->result();
			
            $this->session->set_userdata('page', 'Create Project');
			$this->template->load('agency-admin/static','agency-admin/project/form-project',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
			$name					=strip_tags(addslashes($this->input->post('name',TRUE)));
			$location				=strip_tags(addslashes($this->input->post('location',TRUE)));
			$datestartproject 		=$this->input->post('datestartproject');
			$dateendproject 		=$this->input->post('dateendproject');
			$notes 					=addslashes($this->input->post('notes'));
			$type 					=$this->input->post('type');
			$customer				=$this->input->post('customer');

			$kode = $this->M_project->create_project($name,$customer,$location,$datestartproject,$dateendproject,$notes,$type);
            $this->session->set_flashdata('sukses','Project data has been created!!!.');
            redirect(base_url('Project'));

		}
	}
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['client'] = $this->M_client->GetClientActive()->result();
			$data['project'] = $this->M_project->project($id)->row_array();
			$data['gender'] = $this->M_gender->GetGenderActive()->result();
			$data['ethnicity'] = $this->M_ethnicity->GetEthnicityActive()->result();
			$data['category'] = $this->M_category->GetCategoryActive()->result();
			$data['compelxion'] = $this->M_compelxion->GetCompelxionActive()->result();
			
            $this->session->set_userdata('page', 'Update Project');
			$this->template->load('agency-admin/static','agency-admin/project/update-project',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name					=strip_tags(addslashes($this->input->post('name',TRUE)));
			$location				=strip_tags(addslashes($this->input->post('location',TRUE)));
			$datestartproject 		=$this->input->post('datestartproject');
			$dateendproject 		=$this->input->post('dateendproject');
			$notes 					=addslashes($this->input->post('notes'));
			$type 					=$this->input->post('type');
			$customer				=$this->input->post('customer');

            $id 					=$this->input->post('id');
            $status 				=$this->input->post('status');
            
			$this->M_project->update_project($name,$customer,$location,$datestartproject,$dateendproject,$notes,$type,$status,$id);
			$this->session->set_flashdata('sukses','Project data has been updated!!!.');
			$direct = base_url(''). 'Project/update/'.$id; 
            redirect($direct);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function LoadProject($id=null)
	{
		//$id=$this->input->post('id');
		$projectTalent = $this->M_project->GetProjectTalent($id)->result();

		$output = ' ';
		$array = array();
		foreach ($projectTalent as $p) {
			$id_project=$p->id_project;
			$talent_id=$p->talent_id;
			$imgsrc = '<img src="'. base_url().$p->media_url .'" width="200px" height="200px" >';
			$btnadd = '<div class="btn-group"><button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$talent_id.'" onclick="confirmDelete(\''.$id_project.'\',\''. $talent_id .'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button></div>';
			$array[] = array($p->talentname,$imgsrc,$btnadd);
			//$output .= $talent_id . ' '.$package_id;
			/*
			$output .= '
			<tr>
				<td>'.$p->talentname.'</td>
				<td><img clas="img" src="'. base_url().$p->media_url .'" width="200px" height="200px" /></td>
				<td>
                    <div class="btn-group">
						<button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$talent_id.'" onclick="confirmDelete(\''.$package_id.'\',\''. $talent_id .'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                    </div>
				</td>
				
            </tr>
			'; */
		}

		//echo $output;
		echo json_encode (array("aaData"=>$array)); //Return the JSON Array
	}
	public function LoadProjectTalent()
	{
		$id=$this->input->post('id');
		$gender=$this->input->post('gender');
		$ethnicity=$this->input->post('ethnicity');
		$tag=$this->input->post('tag');
		$category=$this->input->post('category');
		$compelxion=$this->input->post('compelxion');

		$talent = $this->M_talent->talentProjectFilter($id,$gender,$ethnicity,$tag,$category,$compelxion)->result();

		$output = ' ';
		$array = array();
		foreach ($talent as $t) {
			$talent_id=$t->talent_id;
			$name=$t->talent_nickname;
			$media_url = base_url(). $t->media_url;
			$imgsrc = '<img src="'. $media_url .'" width="200px" height="200px" >';
			$btnadd = '<button type="button" class="btn btn-success" data-toggle="tooltip" onclick="addTalent(\''. $id.'\',\''.$talent_id.'\')"  data-placement="top" data-original-title="Update" ><i class="fa fa-plus"></i> ADD</button>';
			$array[] = array($t->talent_id,$imgsrc,$t->talent_nickname,$t->gender_name,$t->ethnicity_name,$t->categoryname,$t->compelxionname,$t->talent_tag,$btnadd);
			/*
			$output .= '
			<tr>
                                    <td>'.$t->talent_id .'</td>
                                    <td><img src="'. $media_url .'" width="200px" height="200px" ></td>
                                    <td>'. $t->talent_nickname .'</td>
                                    <td>'. $t->gender_name .'</td>
                                    <td>'. $t->ethnicity_name .'</td>
                                    <td>'. $t->categoryname .'</td>
                                    <td>'. $t->compelxionname .'</td>
                                    <td><button type="button" class="btn btn-success" data-toggle="tooltip" onclick="addTalent(\''. $id.'\',\''.$talent_id.'\')"  data-placement="top" data-original-title="Update" ><i class="fa fa-plus"></i> ADD</button></td>
                                  </tr>
			'; */
		}

		//echo $output;
		echo json_encode (array("aaData"=>$array)); //Return the JSON Array
	}
	public function addTalent()
	{
		if($this->session->userdata('bma-agc') ){
			$talent 			=addslashes($this->input->post('talentID'));
			$id 				=addslashes($this->input->post('projectId'));

			$this->M_project->addTalent($talent,$id);
			
			//$this->session->set_flashdata('sukses','Talent data has been added!!!.');
			//$direct = base_url(''). 'Packages/update/'.$id; 
            //redirect($direct);
			echo 'success';

		}else{
			//redirect(base_url('AdminLogin'));
			echo 'error';
		}
	}
	public function RemoveTalent()
	{
		if($this->session->userdata('bma-agc') ){
			$projectId 		=addslashes($this->input->post('projectId'));
			$talent_id 			=addslashes($this->input->post('talent_id'));

			//$this->M_packages->remove_location($location_id,$package_id);
			
			//$this->session->set_flashdata('sukses','Location data has been deleted!!!.');
			//$direct = base_url(''). 'Packages/update/'.$package_id; 
			//redirect($direct);
			$response['status']  = 'error';
			$response['message'] = 'Unable to delete  '.$projectId.' '.$talent_id;

			$media=$this->M_project->talentInProject($projectId,$talent_id);
		
			$num = $media->num_rows(); 

			if($num>0){
				$hasil=$media->row_array();
				
				$this->M_project->deleteTalentInProject($projectId,$talent_id);
				$response['status']  = 'success';
				$response['message'] = 'Your data has been deleted ...';
			}

			//echo json_encode($response);
			echo 'success';

		}else{
			//redirect(base_url('Admin_login'));
			$response['status']  = 'error';
			$response['message'] = 'Unable to delete data ';
			//echo json_encode($response);
			echo 'error';
		}
	}
}
