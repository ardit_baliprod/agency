<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Talent extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_talent');
        $this->load->model('M_gender');
        $this->load->model('M_category');
        $this->load->model('M_ethnicity');
        $this->load->model('M_compelxion');
        $this->load->model('M_tags');
        $this->load->model('M_media');
	}
	public function index()
	{
		if($this->session->userdata('bma-agc')){
			$data['talent'] = $this->M_talent->GetTalent()->result();
			$this->session->set_userdata('page', 'Talent');
			$this->template->load('agency-admin/static','agency-admin/talent/data-talent',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
	}
	public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "create";
            $data['gender'] = $this->M_gender->GetGenderActive()->result();
            $data['category'] = $this->M_category->GetCategoryActive()->result();
            $data['ethnicity'] = $this->M_ethnicity->GetEthnicityActive()->result();
            $data['compelxion'] = $this->M_compelxion->GetCompelxionActive()->result();

            $this->session->set_userdata('page', 'Create Talent');
			$this->template->load('agency-admin/static','agency-admin/talent/form-talent',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
            $fname			=strip_tags(addslashes($this->input->post('fname',TRUE)));
            $lname			=strip_tags(addslashes($this->input->post('lname',TRUE)));
            $email			=strip_tags(addslashes($this->input->post('email',TRUE)));
            $phone			=strip_tags(addslashes($this->input->post('phone',TRUE)));
            $birth			=strip_tags(addslashes($this->input->post('birth',TRUE)));
            $age			=strip_tags(addslashes($this->input->post('age',TRUE)));
            $height			=strip_tags(addslashes($this->input->post('height',TRUE)));
            $bust			=strip_tags(addslashes($this->input->post('bust',TRUE)));
            $waist			=strip_tags(addslashes($this->input->post('waist',TRUE)));
            $hips			=strip_tags(addslashes($this->input->post('hips',TRUE)));
            $shoe			=strip_tags(addslashes($this->input->post('shoe',TRUE)));
            $hair			=strip_tags(addslashes($this->input->post('hair',TRUE)));
            $eye			=strip_tags(addslashes($this->input->post('eye',TRUE)));
            $instagram		=addslashes($this->input->post('instagram',TRUE));
            $portofolio		=addslashes($this->input->post('portofolio',TRUE));
            $dress		    =strip_tags(addslashes($this->input->post('dress',TRUE)));
            $shirt		    =strip_tags(addslashes($this->input->post('shirt',TRUE)));
            $suit		    =strip_tags(addslashes($this->input->post('suit',TRUE)));
            $collar		    =strip_tags(addslashes($this->input->post('collar',TRUE)));
            $pants		    =strip_tags(addslashes($this->input->post('pants',TRUE)));
            $bra		    =strip_tags(addslashes($this->input->post('bra',TRUE)));
            $chest		    =strip_tags(addslashes($this->input->post('chest',TRUE)));
            $gender 		=addslashes($this->input->post('gender'));
            $category 		=$this->input->post('category');
            $ethnicity 		=addslashes($this->input->post('ethnicity'));
            $compelxion 	=$this->input->post('compelxion');
            $desc 			=addslashes($this->input->post('bio'));
            $notes 			=addslashes($this->input->post('notes'));
            $tags 		    =$this->input->post('tags');
            
           
			$s	= strtolower($slug);
			
			$kode = $this->M_talent->create_talent($name,$s,$fname,$lname,$email,$phone,$birth,$age,$height,$bust,$waist,$hips,$shoe,$hair,$eye,$gender,$category,$ethnicity,$compelxion,$desc,$tags,$notes,$instagram,$portofolio,$dress,$chest,$suit,$collar,$bra,$shirt,$pants);
            $this->session->set_flashdata('sukses','Talent has been created!!!.');
            //redirect(base_url('Talent'));
            
            $redirect = base_url() . 'Talent/update/' . $kode;
            redirect($redirect);
        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['talent'] = $this->M_talent->talent($id)->row_array();
            $data['talentCategory'] = $this->M_talent->talentCategory($id)->result();
            $data['talentCompelxion'] = $this->M_talent->talentCompelxion($id)->result();
            $data['gender'] = $this->M_gender->GetGenderActive()->result();
            $data['category'] = $this->M_category->GetCategoryActive()->result();
            $data['ethnicity'] = $this->M_ethnicity->GetEthnicityActive()->result();
            $data['compelxion'] = $this->M_compelxion->GetCompelxionActive()->result();
            $data['headshot'] = $this->M_media->GetHeadshot($id)->result();
            $data['galleryVideo'] = $this->M_media->GetGalleryVideos($id)->result();
            $data['videoUncover'] = $this->M_media->GetVideoUncover($id)->result();
            $data['coverVideo'] = $this->M_media->GetCoverVideo($id)->result();

            $this->session->set_userdata('page', 'Update Talent');
			$this->template->load('agency-admin/static','agency-admin/talent/update-talent',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
            $fname			=strip_tags(addslashes($this->input->post('fname',TRUE)));
            $lname			=strip_tags(addslashes($this->input->post('lname',TRUE)));
            $email			=strip_tags(addslashes($this->input->post('email',TRUE)));
            $phone			=strip_tags(addslashes($this->input->post('phone',TRUE)));
            $birth			=strip_tags(addslashes($this->input->post('birth',TRUE)));
            $age			=strip_tags(addslashes($this->input->post('age',TRUE)));
            $height			=strip_tags(addslashes($this->input->post('height',TRUE)));
            $bust			=strip_tags(addslashes($this->input->post('bust',TRUE)));
            $waist			=strip_tags(addslashes($this->input->post('waist',TRUE)));
            $hips			=strip_tags(addslashes($this->input->post('hips',TRUE)));
            $shoe			=strip_tags(addslashes($this->input->post('shoe',TRUE)));
            $hair			=strip_tags(addslashes($this->input->post('hair',TRUE)));
            $eye			=strip_tags(addslashes($this->input->post('eye',TRUE)));
            $instagram		=addslashes($this->input->post('instagram',TRUE));
            $portofolio		=addslashes($this->input->post('portofolio',TRUE));
            $dress		    =strip_tags(addslashes($this->input->post('dress',TRUE)));
            $shirt		    =strip_tags(addslashes($this->input->post('shirt',TRUE)));
            $suit		    =strip_tags(addslashes($this->input->post('suit',TRUE)));
            $collar		    =strip_tags(addslashes($this->input->post('collar',TRUE)));
            $pants		    =strip_tags(addslashes($this->input->post('pants',TRUE)));
            $bra		    =strip_tags(addslashes($this->input->post('bra',TRUE)));
            $chest		    =strip_tags(addslashes($this->input->post('chest',TRUE)));
            $gender 		=addslashes($this->input->post('gender'));
            $category 		=$this->input->post('category');
            $status 		=$this->input->post('status');
            $ethnicity 		=addslashes($this->input->post('ethnicity'));
            $compelxion 	=$this->input->post('compelxion');
            $desc 			=addslashes($this->input->post('bio'));
            $tags 		    =$this->input->post('tags');
            $notes 			=addslashes($this->input->post('notes'));
            
           
			$s	            = strtolower($slug);
			$id 			=$this->input->post('id');

			$this->M_talent->update_talent($name,$s,$fname,$lname,$email,$phone,$birth,$age,$height,$bust,$waist,$hips,$shoe,$hair,$eye,$gender,$category,$ethnicity,$compelxion,$desc,$tags,$id,$notes,$instagram,$portofolio,$dress,$chest,$suit,$collar,$bra,$shirt,$pants,$status);
            $this->session->set_flashdata('sukses','Talent has been Updated!!!.');
            //redirect(base_url('Talent'));
            
            $redirect = base_url() . 'Talent/update/' . $id;
            redirect($redirect);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function delete($id=null)
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            //$this->load->view('welcome_message');
            $this->M_talent->delete_talent($id);
			$this->session->set_flashdata('sukses','Talent has been deleted!!!.');
			redirect(base_url('Talent'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function headshot()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            $id 			=$this->input->post('videoLocationID');
            $statusUploadImage   = false;
            $statusUploadVideo   = false;

            $this->load->library('upload');

            if (! empty($_FILES['video']['name']) || ! empty($_FILES['image']['name'])) {
                $config['upload_path']          = 'uploads/videos/talent/'.$id.'/';	
                $config['allowed_types']        = 'avi|flv|wmv|mkv|mp4|mov|webm|ogg|mpeg2';
                $config['file_name']            = 'headshot-'.$_FILES['video']['name'];
                //$config['overwrite']			= true;
                
                if (!is_dir('uploads/videos/talent/'.$id.'/'))
                {
                    mkdir('uploads/videos/talent/'.$id.'/', 0777, true);
                    $dir_exist = false; // dir not exist
                }
                
                //Load upload library
                //$this->load->library('upload',$config);
                $this->upload->initialize($config);

                if( ! $this->upload->do_upload('video')){
                    echo 'video error '.$this->upload->display_errors();
                    $statusUploadVideo = false;
                }else {
                    //echo 'sukses';
                    // Get data about the file
                    $statusUploadVideo = true;
                    $data = array('upload_data' => $this->upload->data());
        
                        
                    $fileName= $data['upload_data']['file_name'];
                    $fileType= $data['upload_data']['file_type'];
                    $this->M_media->UploadHeadshot($fileName,$fileType,$id);


                    
                }  
                
                $config2['upload_path']          = 'uploads/images/talent/'.$id.'/';	
                $config2['allowed_types']        = 'jpg|jpeg|png|gif';
                //$config2['overwrite']			= true;
                $config2['file_name']            = 'headshot-'.$_FILES['image']['name'];
                    
                if (!is_dir('uploads/images/talent/'.$id.'/'))
                {
                    mkdir('uploads/images/talent/'.$id.'/', 0777, true);
                    $dir_exist = false; // dir not exist
                }
                        
                    //Load upload library
                    // $this->load->library('upload',$config2);
                $this->upload->initialize($config2);
                    
                if( ! $this->upload->do_upload('image')){
                    echo 'image error '.$this->upload->display_errors();
                    $statusUploadImage = false;
                }else {
                    //echo 'sukses';
                    $statusUploadImage = true;
                    $data = array('upload_data' => $this->upload->data());
        
                        
                    $fileName= $data['upload_data']['file_name'];
                    $fileType= $data['upload_data']['file_type'];
                        
                    $this->M_media->UploadHeadshot($fileName,$fileType,$id);

                   
                }

                if($statusUploadImage == true || $statusUploadVideo == true){
                    $this->session->set_flashdata('sukses','Headshot has been Uploaded!!!.');
                    $redirect = base_url() . 'Talent/update/' . $id;
                    redirect($redirect);
                }else{
                    $this->session->set_flashdata('error','Headshot has not upload!!!.');
                    $redirect = base_url() . 'Talent/update/' . $id;
                    redirect($redirect);
                }
                


            }else{
                $this->session->set_flashdata('error','Headshot has not upload!!!.');
                $redirect = base_url() . 'Talent/update/' . $id;
                redirect($redirect);
            }
            


        }else{
            redirect(base_url('AdminLogin'));
        }
    }
    public function embed()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            $id 			=$this->input->post('Talentid');
            $statusUploadEmbed = false;
            $statusUploadVideo = false;

            $this->load->library('upload');

            $config['upload_path']          = 'uploads/videos/talent/'.$id.'/';	
            $config['allowed_types']        = 'avi|flv|wmv|mkv|mp4|mov|webm|ogg|mpeg2';
            $config['file_name']            = $_FILES['file']['name'];
            //$config['overwrite']			= true;
            
            if (!is_dir('uploads/videos/talent/'.$id.'/'))
            {
                mkdir('uploads/videos/talent/'.$id.'/', 0777, true);
                $dir_exist = false; // dir not exist
            }
            
            $embed 			=addslashes($this->input->post('embed'));
            $title 			=$this->input->post('title');
            
            if($embed <> ''){
                $statusUploadEmbed = true;
                $result=$this->M_media->UploadVideoGalleryEmbed($embed,$title,$id);
                
            }
            //Load upload library
            //$this->load->library('upload',$config);
            $this->upload->initialize($config);

            if( ! $this->upload->do_upload('file')){
                $statusUploadVideo = false;
                echo 'error '.$this->upload->display_errors();
            }else {
                $statusUploadVideo = true;
                $data = array('upload_data' => $this->upload->data());
    
                    
                $fileName= $data['upload_data']['file_name'];
                $fileType= $data['upload_data']['file_type'];
                   
                $this->M_media->UploadVideoGallery($fileName,$fileType,$id);

                
            }

            if($statusUploadVideo == true || $statusUploadEmbed == true){
                $this->session->set_flashdata('sukses','Video has been Uploaded!!!.');
                $redirect = base_url() . 'Talent/update/' . $id;
                redirect($redirect);
            }else{
                $this->session->set_flashdata('error','Video has not upload!!!.');
                $redirect = base_url() . 'Talent/update/' . $id;
                redirect($redirect);
            }

        }else{
            redirect(base_url('AdminLogin'));
        }
    }
    // File upload
	public function fileUpload(){
		
		if(!empty($_FILES['file']['name'])){
                
            $TalentID=$this->input->post('inputTalentID');
			// Set preference
			$config['upload_path']          = 'uploads/images/talent/'.$TalentID.'/';	
            $config['allowed_types']        = 'jpg|jpeg|png|gif';
            //$config['overwrite']			= true;
            /*
            $target_file = 'uploads/images/talent/'.$TalentID.'/'.$_FILES['file']['name'];
            // Check if file already exists
            if (file_exists($target_file)) {
                $count = 0;

                while(file_exists($target_file)) {
                    $count++;
                }

                //$newfile = pathinfo($target_file, PATHINFO_FILENAME) . '_' . $count . '.' . pathinfo($target_file, PATHINFO_EXTENSION);
                $newfile = $count . '_' . $_FILES['file']['name'];
                $config['file_name']            = $newfile;
            }else{
                $config['file_name']            = $_FILES['file']['name'];
            }
            */
            //$config['file_name']            = $_FILES['file']['name'];
            
            if (!is_dir('uploads/images/talent/'.$TalentID.'/'))
            {
                mkdir('uploads/images/talent/'.$TalentID.'/', 0777, true);
                $dir_exist = false; // dir not exist
            }
            else{

            }
					
			//Load upload library
			$this->load->library('upload',$config);			
				
			// File upload 
			if($this->upload->do_upload('file')){
				// Get data about the file
                $uploadData = $this->upload->data();
                $fileName = $this->upload->data("file_name");
                $file_type = $this->upload->data("file_type");

                $imageURL = base_url() .'uploads/images/talent/'.$TalentID.'/'.$fileName;
                
                if (getimagesize($imageURL) !== false) {
                    // display image
                    $this->M_media->uploadPhotoGallery($fileName,$file_type,$TalentID);
                }
                
               
			}
        }
        
		
    }
    public function polaroidUpload(){
		
		if(!empty($_FILES['file']['name'])){
                
            $TalentID=$this->input->post('inputPolaroidTalentID');
			// Set preference
			$config['upload_path']          = 'uploads/images/talent/'.$TalentID.'/';	
            $config['allowed_types']        = 'jpg|jpeg|png|gif';
            //$config['overwrite']			= true;
            $config['file_name']            = 'polaroid-'.$_FILES['file']['name'];
            
            if (!is_dir('uploads/images/talent/'.$TalentID.'/'))
            {
                mkdir('uploads/images/talent/'.$TalentID.'/', 0777, true);
                $dir_exist = false; // dir not exist
            }
            else{

            }
					
			//Load upload library
			$this->load->library('upload',$config);			
				
			// File upload
			if($this->upload->do_upload('file')){
				// Get data about the file
                $uploadData = $this->upload->data();
                $fileName = $this->upload->data("file_name");
                $file_type = $this->upload->data("file_type");


                $imageURL = base_url() .'uploads/images/talent/'.$TalentID.'/'.$fileName;
                
                if (getimagesize($imageURL) !== false) {
                    // display image
                    $this->M_media->uploadPhotoPolaroid($fileName,$file_type,$TalentID);
                }
                
                
               
			}
        }
        
		
    }
    public function coverUpload()
    {
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $id=$this->input->post('Talentid');
            $video=$this->input->post('video');

            // Set preference
			$config['upload_path']          = 'uploads/images/talent/'.$id.'/';	
            $config['allowed_types']        = 'jpg|jpeg|png|gif';
            $config['overwrite']			= true;
            $config['file_name']            = 'cover-'.$_FILES['file']['name'];
            
            if (!is_dir('uploads/images/talent/'.$id.'/'))
            {
                mkdir('uploads/images/talent/'.$id.'/', 0777, true);
                $dir_exist = false; // dir not exist
            }
            else{

            }
					
			//Load upload library
			$this->load->library('upload',$config);			
				
			// File upload
			if($this->upload->do_upload('file')){
				// Get data about the file
                $uploadData = $this->upload->data();
                $fileName = $this->upload->data("file_name");
                $file_type = $this->upload->data("file_type");
                
                $this->M_media->uploadPhotoCover($fileName,$file_type,$video,$id);

                $this->session->set_flashdata('sukses','Cover video has been Uploaded!!!.');
                $redirect = base_url() . 'Talent/update/' . $id;
                redirect($redirect);
               
			}else{
                $this->session->set_flashdata('error',$this->upload->display_errors());
                $redirect = base_url() . 'Talent/update/' . $id;
                redirect($redirect);
            }

        }
        else{
            redirect(base_url('AdminLogin'));
        }

    }
    public function deleteMedia($id = null){
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            $id=$this->input->post('Mediaid');
            $TalentId=$this->input->post('TalentId');

            $media=$this->M_media->media($id);

            
            $num = $media->num_rows(); 

            if($num>0){
                $hasil=$media->row_array();
                $nama_foto=$hasil['media_url'];
                if(file_exists($file=FCPATH.''.$nama_foto)){
                    unlink($file);
                }
                $foto=$this->M_media->delete_data($id);

                
            }
            
            $this->session->set_flashdata('sukses','Media has been deleted!!!.');
            $redirect = base_url() . 'Talent/update/' . $TalentId;
            redirect($redirect);
        }
        else{
            redirect(base_url('AdminLogin'));
        }
    }
    public function removeMedia(){

		//Ambil token foto
        $id=$this->input->post('id');
        //$TalentId=$this->input->post('TalentId');
        
        $media=$this->M_media->media($id);

        $response['status']  = 'error';
        $response['message'] = 'Unable to delete file '.$id;

        $num = $media->num_rows();  

		if($num>0){
			$hasil=$media->row_array();
			$nama_foto=$hasil['media_url'];
			if(file_exists($file=FCPATH.''.$nama_foto)){
				unlink($file);
			}
			$foto=$this->M_media->delete_data($id);
            $response['status']  = 'success';
            $response['message'] = 'Your file has been deleted ...';
		}

       
        //echo json_encode($response);
        echo 'success';
    }
    // File upload
	public function videoUpload(){
		
        $locationId= $this->input->post('videoLocationID');
				
		// Set preference
		$config['upload_path'] = 'uploads/videos/location/'.$locationId.'/';	
        $config['allowed_types'] = 'avi|flv|wmv|mkv|mp4|mov|webm|ogg|mpeg2';
        $config['file_name'] = $_FILES['file']['name'];
        $config['overwrite']			= true;
        
        if (!is_dir('uploads/videos/location/'.$locationId.'/'))
        {
            mkdir('uploads/videos/location/'.$locationId.'/', 0777, true);
            $dir_exist = false; // dir not exist
        }
        else{

        }

       
					
		//Load upload library
		$this->load->library('upload',$config);			
				
		// File upload
		if( ! $this->upload->do_upload('image')){
            echo 'error';
		}else {
            // Get data about the file
            $data = array('upload_data' => $this->upload->data());
 
                
            $fileName= $data['upload_data']['file_name'];
            $fileType= $data['upload_data']['file_type']; 
                
            //$result= $this->upload_model->save_upload($title,$image);
                
            //$uploadData = $this->upload->data();
            //$fileName = $this->upload->data("file_name");
            // $locationId=$this->input->post('inputLocationID');
            //$result=$this->M_media->simpan_Media_Video($fileName,$locationId,$fileType);
            //echo json_decode($result);
        }



        
        
		
    }
    public function ListImageGallery(){ 
        $inputTalentID=$this->input->post('inputTalentID');
        $ImageMedia = $this->M_media->GetGalleryImages($inputTalentID)->result();
        $output = ' ';
        foreach ($ImageMedia as $l) {
            
            $media_url = $l->media_url;
            $media_name = $l->media_name;
            $media_id = $l->media_id;
            $full_url = base_url(). $media_url;
            list($width, $height) = getimagesize($full_url);
            if ($width > $height) {
                // Landscape
                $output .= '<div class="ui-state-default col-md-6" id="image_'.$media_id.'">';
            } else {
                // Portrait or Square
                $output .= '<div class="ui-state-default col-md-3" id="image_'.$media_id.'">';
            }
            $output .= '
                <div class="galleryTalentImage" style="background-image: url(\''.$full_url.'\')"></div>
                <br>
                <input type="checkbox" class="gallery-checkbox" value="'.$media_id.'" />
                <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$media_id.'" onclick="confirmDelete(\''.$media_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                </div>
            ';
                /*
                <img src="'.$full_url.'" title="'.$media_name.'" style="width: auto;max-height: 300px;" >
                $output .= '
                <div class="col-md-3">
                    <img src="'.$full_url.'" class="img-thumbnail" width="350" height="350" style="height:350px;" />
                    <br/>
                    <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$media_id.'" onclick="confirmDelete(\''.$media_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                    
                </div>
                '; 
                $output .= '<li class="ui-state-default" id="image_'.$media_id.'">
                                <img src="'.$full_url.'" title="'.$media_name.'" >
                                <br>
                                <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$media_id.'" onclick="confirmDelete(\''.$media_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                            </li>'; */
             
            
        }
       
        echo $output;
    }
    public function ListImagePolaroid(){ 
        $inputTalentID=$this->input->post('inputTalentID');
        $ImageMedia = $this->M_media->GetPolaroidImages($inputTalentID)->result();
        $output = ' ';
        foreach ($ImageMedia as $l) {
            
            $media_url = $l->media_url;
            $media_name = $l->media_name;
            $media_id = $l->media_id;
            $full_url = base_url(). $media_url;
            list($width, $height) = getimagesize($full_url);
            if ($width > $height) {
                // Landscape
                $output .= '<div class="ui-state-default col-md-6" id="image_'.$media_id.'">';
            } else {
                // Portrait or Square
                $output .= '<div class="ui-state-default col-md-3" id="image_'.$media_id.'">';
            }
            $output .= '
                <div class="galleryTalentImage" style="background-image: url(\''.$full_url.'\')"></div>
                <br>
                <input type="checkbox" class="polaroid-checkbox" value="'.$media_id.'" />
                <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$media_id.'" onclick="confirmDelete(\''.$media_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
            </div>
            ';
                /*
                <img src="'.$full_url.'" title="'.$media_name.'" style="width: auto;max-height: 300px;" >
                $output .= '
                <div class="col-md-3">
                    <img src="'.$full_url.'" class="img-thumbnail" width="350" height="350" style="height:350px;" />
                    <br/>
                    <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$media_id.'" onclick="confirmDelete(\''.$media_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                    
                </div>
                '; 
                $output .= '<li class="ui-state-default" id="image_'.$media_id.'">
                                <img src="'.$full_url.'" title="'.$media_name.'" >
                                <br>
                                <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$media_id.'" onclick="confirmDelete(\''.$media_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                            </li>'; */
             
            
        }
       
        echo $output;
    }
    private function _uploadImage()
	{
		$config['upload_path']              = 'uploads/images/category/';	
        $config['allowed_types']            = 'jpg|jpeg|png|gif';
        $config['overwrite']			    = true;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			return $this->upload->data("file_name");
		}
		
		return "no-images.jpeg";
    }
    public function getTags(){
        if(isset($_GET["query"]))
        {
            $query = $_GET["query"];
            $rowTags = $this->M_tags->tagInput($query)->result();
            foreach ($rowTags as $r) { 
                $data[] = $r->name_tags;
                //echo $r->name_tags;
            }
            echo json_encode($data);
        }
        
    }
    public function Reorder(){ 
        //$imageids_arr = $_POST['imageids'];
        $imageids_arr=$this->input->post('imageids');

        // Update sort position of images
        $position = 1;
        foreach($imageids_arr as $id){
            //mysqli_query($con,"UPDATE images_list SET sort=".$position." WHERE id=".$id);
            $hsl=$this->db->query("UPDATE agc_talent_media SET media_order='$position' WHERE media_id='$id' ");
            $position ++;
        }
        
        echo "Update successfully";
        exit;
    }
    public function ReorderPolaroid(){ 
        //$imageids_arr = $_POST['imageids'];
        $imageids_arr=$this->input->post('imageids');

        // Update sort position of images
        $position = 1;
        foreach($imageids_arr as $id){
            //mysqli_query($con,"UPDATE images_list SET sort=".$position." WHERE id=".$id);
            $hsl=$this->db->query("UPDATE agc_talent_media SET media_order='$position' WHERE media_id='$id' ");
            $position ++;
        }
        
        echo "Update successfully";
        exit;
    }
}