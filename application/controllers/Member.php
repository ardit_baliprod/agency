<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
        parent::__construct();
        $this->load->model('M_packages');
        $this->load->model('M_client');
		$this->load->model('M_talent');
		$this->load->model('M_media');
		$this->load->model('M_setting');
		$this->load->model('M_contact'); 
        $this->load->model('M_catalog');
		$this->load->model('M_country');
		$this->data = array(
			'action' => site_url('account/do_login'),
			'username' => set_value('username'),
			'password' => set_value('password'),
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
			'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
		);
		
	}
	public function index()
	{
		//$this->load->view('agency-web/static-template');
		$data = $this->data;
		$this->session->set_userdata('page', 'Member Dashboard');
        $this->template->load('agency-web/static-template','agency-web/member/dashboard',$data);
    }
    public function register()
	{
        //$this->load->view('agency-web/static-template');
        $data = $this->data;
        $data['country'] = $this->M_country->GetCountry()->result();
		$this->session->set_userdata('page', 'Register Member');
        $this->template->load('agency-web/static-template','agency-web/member/register',$data);
	}
	
}
