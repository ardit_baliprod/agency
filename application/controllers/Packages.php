<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
        parent::__construct();
        $this->load->model('M_packages');
        $this->load->model('M_client');
		$this->load->model('M_talent');
		$this->load->model('M_setting');
		$this->load->model('M_contact');
		$this->load->model('M_gender');
		$this->load->model('M_ethnicity'); 
		$this->load->model('M_category');
		$this->load->model('M_compelxion');

	}
	public function index()
	{
		if($this->session->userdata('bma-agc')){
			$data['packages'] = $this->M_packages->GetPackages()->result();
			
			$this->session->set_userdata('page', 'Packages');
			$this->template->load('agency-admin/static','agency-admin/packages/data-packages',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
    }
    public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
			$data['type']	= "create";
			$data['client'] = $this->M_client->GetClientActive()->result();
            $this->session->set_userdata('page', 'Create Package');
			$this->template->load('agency-admin/static','agency-admin/packages/form-packages',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name				=strip_tags(addslashes($this->input->post('name',TRUE)));
            $customer 			=addslashes($this->input->post('customer'));
            $desc 			    =addslashes($this->input->post('desc'));
            
			$kode = $this->M_packages->create_package($name,$customer,$desc);
            $this->session->set_flashdata('sukses','Package data has been created!!!.');
            //redirect(base_url('Packages'));
			$redirect = base_url('Packages/update/') . $kode;
			redirect($redirect);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['client'] = $this->M_client->GetClientActive()->result();
            $data['packages'] = $this->M_packages->package($id)->row_array();
			$data['packagesTalent'] = $this->M_packages->GetPackagesTalent($id)->result();
			$data['talent'] = $this->M_talent->talentPackage($id)->result();
			$data['gender'] = $this->M_gender->GetGenderActive()->result();
			$data['ethnicity'] = $this->M_ethnicity->GetEthnicityActive()->result();
			$data['category'] = $this->M_category->GetCategoryActive()->result();
			$data['compelxion'] = $this->M_compelxion->GetCompelxionActive()->result();
            $this->session->set_userdata('page', 'Update Package');
			$this->template->load('agency-admin/static','agency-admin/packages/update-packages',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name				=strip_tags(addslashes($this->input->post('name',TRUE)));
            $customer 			=addslashes($this->input->post('customer'));
            $desc 			    =addslashes($this->input->post('desc'));
            $id 				=addslashes($this->input->post('id'));
            $status 			=addslashes($this->input->post('status'));
            
			$this->M_packages->update_package($name,$customer,$desc,$id,$status);
			$this->session->set_flashdata('sukses','Package data has been updated!!!.');
			$direct = base_url(''). 'Packages/update/'.$id; 
            redirect($direct);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function addTalent()
	{
		if($this->session->userdata('bma-agc') ){
			$talent 			=addslashes($this->input->post('talentID'));
			$id 				=addslashes($this->input->post('packageId'));

			$this->M_packages->addTalent($talent,$id);
			
			//$this->session->set_flashdata('sukses','Talent data has been added!!!.');
			//$direct = base_url(''). 'Packages/update/'.$id; 
            //redirect($direct);
			echo 'success';

		}else{
			//redirect(base_url('AdminLogin'));
			echo 'error';
		}
	}
	public function RemoveTalent()
	{
		if($this->session->userdata('bma-agc') ){
			$package_id 		=addslashes($this->input->post('package_id'));
			$talent_id 			=addslashes($this->input->post('talent_id'));

			//$this->M_packages->remove_location($location_id,$package_id);
			
			//$this->session->set_flashdata('sukses','Location data has been deleted!!!.');
			//$direct = base_url(''). 'Packages/update/'.$package_id; 
			//redirect($direct);
			$response['status']  = 'error';
			$response['message'] = 'Unable to delete file '.$package_id.' '.$talent_id;

			$media=$this->M_packages->talentInPackage($package_id,$talent_id);
		
			$num = $media->num_rows(); 

			if($num>0){
				$hasil=$media->row_array();
				
				$this->M_packages->deleteTalentInPackage($package_id,$talent_id);
				$response['status']  = 'success';
				$response['message'] = 'Your file has been deleted ...';
			}

			//echo json_encode($response);
			echo 'success';

		}else{
			//redirect(base_url('Admin_login'));
			$response['status']  = 'error';
			$response['message'] = 'Unable to delete file ';
			//echo json_encode($response);
			echo 'error';
		}
	}
	public function LoadPackage($id=null)
	{
		//$id=$this->input->post('id');
		$packagesTalent = $this->M_packages->GetPackagesTalent($id)->result();

		$output = ' ';
		$array = array();
		foreach ($packagesTalent as $p) {
			$package_id=$p->package_id;
			$talent_id=$p->talent_id;
			$imgsrc = '<img src="'. base_url().$p->media_url .'" width="200px" height="200px" >';
			$btnadd = '<div class="btn-group"><button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$talent_id.'" onclick="confirmDelete(\''.$package_id.'\',\''. $talent_id .'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button></div>';
			$array[] = array($p->talentname,$imgsrc,$btnadd);
			//$output .= $talent_id . ' '.$package_id;
			/*
			$output .= '
			<tr>
				<td>'.$p->talentname.'</td>
				<td><img clas="img" src="'. base_url().$p->media_url .'" width="200px" height="200px" /></td>
				<td>
                    <div class="btn-group">
						<button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$talent_id.'" onclick="confirmDelete(\''.$package_id.'\',\''. $talent_id .'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                    </div>
				</td>
				
            </tr>
			'; */
		}

		//echo $output;
		echo json_encode (array("aaData"=>$array)); //Return the JSON Array
	}
	//public function LoadPackageTalent($id=null,$gender = null,$ethnicity = null,$tag = null,$category = null,$compelxion = null)
	public function LoadPackageTalent()
	{
		$id=$this->input->post('id');
		$gender=$this->input->post('gender');
		$ethnicity=$this->input->post('ethnicity');
		$tag=$this->input->post('tag');
		$category=$this->input->post('category');
		$compelxion=$this->input->post('compelxion');
		//$talent = $this->M_talent->talentPackage($id)->result();

		//$strtag= urlencode($tag);
		/*
		$qry="SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,j.media_type,j.media_mime,j.media_url,f.categoryid,i.compelxionid
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname',GROUP_CONCAT(e.category_id SEPARATOR', ') AS 'categoryid' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id 
        LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname',GROUP_CONCAT(h.compelxion_id SEPARATOR', ') AS 'compelxionid' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id  GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id 
        LEFT JOIN agc_talent_media j ON a.talent_id = j.talent_id 
        WHERE j.media_type = 'Headshot' AND j.media_mime LIKE 'image%' AND a.talent_status=1
        AND a.talent_id NOT IN (SELECT talent_id FROM agc_package_talent WHERE package_id = '$id')  ";

        if($gender && $gender <> ''){
            $qry .=" AND a.gender_id='$gender' ";
        }
        if($ethnicity && $ethnicity <> ''){
            $qry .=" AND a.ethnicity_id='$ethnicity' ";
        }
        if($tag && $tag <> ''){
            $qry .=" AND a.talent_tag LIKE '%$tag%' ";
        }
        if($category && $category <> ''){
            $qry .=" AND f.categoryid LIKE '%$category%' ";
        }
        if($compelxion && $compelxion <> ''){
            $qry .=" AND i.compelxionid LIKE '%$compelxion%' ";
        }
		$qry .=" ORDER BY a.talent_nickname ";

		echo $qry;
		*/
		
		
		
		$talent = $this->M_talent->talentPackageFilter($id,$gender,$ethnicity,$tag,$category,$compelxion)->result();

		$output = ' ';
		$array = array();
		foreach ($talent as $t) {
			$talent_id=$t->talent_id;
			$name=$t->talent_nickname;
			$media_url = base_url(). $t->media_url;
			$imgsrc = '<img src="'. $media_url .'" width="200px" height="200px" >';
			$btnadd = '<button type="button" class="btn btn-success" data-toggle="tooltip" onclick="addTalent(\''. $id.'\',\''.$talent_id.'\')"  data-placement="top" data-original-title="Update" ><i class="fa fa-plus"></i> ADD</button>';
			$array[] = array($t->talent_id,$imgsrc,$t->talent_nickname,$t->gender_name,$t->ethnicity_name,$t->categoryname,$t->compelxionname,$t->talent_tag,$btnadd);
			/*
			$output .= '
			<tr>
                                    <td>'.$t->talent_id .'</td>
                                    <td><img src="'. $media_url .'" width="200px" height="200px" ></td>
                                    <td>'. $t->talent_nickname .'</td>
                                    <td>'. $t->gender_name .'</td>
                                    <td>'. $t->ethnicity_name .'</td>
                                    <td>'. $t->categoryname .'</td>
                                    <td>'. $t->compelxionname .'</td>
                                    <td><button type="button" class="btn btn-success" data-toggle="tooltip" onclick="addTalent(\''. $id.'\',\''.$talent_id.'\')"  data-placement="top" data-original-title="Update" ><i class="fa fa-plus"></i> ADD</button></td>
                                  </tr>
			'; */
		}

		//echo $output;
		echo json_encode (array("aaData"=>$array)); //Return the JSON Array
	}
	public function ListTalent()
	{
		if($this->session->userdata('bma-agc') ){
            $id 				=addslashes($this->input->post('id'));
            //$id 				= 'CTL0001';

            //$this->M_catalog->listTalent($id);
            $listTalent = $this->M_packages->listTalent($id)->result();
            $output = ' ';

            foreach ($listTalent as $l) {
            
                $media_url = $l->media_url;
                $talent_id = $l->talent_id;
                $nickname = $l->talent_nickname;
                $full_url = base_url(). $media_url;
                $output .= '<div class="agency-catalog-box ui-state-default" id="catalogTalent_'.$talent_id.'">
                                <div class="agency-catalog-headshot" style="background-image: url(\''.$full_url.'\')" >
                                </div>
                                <br>
                                <p>'.$nickname.'</p>
                                <br>
                                <input type="checkbox" class="gallery-checkbox" value="'.$talent_id.'" />
                                <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$talent_id.'" onclick="confirmDelete(\''.$id.'\',\''.$talent_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                            </div> '; 

                
            }
           
            echo $output;            
            
			//$this->session->set_flashdata('sukses','Talent data has been added!!!.');
			//$direct = base_url(''). 'Packages/update/'.$id; 
            //redirect($direct);
			//echo 'success';

		}else{
			//redirect(base_url('AdminLogin'));
			echo ' ';
		}
	}
	public function Reorder(){ 
        //$imageids_arr = $_POST['imageids'];
		$imageids_arr=$this->input->post('imageids');
		$packagetalentID=$this->input->post('packagetalentID');
		//$catalogtalentID= 'CTL0001';
		//$imageids_arr= array("TLT0001", "TLT0004", "TLT0003");;
		//echo '<script>console.log('.$imageids_arr.');</script>';
		//echo '<script>alert("'.$catalogtalentID.'");</script>';

        // Update sort position of images
        $position = 1;
        foreach($imageids_arr as $id){
			//mysqli_query($con,"UPDATE images_list SET sort=".$position." WHERE id=".$id);
			//echo '<script>alert("UPDATE agc_catalog_talent SET order='.$position.' WHERE catalog_id='.$catalogtalentID.' AND talent_id='.$id.' ");</script>';
            $hsl=$this->db->query("UPDATE agc_package_talent SET list_order='$position' WHERE package_id='$packagetalentID' AND talent_id='$id' ");
            $position ++;
        }
        
        echo "Update successfully";
        exit;
	}
	public function sendmail(){
		if($this->session->userdata('bma-agc') ){
			//Ambil token foto
			//$id=$this->input->post('packageid');
			$id=$this->input->post('packageid');
			$messageEmail=$this->input->post('messageEmail');
			$addEmail=$this->input->post('addEmail');
			//$id='PCK0001'; 
			
			$packages = $this->M_packages->Package($id);
	
			
			
			$num = $packages->num_rows();
	
			if($num>0){
				$hasil=$packages->row_array();
				$setting = $this->M_setting->GetSetting()->result();
	
				foreach ($setting as $s) { 
					if($s->option_name=='stmphost'){
						$stmphost = $s->option_value;
					}
					if($s->option_name=='stmpauth'){
						$stmpauth = $s->option_value;
					}
					if($s->option_name=='stmpusername'){
						$stmpusername = $s->option_value;
					}
					if($s->option_name=='stmppassword'){
						$stmppassword = $s->option_value;
					}
					if($s->option_name=='stmpsecure'){
						$stmpsecure = $s->option_value;
					}
					if($s->option_name=='stmpport'){
						$stmpport = $s->option_value;
					}
					if($s->option_name=='sendgrid_api'){
						$sendgrid_api = $s->option_value;
					}
				}
				$customer = $hasil['client_id'];
	
				//$rw_customer = $this->M_client->Customer($customer)->result(); 
				$contact = $this->M_contact->ContactActive($customer)->result();
				
				include APPPATH . 'third_party/sendgrid-php/sendgrid-php.php';
	
				//$this->load->third_party('sendgrid-php');
	
				$email = new \SendGrid\Mail\Mail();
				$email->setFrom("contact@balimodelagency.com", "Contact BMA");
				$email->setSubject("Talent Package");
				/*
				$email->addContent(
				"text/plain", "and easy to do anywhere, even with PHP"
				);
				$email->addContent(
				"text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
				);*/
				//$kode_package = 'PKG0013'; 
				$tos = array();
				foreach ($contact as $c) { 
					//$mail->addAddress($c->contact_email);
					$fullname = $c->contact_title . ' ' . $c->contact_fname . ' '. $c->contact_lname;
					//$email->addTo($c->contact_email, $fullname);
					$tos += array($c->contact_email => $fullname);
				}
				//$email->addTo('sim@balimodelagency.com', 'Simone');
				
				$emailArr = explode(',', $addEmail);
        		foreach($emailArr as $em){
					$emailcc = strtolower($em);
					$string_email = preg_replace('/\s+/', '', $emailcc);
					//$email->addTo($string_email);
					$tos += array($string_email => " ");
				}
				$email->addTos($tos);
				ob_start();
				$data['packages'] = $this->M_packages->package($id)->row_array();
				$data['setting'] = $this->M_setting->GetSetting()->result();
				$data['talent'] = $this->M_talent->talentPackagesEmail($id)->result();
				$data['messageEmail'] = $messageEmail;
				$link=sha1(md5($id)); 
				$packagelink = base_url('Catalog/package/').$link;
				$data['link'] = $packagelink;
				$this->load->view('email/email_packages',$data);
				$mailContent = ob_get_contents();
				//$mail->Body = $mailContent;
				$email->addContent(
						"text/html", $mailContent
						);
				ob_end_clean();
								
				//$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
				//$API_KEY = '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';
				//$sendgrid = new \SendGrid($API_KEY);
				$sendgrid = new \SendGrid($sendgrid_api);
				try {
				$response = $sendgrid->send($email);
				//print $response->statusCode() . "\n";
				//print_r($response->headers());
				//print $response->body() . "\n";

				$retStatus = 'success';
				//$response['status']  = 'success';
				//$response['message'] = 'Your message has been sent ...';
				} catch (Exception $e) {
				//echo 'Caught exception: ',  $e->getMessage(), "\n";
				$retStatus = 'success';
				//$response['status']  = 'error';
				//$response['message'] = 'Unable to send email ';
				//$exception = 'Caught exception: '.  $e->getMessage();
				//$response['exception'] = $exception;
				//echo "<script>console.log('Debug Objects: " . $response . "' );</script>";

				}
				echo $retStatus;
			}
	
		   
			//echo json_encode($response);
			
		}else{
			redirect(base_url('AdminLogin'));
		}
		
	}
}
