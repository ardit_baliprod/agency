<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->model('M_packages');
        $this->load->model('M_client');
		$this->load->model('M_talent'); 
		$this->load->model('M_setting');
        $this->load->model('M_contact');
        $this->load->model('M_media');
	  
    } 

	public function index()
	{
		//$this->load->view('welcome_message');
    }
    public function package($id=null)
    { 
        //$this->load->view('download');
        $data['packages'] = $this->M_packages->Package($id)->row_array();
        $data['talents'] = $this->M_packages->EmailPackageTalent($id)->result();
        $data['talentMedia'] = $this->M_media->GetGalleryPDF($id)->result();
	    //$data['listing'] = $this->M_listing->GetEmailGuestListing($id)->result();
        //$data['listing'] = $this->M_listing->GetPDFLCTListing($id)->result();
        //$data['pdflisting'] = $this->M_listing->GetPDFListing($id)->result();
		//$this->load->view('agency-admin/packages/pdf-packages',$data);
		$this->load->view('pdf/packages-compcard',$data);
	}
	public function twophoto()
	{
		//$this->load->view('welcome_message');
		$this->load->view('pdf/pdf-two-photos');
	}
	public function compcard()
	{
		//$this->load->view('welcome_message');
		$this->load->view('pdf/pdf-compcard');
    }
}
