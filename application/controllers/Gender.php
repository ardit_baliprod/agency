<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gender extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_gender');
	
	}
	public function index()
	{
		if($this->session->userdata('bma-agc')){
			$data['gender'] = $this->M_gender->GetGender()->result();
			$this->session->set_userdata('page', 'Gender');
			$this->template->load('agency-admin/static','agency-admin/gender/data-gender',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
	}
	public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "create";
            $this->session->set_userdata('page', 'Create Gender');
			$this->template->load('agency-admin/static','agency-admin/gender/form-gender',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
            $desc 			=addslashes($this->input->post('desc'));
           
			$s	= strtolower($slug);
			
			$this->M_gender->create_gender($name,$s,$desc);
            $this->session->set_flashdata('sukses','gender has been created!!!.');
            redirect(base_url('Gender'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	} 
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['gender'] = $this->M_gender->gender($id)->row_array();
            $this->session->set_userdata('page', 'Update Gender');
			$this->template->load('agency-admin/static','agency-admin/gender/form-gender',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
			$desc 			=addslashes($this->input->post('desc'));
			$status 			=$this->input->post('status');
           
			$s				=strtolower($slug);
			$id 			=$this->input->post('id');

			$this->M_gender->update_gender($name,$s,$desc,$status,$id);
            $this->session->set_flashdata('sukses','gender has been updated!!!.');
            redirect(base_url('Gender'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function delete($id=null)
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            //$this->load->view('welcome_message');
            $this->M_gender->delete_gender($id);
			$this->session->set_flashdata('sukses','Gender has been deleted!!!.');
			redirect(base_url('Gender'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
}