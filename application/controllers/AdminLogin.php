<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminLogin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_user');
		$this->load->model('M_log');
		$this->load->helper('captcha');
	}

	public function index()
	{
		if($this->session->userdata('bma-agc')){
			redirect(base_url('Dashboard'));
		}else{
			$data = array(
				'action' => site_url('welcome/login'),
				'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
				'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
			);
			$this->session->set_userdata('page', 'Admin Login');
			$this->load->view('agency-admin/login',$data);
		}
		
        //$this->template->load('agency-admin/static','agency-admin/dashboard');
	}

	public function validation(){

		// validasi form
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('user_password', 'Password', 'trim|required');

		$recaptcha = $this->input->post('g-recaptcha-response');
	    $response = $this->recaptcha->verifyResponse($recaptcha);
	   

		if ($this->form_validation->run() == FALSE || !isset($response['success']) || $response['success'] <> true) {
			$data = array(
				'action' => site_url('welcome/login'),
				'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
				'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
			);
			$this->load->view('agency-admin/login',$data);
		}else{
			$username=strip_tags(addslashes($this->input->post('user_name',TRUE)));
			$password=$this->input->post('user_password');
			
			$u	= strtolower($username);

			$cadmin=$this->M_user->login($u);
		    $xcadmin=$cadmin->row_array();
			$user = $xcadmin['username'];
		    //$email = $xcadmin['email_karyawan'];
		    //$pass_ambil = $xcadmin['password_karyawan'];
		    $pass_ambil = $xcadmin['password'];
			$pass_en = sha1(md5($password));
			
			if($u == $user && $pass_en == $pass_ambil){
				$iduser = $xcadmin['id_user'];
				$name = $xcadmin['fname'] .' '.$xcadmin['lname'];
				$position = $xcadmin['position'];
				$newdata = array(
					'bma-agc'       =>  TRUE,
					'iduser'        =>  $iduser,
					'user'        	=>  $u,
					'name'        	=>  $name,
					'position'    	=>  $position
				);
				
				$this->session->set_userdata($newdata);
				//echo date("Y-m-d H:i:s e");
				$login=$xcadmin['login'];
				$login++;
				$ip = $this->input->ip_address();
				date_default_timezone_set("Asia/Bangkok");
				$datuser = array(	'login' 			=> $login,
									'last_login' 		=> date("Y-m-d h:i:s"),
									'last_login_ip'		=> $ip
									);
				$this->db->where('id_user',$iduser);
				$this->db->update('agc_userdetail', $datuser);

				$desc='Login User '.$username.' '.$kode;
				//$iduser=$this->session->userdata('user');
				$this->M_log->add($iduser,$desc);

				redirect(base_url('Dashboard'));
			}else{
				//echo 'user input : '. $u . ' user database : '. $user. ' pass en : '. $pass_en .' pass database : '.$pass_ambil ;
				$this->session->set_flashdata('error','Username and password not found!!!');
				$data = array(
					'action' => site_url('welcome/login'),
					'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
					'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
				);
				$this->load->view('agency-admin/login',$data);
			}

		}

	}
	public function logout(){
		//$this->session->sess_destroy();
		//$user=$this->session->userdata('user');
  		//$name=$this->session->userdata('name');
  		//$position=$this->session->userdata('position');
		//$desc='Logout user '.$user;
		//$log=$this->M_history->tambah($user,$desc);
		$user=$this->session->userdata('user');
		$iduser=$this->session->userdata('iduser');
		$desc='Logout User '.$user.' '.$iduser;
		$this->M_log->add($iduser,$desc);

        $this->session->set_userdata('bma-agc',FALSE);
        unset(
               
				$_SESSION['user'],
				$_SESSION['name'],
				$_SESSION['position']
        );
		redirect(base_url('AdminLogin'));
	}

}