<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminUser extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
		parent::__construct();
		$this->load->model('M_user');
	
	}
	public function index()
	{
        //$this->load->view('agency-admin/static');
		//$this->template->load('agency-admin/static','agency-admin/dashboard');
		if($this->session->userdata('bma-agc')){
			$data['user'] = $this->M_user->GetUser()->result();
			$this->session->set_userdata('page', 'User');
			$this->template->load('agency-admin/static','agency-admin/admin-user/data-user',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
		
	}
	public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "create";
            $this->session->set_userdata('page', 'Create User');
			$this->template->load('agency-admin/static','agency-admin/admin-user/form-user',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $username			=strip_tags(addslashes($this->input->post('username',TRUE)));
            $fname				=strip_tags(addslashes($this->input->post('fname',TRUE)));
            $lname				=strip_tags(addslashes($this->input->post('lname',TRUE)));
            $telepon 			=strip_tags(addslashes($this->input->post('phone',TRUE)));
            $position 			=addslashes($this->input->post('position'));
            $email 				=addslashes($this->input->post('email'));
            $pass 				=addslashes($this->input->post('pass'));
            $repass 			=addslashes($this->input->post('repass'));

			$u	= strtolower($username);
			
			$cekuser = true;

			$cekuser = $this->M_user->check_user($u,$email);

            if($pass<>$repass || $cekuser == false){
                $this->session->set_flashdata('error','Cek kembali inputan anda!!!.');
                redirect(base_url('AdminUser/create'));
            }else{
                //$passwd = sha1(md5($pass));
                $this->M_user->create_user($u,$fname,$lname,$position,$telepon,$email,$pass);
                $this->session->set_flashdata('sukses','user has been created!!!.');
                redirect(base_url('AdminUser'));
            }

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['user'] = $this->M_user->User($id)->row_array();
            $this->session->set_userdata('page', 'Update User');
			$this->template->load('agency-admin/static','agency-admin/admin-user/form-user',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $username			=strip_tags(addslashes($this->input->post('username',TRUE)));
            $fname				=strip_tags(addslashes($this->input->post('fname',TRUE)));
            $lname				=strip_tags(addslashes($this->input->post('lname',TRUE)));
            $telepon 			=strip_tags(addslashes($this->input->post('phone',TRUE)));
            $position 			=addslashes($this->input->post('position'));
            $email 				=addslashes($this->input->post('email'));
            $pass 				=addslashes($this->input->post('pass'));
            $repass 			=addslashes($this->input->post('repass'));
			$status 			=addslashes($this->input->post('status'));
			$id 				=$this->input->post('id');

			$u	= strtolower($username);
			
			$cekuser = true;

			$cekuser = $this->M_user->check_user_update($u,$email,$id);

            if($pass<>$repass || $cekuser == false){
				$this->session->set_flashdata('error','Cek kembali inputan anda!!!.');
				$redirect = base_url().'AdminUser/update/'.$id;
				redirect($redirect);
                //redirect(base_url('AdminUser/update'));
            }else{
				//$passwd = sha1(md5($pass));
                $this->M_user->update_user($u,$fname,$lname,$position,$telepon,$email,$pass,$status,$id);
                $this->session->set_flashdata('sukses','user has been created!!!.');
                redirect(base_url('AdminUser'));
            }

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
}