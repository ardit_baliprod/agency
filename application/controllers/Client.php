<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
        parent::__construct();
        $this->load->model('M_client');
		$this->load->model('M_country');
		$this->load->model('M_contact');
	}
	public function index()
	{
        if($this->session->userdata('bma-agc')){
			$data['client'] = $this->M_client->GetClient()->result();
			
			$this->session->set_userdata('page', 'Client');
			$this->template->load('agency-admin/static','agency-admin/client/data-client',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
	}
	public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
			$data['type']	= "create";
			$data['country'] = $this->M_country->GetCountry()->result();
            $this->session->set_userdata('page', 'Create Client');
			$this->template->load('agency-admin/static','agency-admin/client/form-client',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name				=strip_tags(addslashes($this->input->post('name',TRUE)));
            $phone				=strip_tags(addslashes($this->input->post('phone',TRUE)));
            $address			=strip_tags(addslashes($this->input->post('address',TRUE)));
			$city 				=strip_tags(addslashes($this->input->post('city',TRUE)));
			$state 				=strip_tags(addslashes($this->input->post('state',TRUE)));
			$zip 				=strip_tags(addslashes($this->input->post('zip',TRUE)));
			$website 			=strip_tags(addslashes($this->input->post('website',TRUE)));
            $country 			=addslashes($this->input->post('country'));
            
			$this->M_client->add_customer($name,$phone,$address,$city,$state,$zip,$website,$country);
            $this->session->set_flashdata('sukses','Client has been created!!!.');
            redirect(base_url('Client'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['country'] = $this->M_country->GetCountry()->result();
			$data['client'] = $this->M_client->client($id)->row_array();
			$data['contact'] = $this->M_contact->Contact($id)->result();
            $this->session->set_userdata('page', 'Update Client');
			$this->template->load('agency-admin/static','agency-admin/client/update-client',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name				=strip_tags(addslashes($this->input->post('name',TRUE)));
            $phone				=strip_tags(addslashes($this->input->post('phone',TRUE)));
            $address			=strip_tags(addslashes($this->input->post('address',TRUE)));
			$city 				=strip_tags(addslashes($this->input->post('city',TRUE)));
			$state 				=strip_tags(addslashes($this->input->post('state',TRUE)));
			$zip 				=strip_tags(addslashes($this->input->post('zip',TRUE)));
			$website 			=strip_tags(addslashes($this->input->post('website',TRUE)));
			$country 			=addslashes($this->input->post('country'));
			$id 				=addslashes($this->input->post('id'));
			$status 			=addslashes($this->input->post('status'));

			$this->M_client->update_customer($name,$phone,$address,$city,$state,$zip,$website,$country,$status,$id);
            $this->session->set_flashdata('sukses','Client has been updated!!!.');
            //redirect(base_url('Client'));
            $redirect = base_url() . 'Client/update/' . $id;
            redirect($redirect);

        }
        else{
        	redirect(base_url('AdminLogin')); 
        } 
	}
	public function addcontact()
	{
		if($this->session->userdata('bma-agc') ){
			$clientid				=strip_tags(addslashes($this->input->post('clientid',TRUE)));
            $fname					=strip_tags(addslashes($this->input->post('fname',TRUE)));
            $lname					=strip_tags(addslashes($this->input->post('lname',TRUE)));
			$email 					=strip_tags(addslashes($this->input->post('email',TRUE)));
			$phone 					=strip_tags(addslashes($this->input->post('phone',TRUE)));
			$position 				=strip_tags(addslashes($this->input->post('position',TRUE)));
			$title 					=strip_tags(addslashes($this->input->post('title',TRUE)));
			$primary 				=strip_tags(addslashes($this->input->post('primary',TRUE)));

			$this->M_contact->simpan_contact($fname,$lname,$email,$phone,$position,$title,$primary,$clientid);
			
			echo 'success';


		}else{
			redirect(base_url('AdminLogin'));
		}
	}
	public function updatecontact()
	{
		if($this->session->userdata('bma-agc') ){
			$clientid				=strip_tags(addslashes($this->input->post('clientid',TRUE)));
			$contactid				=strip_tags(addslashes($this->input->post('contactid',TRUE)));
            $fname					=strip_tags(addslashes($this->input->post('fname',TRUE)));
            $lname					=strip_tags(addslashes($this->input->post('lname',TRUE)));
			$email 					=strip_tags(addslashes($this->input->post('email',TRUE)));
			$phone 					=strip_tags(addslashes($this->input->post('phone',TRUE)));
			$position 				=strip_tags(addslashes($this->input->post('position',TRUE)));
			$title 					=strip_tags(addslashes($this->input->post('title',TRUE)));
			$primary 				=strip_tags(addslashes($this->input->post('primary',TRUE)));
			$status 				=strip_tags(addslashes($this->input->post('status',TRUE)));

			$this->M_contact->update_contact($fname,$lname,$email,$phone,$position,$title,$primary,$clientid,$status,$contactid);
			
			$direct = base_url().'Client/update/'.$clientid;
			redirect($direct);

		}else{
			redirect(base_url('AdminLogin'));
		}
	}
    
}