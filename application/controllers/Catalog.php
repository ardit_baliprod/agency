<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
        parent::__construct();
        $this->load->model('M_packages');
        $this->load->model('M_client');
		$this->load->model('M_talent');
		$this->load->model('M_media');
		$this->load->model('M_setting');
		$this->load->model('M_contact');
		$this->load->model('M_catalog');
		$this->data = array(
			'action' => site_url('account/do_login'),
			'username' => set_value('username'),
			'password' => set_value('password'),
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
			'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
		);
	}
	public function index()
	{
		$data = $this->data;
		//$this->load->view('agency-web/static-template');
		$this->session->set_userdata('page', 'Packages Talent');
        $this->template->load('agency-web/static-template','agency-web/packages-old',$data);
	}
	public function talent($id=null){

		$data = $this->data;
		$data['talent'] = $this->M_talent->talentSlug($id)->row_array();
		$data['galleryVideo'] = $this->M_media->GetGalleryVideosSlug($id)->result();
		$galleryVideo = $this->M_media->GetGalleryVideosSlug($id);
		$data['tot_video'] = $galleryVideo->num_rows();
		$data['coverVideo'] = $this->M_media->GetCoverVideoSlug($id)->result();
		$data['galleryImage'] = $this->M_media->GetGalleryImagesSlug($id)->result();
		$data['polaroid'] = $this->M_media->GetPolaroidImagesSlug($id)->result();
		$polaroid = $this->M_media->GetPolaroidImagesSlug($id);
		$data['tot_polaroid'] = $polaroid->num_rows();
		$talent = $this->M_talent->talentSlug($id)->row_array();
		$status = $talent['talent_status'];
		if($status == 1){
			$this->session->set_userdata('page', 'Talent Details');
			$this->template->load('agency-web/static-template','agency-web/talent',$data);
		}else{
			redirect(base_url('Notfound'));
		}	
	} 
	public function package($id=null)
	{
		$data = $this->data;
		$data['packages'] = $this->M_packages->packageEN($id)->row_array();
		$data['PackagesTalent'] = $this->M_packages->EmailPackageTalentEN($id)->result();
		$this->session->set_userdata('page', 'Packages Talent');
        $this->template->load('agency-web/static-template','agency-web/packages',$data);
	}
	public function showcase($id=null)
	{
		$data = $this->data;
		$data['catalog'] = $this->M_catalog->GetCatalogPublic($id)->row_array();
		$data['catalogTalent'] = $this->M_catalog->listTalentEN($id)->result();
		$this->session->set_userdata('page', 'Showcase Talent');
        $this->template->load('agency-web/static-template','agency-web/showcase',$data);
	}
	public function mockup($id=null)
	{ 
		$data = $this->data;
		$this->template->load('agency-web/static-template','agency-web/home-beta',$data);
	}
}
