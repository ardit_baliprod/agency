<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
		parent::__construct();
		$this->load->model('M_category');
	
	}
	public function index()
	{
		if($this->session->userdata('bma-agc')){
			$data['category'] = $this->M_category->GetCategory()->result();
			$this->session->set_userdata('page', 'Category');
			$this->template->load('agency-admin/static','agency-admin/category/data-category',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
    }
    public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "create";
            $this->session->set_userdata('page', 'Create Category');
			$this->template->load('agency-admin/static','agency-admin/category/form-category',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
            $desc 			=addslashes($this->input->post('desc'));
           
			$s	= strtolower($slug);
			
			$this->M_category->create_category($name,$s,$desc);
            $this->session->set_flashdata('sukses','Category has been created!!!.');
            redirect(base_url('Category'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
             
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['category'] = $this->M_category->category($id)->row_array();
            $this->session->set_userdata('page', 'Update Category');
			$this->template->load('agency-admin/static','agency-admin/category/form-category',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
			$desc 			=addslashes($this->input->post('desc'));
			$status 		=$this->input->post('status');
           
			$s				=strtolower($slug);
			$id 			=$this->input->post('id');

			$this->M_category->update_category($name,$s,$desc,$status,$id);
            $this->session->set_flashdata('sukses','Category has been updated!!!.');
            redirect(base_url('Category'));
            

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
	}
	public function delete($id=null)
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            
            //$this->load->view('welcome_message');
            $this->M_category->delete_category($id);
			$this->session->set_flashdata('sukses','Category has been deleted!!!.');
			redirect(base_url('Category'));

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
}
