<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	
	}
	public function index()
	{
        //$this->load->view('welcome_message');
        if($this->session->userdata('bma-agc')){
			$date = date("Y-m-d");
			$start = date("Y-m-d", strtotime("$date -10 days"));
            $data['datestart'] = $start;
			$data['dateend'] = $date;
			$datestart			= $start; 
            $dateend			= $date;

            if($this->input->server('REQUEST_METHOD') === 'POST'){
                $datestart			=$this->input->post('datestart'); 
                $dateend			=$this->input->post('dateend'); 
                
                $data['datestart'] = $datestart;
                $data['dateend'] = $dateend;
			}
			
			$data['history'] = $this->M_log->GetHistory($datestart,$dateend)->result();

            $this->session->set_userdata('page', 'History');
			$this->template->load('agency-admin/static','agency-admin/log/data-log',$data);

		}else{
			redirect(base_url('AdminLogin'));
		}
	}
}