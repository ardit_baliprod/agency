<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Showcase extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
		parent::__construct();
        $this->load->model('M_catalog');
        $this->load->model('M_talent');
        $this->load->model('M_gender');
        $this->load->model('M_category');
        $this->load->model('M_tags');
        
	}
	public function index()
	{
		if($this->session->userdata('bma-agc')){
			$data['showcase'] = $this->M_catalog->GetCatalog()->result();
			$this->session->set_userdata('page', 'Showcase');
			$this->template->load('agency-admin/static','agency-admin/showcase/data-showcase',$data);
		}else{
			redirect(base_url('AdminLogin'));
		}
    }
    public function create()
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
			$data['type']	= "create";
			$data['gender'] = $this->M_gender->GetGenderActive()->result();
            $data['category'] = $this->M_category->GetCategoryActive()->result();
            $data['tags'] = $this->M_tags->GetTags()->result();
            $this->session->set_userdata('page', 'Create Showcase');
			$this->template->load('agency-admin/static','agency-admin/showcase/form-showcase',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function created()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
            $gender 		=addslashes($this->input->post('gender'));
            $category 		=$this->input->post('category');
            $tags 		    =$this->input->post('tags');

            $s	= strtolower($slug);

            $kode = $this->M_catalog->create_catalog($name,$s,$gender,$category,$tags);
            $this->session->set_flashdata('sukses','Showcase has been created!!!.');
            //redirect(base_url('Showcase'));
            
            $redirect = base_url() . 'Showcase/update/' . $kode;
            redirect($redirect);

        }
        else{
        	redirect(base_url('AdminLogin'));
        }
    }
    public function update($id=null)
	{
        if($this->session->userdata('bma-agc')){
            
            //$this->load->view('welcome_message');
            $data['type']	= "update";
            $data['catalog'] = $this->M_catalog->GetUpdateCatalog($id)->row_array();
			$data['gender'] = $this->M_gender->GetGenderActive()->result();
            $data['category'] = $this->M_category->GetCategoryActive()->result();
            $data['tags'] = $this->M_tags->GetTags()->result();
            $data['catalogtags'] = $this->M_catalog->GetCatalogTags($id)->result();
            $this->session->set_userdata('page', 'Create Showcase');
			$this->template->load('agency-admin/static','agency-admin/showcase/update-showcase',$data);

        }
        else{
        	redirect(base_url('AdminLogin'));
        } 
    }
    public function updated()
	{
        if($this->session->userdata('bma-agc') && $this->input->server('REQUEST_METHOD') === 'POST'){
            $name			=strip_tags(addslashes($this->input->post('name',TRUE)));
            $slug			=strip_tags(addslashes($this->input->post('slug',TRUE)));
            $gender 		=addslashes($this->input->post('gender'));
            $category 		=$this->input->post('category');
            $tags 		    =$this->input->post('tags');

            $s	            = strtolower($slug);
            $id 			=$this->input->post('id');

            $kode = $this->M_catalog->update_catalog($name,$s,$gender,$category,$tags,$id);
            $this->session->set_flashdata('sukses','Showcase has been created!!!.');
            //redirect(base_url('Showcase'));
            
            $redirect = base_url() . 'Showcase/update/' . $kode;
            redirect($redirect);

        }
        else{
        	redirect(base_url('AdminLogin'));
        }
    }
    public function LoadTalentOutside($id=null)
	{
		//$id=$this->input->post('id');

		$categoryTalent = $this->M_talent->talentCatalog($id)->result();

		$output = ' ';
		$array = array();
		foreach ($categoryTalent as $c) {
            $talent_id=$c->talent_id;
            $media_url = base_url(). $c->media_url;
			$imgsrc = '<img src="'. $media_url .'" width="200px" height="200px" >';
			$btnadd = '<button type="button" class="btn btn-success" data-toggle="tooltip" onclick="addTalent(\''. $id.'\',\''.$talent_id.'\')"  data-placement="top" data-original-title="Update" ><i class="fa fa-plus"></i> ADD</button>';
			$array[] = array($c->talent_id,$imgsrc,$c->talent_nickname,$c->gender_name,$c->ethnicity_name,$c->categoryname,$c->compelxionname,$c->talent_tag,$btnadd);
			//$output .= $talent_id . ' '.$package_id;
			/*
			$output .= '
			<tr>
				<td>'.$p->talentname.'</td>
				<td><img clas="img" src="'. base_url().$p->media_url .'" width="200px" height="200px" /></td>
				<td>
                    <div class="btn-group">
						<button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$talent_id.'" onclick="confirmDelete(\''.$package_id.'\',\''. $talent_id .'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                    </div>
				</td>
				
            </tr>
			'; */
		}

		//echo $output;
		echo json_encode (array("aaData"=>$array)); //Return the JSON Array
    }
    public function addTalent()
	{
		if($this->session->userdata('bma-agc') ){
			$talent 			=addslashes($this->input->post('talentID'));
			$id 				=addslashes($this->input->post('catalogId'));

			$this->M_catalog->addTalent($talent,$id);
			
			//$this->session->set_flashdata('sukses','Talent data has been added!!!.');
			//$direct = base_url(''). 'Packages/update/'.$id; 
            //redirect($direct);
			echo 'success';

		}else{
			//redirect(base_url('AdminLogin'));
			echo 'error';
		}
    }
    public function ListTalent()
	{
		if($this->session->userdata('bma-agc') ){
            $id 				=addslashes($this->input->post('id'));
            //$id 				= 'CTL0001';

            //$this->M_catalog->listTalent($id);
            $listTalent = $this->M_catalog->listTalent($id)->result();
            $output = ' ';

            foreach ($listTalent as $l) {
            
                $media_url = $l->media_url;
                $talent_id = $l->talent_id;
                $nickname = $l->talent_nickname;
                $full_url = base_url(). $media_url;
                $output .= '<div class="agency-catalog-box ui-state-default" id="catalogTalent_'.$talent_id.'">
                                <div class="agency-catalog-headshot" style="background-image: url(\''.$full_url.'\')" >
                                </div>
                                <br>
                                <p>'.$nickname.'</p>
                                <br>
                                <input type="checkbox" class="gallery-checkbox" value="'.$talent_id.'" />
                                <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="'.$talent_id.'" onclick="confirmDelete(\''.$id.'\',\''.$talent_id.'\')" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                            </div> '; 

                
            }
           
            echo $output;            
            
			//$this->session->set_flashdata('sukses','Talent data has been added!!!.');
			//$direct = base_url(''). 'Packages/update/'.$id; 
            //redirect($direct);
			//echo 'success';

		}else{
			//redirect(base_url('AdminLogin'));
			echo ' ';
		}
    }
    public function RemoveTalent()
	{
		if($this->session->userdata('bma-agc') ){
			$catalogId 		=addslashes($this->input->post('catalogId'));
            $talentID 		=addslashes($this->input->post('talentID'));
            //$catalogId 		='CTL0001';
			//$talentID 		='TLT0004';

			//$this->M_packages->remove_location($location_id,$package_id);
			
			//$this->session->set_flashdata('sukses','Location data has been deleted!!!.');
			//$direct = base_url(''). 'Packages/update/'.$package_id; 
			//redirect($direct);
			$response['status']  = 'error';
			$response['message'] = 'Unable to delete file '.$catalogId.' '.$talentID;

			$catalogTlnt=$this->M_catalog->talentInCatalog($catalogId,$talentID);
		
			$num = $catalogTlnt->num_rows(); 
            
			if($num>0){
				$hasil=$catalogTlnt->row_array();
				
				$this->M_catalog->deleteTalentInCatalog($catalogId,$talentID);
				$response['status']  = 'success';
				$response['message'] = 'Your file has been deleted ...';
			} 

			//echo json_encode($response);
			echo 'success';

		}else{
			//redirect(base_url('Admin_login'));
			$response['status']  = 'error';
			$response['message'] = 'Unable to delete file ';
			//echo json_encode($response);
			echo 'error';
		}
	}
	public function Reorder(){ 
        //$imageids_arr = $_POST['imageids'];
		$imageids_arr=$this->input->post('imageids');
		$catalogtalentID=$this->input->post('catalogtalentID');
		//$catalogtalentID= 'CTL0001';
		//$imageids_arr= array("TLT0001", "TLT0004", "TLT0003");;
		//echo '<script>console.log('.$imageids_arr.');</script>';
		//echo '<script>alert("'.$catalogtalentID.'");</script>';

        // Update sort position of images
        $position = 1;
        foreach($imageids_arr as $id){
			//mysqli_query($con,"UPDATE images_list SET sort=".$position." WHERE id=".$id);
			//echo '<script>alert("UPDATE agc_catalog_talent SET order='.$position.' WHERE catalog_id='.$catalogtalentID.' AND talent_id='.$id.' ");</script>';
            $hsl=$this->db->query("UPDATE agc_catalog_talent SET list_order='$position' WHERE catalog_id='$catalogtalentID' AND talent_id='$id' ");
            $position ++;
        }
        
        echo "Update successfully";
        exit;
	}
	public function removeBulkTalent(){

		//Ambil token foto
		$id=$this->input->post('id');
		$catalogId=$this->input->post('catalogId');
        //$TalentId=$this->input->post('TalentId');
        
        $this->M_catalog->deleteTalentInCatalog($catalogId,$id);

       
        //echo json_encode($response);
        echo 'success';
	}
	
}
