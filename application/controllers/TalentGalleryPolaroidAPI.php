<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class TalentGalleryPolaroidAPI extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_talent');
        $this->load->model('M_media');
     }
     /** 
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get()
	{
        $id = $this->get('id');
        $slug = $this->get('slug');
        
        
        if($slug===null){
            $slug = '';
        }
        

        if($id===null){
            //$listing = $this->M_listing->GetListingTableFilterActive($start,$limit,$city,$category)->result();
            //$data = $this->db->get_where("items", ['id' => $id])->row_array();
            
            $data = $this->M_media->GetPolaroidImagesSlugAPI($slug)->result();
            //$data = $this->M_listing->GetListingTableJsonActive()->result();
        }else{
            //$data = $this->db->get("items")->result();
            $data = $this->M_media->GetPolaroidImagesAPI($id)->result();
        }
     
        if($data){
            $this->response($data, REST_Controller::HTTP_OK);
        }else{
            $this->response([ 'status' => false, 'message' => 'Result not found' ], REST_Controller::HTTP_NOT_FOUND);
        }
        
	}

}
