<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
		parent::__construct();
		
	}
	public function index()
	{
		//$this->load->view('agency-admin/static');
		if($this->session->userdata('bma-agc')){
			//$this->session->set_userdata('page', 'Dashboard');
			//$this->template->load('agency-admin/static','agency-admin/dashboard',$data);
			$this->session->set_userdata('page', 'User');
			$this->template->load('agency-admin/static','agency-admin/dashboard');
		}else{
			redirect(base_url('AdminLogin'));
		}
        
	}
	
}