<?php
$datestring = '%d/%m/%Y ';
//============================================================+
// File name   : example_003.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 003 for TCPDF class
//               Custom Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni 
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+
 
/**
 * Creates an example PDF TEST document using TCPDF 
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).



// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		//$image_file = K_PATH_IMAGES.'logo-upperground.png';
		//$this->Image($image_file, 10, 10, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B');
		// Title
		//$this->Cell(0, 15, 'INVOICE', 0, false, 'R', 0, '', 0, false, 'M', 'M');
		// create some HTML content
		//$html = '<p>INVOICE</p><span style="font-size:8px;font-style: italic;">#INV.09.2018.000007</span><br><span style="font-size:8px;color:green;">LUNAS</span>';

		// output the HTML content
		//$this->writeHTML($html, true, 0, true, 0,'R');
		
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		//$this->SetY(-15);
		// Set font
		//$this->SetFont('helvetica', 'I', 8);
		// Page number
        //$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        //$image_file = K_PATH_IMAGES.'BMA-Logo-Footer.png';
		//$this->Image($image_file, 10, 10, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	}

	
}

// create new PDF document
$pdf = new MYPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Baliprod Location');
$pdf->SetTitle('Package');
$pdf->SetSubject('Package');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
$pdf->SetFooterMargin(0);
 
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', 12);

// add a page
$pdf->AddPage();

// set some text to print
// create some HTML content
// Test fonts nesting



// draw jpeg image
//$pdf->SetAlpha(0.3);

//$image_file = K_PATH_IMAGES.'baliprod-logo.png';
//$pdf->Image($image_file, 100, 100, 150, 150, '', '', '', true, 72);

// restore full opacity
//$pdf->SetAlpha(1);







// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
//$pdf->SetAlpha(0.3);
//$img_file = K_PATH_IMAGES.'Baliprod-Location-PDF-Cover-1.jpg';
$img_file = base_url().'/src/admin_assets/dist/img/BMA-2.0-PDF-Cover.jpg';
$pdf->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();
//$pdf->SetAlpha(1);


$html ='
        <h1 style="font-size:36px;color:#fff;text-transform: uppercase;">Test Compcard</h1>
        ';

// output the HTML content
//$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->writeHTML($html, true, false, true, false, 'C');
$y = 160;
$x = 10;
$pdf->writeHTMLCell('', '', $x, $y, $html, 0, 0, 0, true, 'C', true);

$imageBma = K_PATH_IMAGES .'BMA-Logo-Footer.png';

$pdf->AddPage();
    // get the current page break margin
    $bMargin = $pdf->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $pdf->getAutoPageBreak();
    // disable auto-page-break
    $pdf->SetAutoPageBreak(false, 0);

    $pdf->Image($imageBma, 110, 0, 90, 25, '', '', 'L', false, 300, '', false, false, 0, 'CM', false, false);

    $image1 = base_url() . 'src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-22.jpg';
    $image2 = base_url() . 'src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-110.jpg';
    $image3 = base_url() . 'src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-2.jpg';
    $image4 = base_url() . 'src/admin_assets/dist/img/sasha-p/sasha-p-bma-15.png';
    $image5 = base_url() . 'src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-5.jpg';


    $pdf->Image($image1, '', '', 130, 150, '', '', '', true, 300, '', false, false, 0, 'CM', false, false);
    //$pdf->Image($image2, 155, '', 140, 140, '', '', '', true, 300, '', false, false, 0, 'LM', false, false);
    $pdf->Image($image2, 155, '', 70, 70, '', '', '', true, 300, '', false, false, 0, 'LM', false, false);
    $pdf->Image($image3, 230, '', 70, 70, '', '', '', true, 300, '', false, false, 0, 'LM', false, false);
    $pdf->Image($image4, 155, 107, 70, 70, '', '', '', true, 300, '', false, false, 0, 'LM', false, false);
    $pdf->Image($image5, 230, 107, 70, 70, '', '', '', true, 300, '', false, false, 0, 'LM', false, false);

    $pdf->Image($imageBma, 177, 171, 90, 25, '', '', 'L', false, 300, '', false, false, 0, 'CM', false, false);

    $table ='
        <table cellspacing="0" cellpadding="1" border="0" align="center" width="100%">
            <tr>
                <td width="50%" >
                <p style="font-size:12px;line-height:18px;">Height: 175cm Bust: 85cm Waist: 60cm Hips: 89cm Shoe size: 40 Hair: Brown Eyes: Brown</p>
                </td>
                <td width="50%" >
                <p style="font-size:12px;line-height:18px;"><a href="https://goo.gl/maps/1kordExhA892" style="text-decoration:none;color:#000000;">Jalan Pantai Pererenan 98B Pererenan, Mengwi, Bali 80351, Indonesia</a><a href="tel:+6281338384715" style="text-decoration:none;color:#000000;">+62(0)81338384715</a> | <a href="mailto:contact@balimodelagnecy.com" style="text-decoration:none;color:#000000;">contact@balimodelagnecy.com</a></p>
                </td>
            </tr>
        </table>
        ';

    // output the HTML content
    $pdf->writeHTMLCell('', '', 10, 180, $table, 0, 0, 0, true, 'C', true);
    
    $html ='<p style="text-transform: uppercase;font-size:20px;">Sasha P</p>';

    // output the HTML content
    $pdf->writeHTMLCell('', '', 67, 180, $html, 0, 0, 0, true, 'L', true);

    $pdf->AddPage();

    $pdf->StartTransform();
    $pdf->Rect(0, 0, 200, 300, 'CNZ'); //Clipping mask (CNZ style makes your day)
    $pdf->Image($image1, 0, 0, 300, 400, '', true, '', false, 300);
    //this would actually cut off a 50 units a in each direction.
    $pdf->StopTransform();
    
    $pdf->AddPage();
    // -- set new background ---
    
    // get the current page break margin
    $bMargin = $pdf->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $pdf->getAutoPageBreak();
    // disable auto-page-break
    $pdf->SetAutoPageBreak(false, 0);
    // set bacground image
    //$pdf->SetAlpha(0.3);
    //$img_file = K_PATH_IMAGES.'Baliprod-Location-PDF-Cover-1.jpg';
    $img_file = base_url().'/src/admin_assets/dist/img/BMA-2.0-PDF-Cover-Back.jpg';
    $pdf->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
    // restore auto-page-break status
    $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
    // set the starting point for the page content
    $pdf->setPageMark();
    //$pdf->SetAlpha(1);


//Close and output PDF document
$namafile="Compcard";
$pdf->Output($namafile, 'I');

//============================================================+
// END OF FILE
//============================================================+    
