<?php 
	foreach ($setting as $s) {   
		if($s->option_name=='sitename'){
            $sitename = $s->option_value;
        }
        if($s->option_name=='companyname'){
            $companyname = $s->option_value;
        }
        if($s->option_name=='companyaddress'){
            $companyaddress = $s->option_value;
        }
        if($s->option_name=='companyzipcode'){
            $companyzipcode = $s->option_value;
        }
        if($s->option_name=='companycountry'){
            $companycountry = $s->option_value; 
        }
        if($s->option_name=='companyphone'){ 
            $companyphone = $s->option_value;
        }
        if($s->option_name=='companymail'){
            $companymail = $s->option_value;
		}
		if($s->option_name=='emailfrom'){
            $emailfrom = $s->option_value;
        }
        if($s->option_name=='emailreplyto'){
            $emailreplyto = $s->option_value;
        }
        if($s->option_name=='companyweb'){
          $companyweb = $s->option_value;
        }
        if($s->option_name=='companytextweb'){
          $companytextweb = $s->option_value;
        }
        if($s->option_name=='companyfb'){
          $companyfb = $s->option_value;
        }
        if($s->option_name=='companyig'){
          $companyig = $s->option_value;
        }
        if($s->option_name=='companyyt'){
          $companyyt = $s->option_value;
		}
		if($s->option_name=='companytwitter'){
            $companytwitter = $s->option_value;
          }
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Email</title>
	
</head>
<body>
	

	<table width="100%" border="0" cellspacing="0" cellpadding="30" bgcolor="#FFF">
		<tr>
			<td style="padding: 0px;">
				
				<table width="60%" border="0" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" style="margin: 0px auto;font-size:12px;padding: 0px;" align="center">
					<tr>
			          	<td width="100%" align="center" style="padding: 0px;">
			            	<img src="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-2.0-Mail-Header.jpg" style="width: 100%;">
			            </td>
			          </tr>
				</table>
			</td>
		</tr> 
		<tr>
			<td style="padding: 0px;">
				<table width="60%" border="0" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" style="margin: 0px auto;font-size:12px;padding: 0px;" align="center">
					<tr>
						<td style="padding: 0px;">
							<h1 style="text-align: center;font-size: 30px;margin: 0px;">
								<a href="<?php echo $link; ?>" style="text-decoration:none;color:#000000;"><?php echo $packages['package_name']; ?></a>
							</h1>
							<p style="padding: 20px 0;"><?php echo $messageEmail; ?></p>
						</td>
					</tr>
					<tr>
						<td style="padding: 10px 0;">
							<hr style="width:100%;border-color:#000;border-style:solid;border-width:5px 0 0;clear: both;margin:0 0 20px;height: 0;" />
						</td>
					</tr> 
					<tr>
						<td>
							<table>
                                <tbody>
								<?php 
									$row=0;
									foreach ($talent as $t) {  
										$first_url = base_url(). $t->media_url;
										$row=$row+1;
										if ($row % 3 == 1) {
											echo '<tr>'; 
										 } 
								?>
									<td style="vertical-align:top;width:30%;padding:1.5%;font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;color:#333;text-align:center;margin:5px">
                                       	<a href="<?php echo 'https://www.balimodelagency.com/'.$t->talent_slug; ?>" title="<?= $t->talent_nickname ?>" target="_blank">
                                       		<img src="<?= $first_url ?>" alt="<?= $t->talent_nickname ?>" style="display:inline-block;width:250px;height:370px;object-fit: cover;" class="CToWUd">
                                       	</a>
                                       	<p><?= $t->talent_nickname ?></p>
                                       	<br>
                                    </td>
								<?php 
										if ($row % 3 == 0) {
											echo '</tr>';
										 } 	
									}
									if ($row % 3 <> 0) {
										echo '</tr>';
									 } 	
								?>
                                    			
                                </tbody>
                            </table>	
						</td>

					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="60%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="margin: 0px auto;font-size:12px; text-align: center;font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#000000;" align="center">
                    <tr>
                        <td colspan="2" align="center" style="">
                            
                            <hr style="width:100%;border-color:#000;border-style:solid;border-width:5px 0 0;clear: both;margin:0 0 20px;height: 0;" />
                            
                        </td>
                    </tr>
                    <tr>
        				<td style="width: 60%;text-align: left;">
        					<p style="font-size: x-large;">
        						<a href="<?= $companyweb ?>" style="text-decoration: none;color: #000;"><?= $companytextweb ?></a>
        					</p>
        						
        				</td>
        				<td style="width: 40%;" >
        					<a href="<?= $companyfb ?>" style="margin: 0px 10px;"><img src="https://www.balimodelagency.com/wp-content/uploads/2018/12/Facebook-Icon.png" alt="icon-fb" style="border: none; width: 20px; height: 20px; max-width: 20px; max-height: 20px;" ></a>
        					<a href="<?= $companyig ?>" style="margin: 0px 10px;"><img src="https://www.balimodelagency.com/wp-content/uploads/2018/12/Instagram-Icon.png" alt="icon-ig" style="border: none; width: 20px; height: 20px; max-width: 20px; max-height: 20px;"></a>
        					<a href="<?= $companyyt ?>" style="margin: 0px 10px;"><img src="https://www.balimodelagency.com/wp-content/uploads/2019/01/icon-youtube.png" alt="icon-yt" style="border: none; width: 20px; height: 20px; max-width: 20px; max-height: 20px;"></a>
							
        				</td>
        			</tr>
        			
            	</table>	
			</td>
			
		</tr>
	</table>
</body>
</html>