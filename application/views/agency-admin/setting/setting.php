<?php
  
  $sitename = '';
  $companyname = '';
  $companyaddress = '';
  $companyzipcode = '';
  $companycountry = '';
  $companyphone = '';
  $companymail = '';
  $stmphost = '';
  $stmpauth = '';
  $stmpusername = '';
  $stmppassword = '';
  $stmpsecure = '';
  $stmpport = '';
  $emailfrom = '';
  $emailreplyto = '';
  $companyweb = '';
  $companytextweb = '';
  $companyfb = '';
  $companyig = '';
  $companyyt = '';
  $companytwitter = '';
  $sendgrid_api = '';

    foreach ($setting as $s) {   
        if($s->option_name=='sitename'){
            $sitename = $s->option_value;
        }
        if($s->option_name=='companyname'){
            $companyname = $s->option_value;
        }
        if($s->option_name=='companyaddress'){
            $companyaddress = $s->option_value;
        }
        if($s->option_name=='companyzipcode'){
            $companyzipcode = $s->option_value;
        }
        if($s->option_name=='companycountry'){
            $companycountry = $s->option_value;
        }
        if($s->option_name=='companyphone'){
            $companyphone = $s->option_value;
        }
        if($s->option_name=='companymail'){
            $companymail = $s->option_value;
        }
        if($s->option_name=='stmphost'){
            $stmphost = $s->option_value;
        }
        if($s->option_name=='stmpauth'){
            $stmpauth = $s->option_value;
        }
        if($s->option_name=='stmpusername'){
            $stmpusername = $s->option_value;
        }
        if($s->option_name=='stmppassword'){
            $stmppassword = $s->option_value;
        }
        if($s->option_name=='stmpsecure'){
            $stmpsecure = $s->option_value;
        }
        if($s->option_name=='stmpport'){
            $stmpport = $s->option_value;
        }
        if($s->option_name=='emailfrom'){
            $emailfrom = $s->option_value;
        }
        if($s->option_name=='emailreplyto'){
            $emailreplyto = $s->option_value;
        }
        if($s->option_name=='companyweb'){
          $companyweb = $s->option_value;
        }
        if($s->option_name=='companytextweb'){
          $companytextweb = $s->option_value;
        }
        if($s->option_name=='companyfb'){
          $companyfb = $s->option_value;
        }
        if($s->option_name=='companyig'){
          $companyig = $s->option_value;
        }
        if($s->option_name=='companyyt'){
          $companyyt = $s->option_value;
        }
        if($s->option_name=='companytwitter'){
          $companytwitter = $s->option_value;
        }
        if($s->option_name=='sendgrid_api'){
          $sendgrid_api = $s->option_value;
        }
             
    }
    $judul ='Setting';
 
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?= $judul ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        
        <div class="row">
          <div class="col-12">
            <h4>Setting</h4>
          </div>
        </div>
        <!-- ./row -->
        <div class="row">
          <div class="col-12 col-sm-12 col-lg-12">
            <div class="card card-primary">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">System</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Email</a>
                  </li>
                  
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                  <?php if($this->session->flashdata('sukses')) { ?>
                              <div class="alert alert-success alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                  <h4><i class="icon fa fa-check"></i> Success!</h4>
                                  <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                              </div>
                  <?php } ?>
                    <form class="form-horizontal" method="post" action="<?php echo base_url('Setting/system'); ?>" >
                      <div class="card-body">
                        <div class="form-group row">
                          <label for="inputName" class="col-sm-2 col-form-label">Site Name</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputName" name="sitename" placeholder="Site Name" <?php echo 'value="'.$sitename .'"'; ?>  >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPhone" class="col-sm-2 col-form-label">Company Name</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPhone" name="companyname" placeholder="Company Name" <?php echo 'value="'.$companyname .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputAdress" class="col-sm-2 col-form-label">Company Address</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputAdress" name="address" placeholder="Company Address" <?php echo 'value="'.$companyaddress .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputCity" class="col-sm-2 col-form-label">ZIP Code</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputCity" name="zipcode" placeholder="ZIP Code" <?php echo 'value="'.$companyzipcode .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputState" class="col-sm-2 col-form-label">Company Phone</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputState" name="phone" placeholder="Company Phone" <?php echo 'value="'.$companyphone .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputZIP" class="col-sm-2 col-form-label">Company Email</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputZIP" name="email" placeholder="Company Email" <?php echo 'value="'.$companymail .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputCountry" class="col-sm-2 col-form-label">Country</label>
                          <div class="form-group col-sm-10">
                            <select class="form-control select2" name="country" style="width: 100%;">
                              <option value="" >Select Country</option>
                            <?php 
                                foreach ($country as $c) { 
                                    $country_id=$c->country_id;
                                    $name=$c->short_name;
                            ?>
                              <option value="<?= $name ?>" <?php if($name==$companycountry){ echo 'selected'; } ?> ><?= $name ?></option>
                                <?php } ?>
                              
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputWebsite" class="col-sm-2 col-form-label">Website</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputWebsite" name="website" placeholder="Website" <?php echo 'value="'.$companyweb .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputWebsite" class="col-sm-2 col-form-label">Text Website</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputtextWebsite" name="textwebsite" placeholder="Text Website" <?php echo 'value="'.$companytextweb .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputWebsite" class="col-sm-2 col-form-label">Facebook</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputFacebook" name="facebook" placeholder="Facebook" <?php echo 'value="'.$companyfb .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputWebsite" class="col-sm-2 col-form-label">Instagram</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputig" name="ig" placeholder="Instagram" <?php echo 'value="'.$companyig .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputWebsite" class="col-sm-2 col-form-label">Youtube</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputYt" name="youtube" placeholder="Youtube" <?php echo 'value="'.$companyyt .'"'; ?> >
                          </div>
                        </div>       
                        <div class="form-group row">
                          <label for="inputWebsite" class="col-sm-2 col-form-label">Twitter</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputTwit" name="twitter" placeholder="Twitter" <?php echo 'value="'.$companytwitter .'"'; ?> >
                          </div>
                        </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                      </div>
                      <!-- /.card-footer -->
                    </form>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                    <form class="form-horizontal" method="post" action="<?php echo base_url('Setting/email'); ?>" >
                      <div class="card-body">
                        <div class="form-group row">
                          <label for="inputName" class="col-sm-2 col-form-label">STMP host</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputhost" name="stmphost" placeholder="STMP host" <?php echo 'value="'.$stmphost .'"'; ?>  >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPhone" class="col-sm-2 col-form-label">STMP auth</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputAuth" name="stmpauth" placeholder="STMP auth" <?php echo 'value="'.$stmpauth .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputAdress" class="col-sm-2 col-form-label">Username</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputUser" name="username" placeholder="Username" <?php echo 'value="'.$stmpusername .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputCity" class="col-sm-2 col-form-label">Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputPass" name="password" placeholder="Password" <?php echo 'value="'.$stmppassword .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputState" class="col-sm-2 col-form-label">STMP Secure</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSecure" name="stmpsecure" placeholder="STMP Secure" <?php echo 'value="'.$stmpsecure .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputState" class="col-sm-2 col-form-label">STMP Port</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPort" name="stmpport" placeholder="STMP Port" <?php echo 'value="'.$stmpport .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputZIP" class="col-sm-2 col-form-label">Email From</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputEmailFrom" name="emailfrom" placeholder="Email From" <?php echo 'value="'.$emailfrom .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputZIP" class="col-sm-2 col-form-label">Email Reply To</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputEmailReply" name="emailreply" placeholder="Email Reply To" <?php echo 'value="'.$emailreplyto .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputZIP" class="col-sm-2 col-form-label">Sendgrid API</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputSendgrid" name="sendgrid" placeholder="Sendgrid API" <?php echo 'value="'.$sendgrid_api .'"'; ?> >
                          </div>
                        </div>      
                        
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                      </div>
                      <!-- /.card-footer -->
                    </form>
                  </div>
                  
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

                          <div class="modal fade" id="modal-xl">
                            <div class="modal-dialog modal-xl">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Add Contact</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <form>
                                    <div class="form-group">
                                      <label for="fname">First Name</label>
                                      <input type="text" class="form-control" id="fname" placeholder="First Name" required autofocus>
                                      <input type="hidden" class="form-control" id="clientid" value="<?php echo $client_id; ?>" >
                                    </div>
                                    <div class="form-group">
                                      <label for="lname">Last Name</label>
                                      <input type="text" class="form-control" id="lname" placeholder="Last Name" required autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="alamat">Title</label>
                                      <select class="form-control" id="title" >
                                          <option value="Mr">Mr</option>
                                          <option value="Mrs">Mrs</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="email">Email</label>
                                      <input type="email" class="form-control" id="email" placeholder="Email" required autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="phone">Phone</label>
                                      <input type="text" class="form-control" id="phone" placeholder="Phone" required autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="position">Position</label>
                                      <input type="text" class="form-control" id="position" placeholder="Position" required autofocus>
                                    </div>
                                    <div class="form-check">
                                      <input type="checkbox" class="form-check-input" id="isprimary">
                                      <label for="position">Is Primary</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary" id="save">save</button>
                                  </form>
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  
                                </div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
                          
                         


<script>

//insert data
$('#save').on('click',function(){
            
            var clientid  =$('#clientid').val();
            var fname     =$('#fname').val();
            var lname     =$('#lname').val();
            var email     =$('#email').val();
            var phone     =$('#phone').val();
            var position  =$('#position').val();
            var f         = document.getElementById("title");
            var title     = f.options[f.selectedIndex].value;
            var primary   = document.getElementById("isprimary").checked;

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>Customer/addcontact",
                data : {clientid:clientid , fname:fname, lname:lname,email:email,phone:phone,position:position,title:title,primary:primary},
                success: function(data){
                    $('#clientid').val('');
                    $('#fname').val('');
                    $('#lname').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('#position').val('');
                    $('.modal').modal('hide');
                    Swal.fire({
                      title: 'Good Job!',
                      text: "Data contact succesfully added!!",
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'OK!'
                    }).then((result) => {
                      if (result.value) {
                        window.location.reload(true);
                      }
                    })
                }
            });
        });

       


</script>