<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Log System</h1>
            <br/>
            
            
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Log System</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->

    </section>
    <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
    <?php } ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data History</h3>
            </div>
            <!-- /.card-header -->
            
            <div class="card-body">
              <form class="form-horizontal" method="post" autocomplete="off" enctype="multipart/form-data" action="<?php echo base_url(); ?>History">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group">
                            <input type="text" class="form-control" id="datestart" name="datestart" <?php if($datestart<>''){ echo 'value="'.$datestart .'"'; } ?> placeholder="Date From">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        
                        <div class="form-group">
                            <div class="input-group">
                            <input type="text" class="form-control" id="dateend" name="dateend" <?php if($dateend<>''){ echo 'value="'.$dateend .'"'; } ?> placeholder="Date To">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary" id="filter"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                    
                    
                </div>
              </form>
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Date</th>
                  <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $i=0;

                    foreach ($history as $h) { 
                        $i++;
                        $datestring = '%d %M %Y - %h:%i:%s'; 
                ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?= $h->username ?></td>
                  <td><?= $h->fname .' '. $h->lname ?></td>
                  <td><?= mdate($datestring, strtotime($h->date_in)) ?></td>
                  <td><?= $h->description ?></td>
                 
                  
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Date</th>
                  <th>Description</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
</script>