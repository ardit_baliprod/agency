<?php
  if ($type == 'update'){
    $id_user = stripslashes($user['id_user']);
    $username = stripslashes($user['username']);
    $fname = stripslashes($user['fname']); 
    $lname = stripslashes($user['lname']); 
    $position = stripslashes($user['position']);
    $phone = stripslashes($user['phone']);
    $email = stripslashes($user['email']);
    $status = stripslashes($user['status']);

    $judul ='Update User';
  }
  else{
    $judul ='Add User';
    $position = '';
  } 
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>AdminUser">Data Users</a></li>
              <li class="breadcrumb-item active">User Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><?= $judul ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" <?php if($type=='create'){ ?> action="<?php echo base_url('AdminUser/created'); ?>" <?php } else { ?> action="<?php echo base_url('AdminUser/updated'); ?>" <?php } ?>>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputFname" class="col-sm-2 col-form-label">First Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputFname" name="fname" required placeholder="First Name" <?php if($type == 'update'){ echo 'value="'.$fname .'"'; } ?> >
                      <?php if($type == 'update') { echo form_hidden('id', $id_user); } ?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputLname" class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputLname" name="lname" placeholder="Last Name" <?php if($type == 'update'){ echo 'value="'.$lname .'"'; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputLname" class="col-sm-2 col-form-label">Position</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="position">
                          <option>Select Position</option>
                          <option value="Administrator" <?php if($position == 'Administrator'){ echo 'selected'; } ?> >Administrator</option>
                          <option value="Editor" <?php if($position == 'Editor'){ echo 'selected'; } ?> >Editor</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" name="email" required placeholder="Email" <?php if($type == 'update'){ echo 'value="'.$email .'"'; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPhone" class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone" <?php if($type == 'update'){ echo 'value="'.$phone .'"'; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputUsername" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputUsername" name="username" placeholder="Username" <?php if($type == 'update'){ echo 'value="'.$username .'" '; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword1" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputPassword1" name="pass" <?php if($type == 'create') { echo 'requried'; }?> placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword2" class="col-sm-2 col-form-label">Re-Type Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputPassword2" name="repass" <?php if($type == 'create') { echo 'requried'; }?> placeholder="Password">
                    </div>
                  </div>
                  <?php if($type == 'update') { ?>
                        <div class="form-group row">
                        <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                        
                            <div class="col-sm-10">
                                <select class="form-control" name="status" >
                                    <option <?php if($status==1) { echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($status==0) { echo "selected"; } ?> value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->