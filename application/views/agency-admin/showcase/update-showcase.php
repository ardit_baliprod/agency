<?php
  if ($type == 'update'){
    $catalog_id             = stripslashes($catalog['catalog_id']);
    $catalog_name           = stripslashes($catalog['catalog_name']); 
    $catalog_slug           = stripslashes($catalog['catalog_slug']);
    $catalog_status         = stripslashes($catalog['catalog_status']); 
    $category_id_select     = stripslashes($catalog['category_id']);
    $gender_id_select       = stripslashes($catalog['gender_id']);
    $link=sha1(md5($catalog_id));

    $judul ='Update Showcase';
  }
  else{
    $judul ='Add Showcase';
    $client = '';
  }  
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Showcase">Data Showcase</a></li>
              <li class="breadcrumb-item active"><?= $judul ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid --> 
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
              <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-lg-12 col-sm-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-edit"></i>
                  <?= $judul ?> 
                </h3>
              </div>
              <div class="card-body">
                
                <!-- <h4 class="">Custom Content Above</h4> -->
                <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Details</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Talent</a>
                  </li>
                  
                </ul>
                <div class="tab-custom-content">
                    <div class="btn-group">
                      <!--<button type="button" class="btn btn-primary" data-toggle="tooltip" onclick="sendmail('<?php //echo $package_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-envelope"></i></button> -->
                      <button type="button" class="btn btn-info" data-toggle="tooltip" onclick="Newtab('<?php echo base_url('Catalog/showcase/').$link; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-eye"></i></button>
                      
                    </div>
                </div>
                <div class="tab-content" id="custom-content-above-tabContent">
                  <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="<?php echo base_url('Showcase/updated'); ?>" >
                        <div class="card-body">
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputCatalogName" name="name" onkeyup="sync2()" placeholder="Name" value="<?= $catalog_name ?>"  >
                            <?php if($type == 'update') { echo form_hidden('id', $catalog_id); } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputSlug" class="col-sm-2 col-form-label">Slug</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSlug" name="slug" placeholder="slug" style="text-transform: lowercase" value="<?= $catalog_slug ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputCountry" class="col-sm-2 col-form-label">Category</label>
                            <div class="form-group col-sm-10">
                            <select class="form-control select2" name="category" style="width: 100%;">
                                <option value="" >Select Category</option>
                                <option <?php if($category_id_select == '-'){ echo 'selected'; } ?> value="-" >None</option>
                            <?php 
                                foreach ($category as $c) { 
                                    $category_id=$c->category_id;
                                    $name=$c->category_name;
                            ?>
                                <option <?php if($category_id_select == $category_id){ echo 'selected'; } ?> value="<?= $category_id ?>"><?= $name ?></option>
                                <?php } ?>
                                
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputCountry" class="col-sm-2 col-form-label">Gender</label>
                            <div class="form-group col-sm-10">
                            <select class="form-control select2" name="gender" style="width: 100%;">
                                <option value="" >Select Gender</option>
                                <option <?php if($gender_id_select == '-'){ echo 'selected'; } ?> value="-" >None</option>
                            <?php 
                                foreach ($gender as $g) { 
                                    $gender_id=$g->gender_id;
                                    $name=$g->gender_name;
                            ?>
                                <option <?php if($gender_id_select == $gender_id){ echo 'selected'; } ?> value="<?= $gender_id ?>"><?= $name ?></option>
                                <?php } ?>
                                
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="City" class="col-sm-2 col-form-label">Tags</label>
                            <div class="col-sm-10">
                                <select class="form-control select2" multiple="multiple" name="tags[ ]" style="width: 100%;">
                                <?php 
                                foreach ($tags as $t) { 
                                    $include = false;
                                    $id_tags_select=$t->id_tags;
                                    $name=$t->name_tags;
                                    foreach ($catalogtags as $ct) {
                                        if($id_tags_select == $ct->id_tags)
                                        {
                                          $include = true;
                                        } 
                                    }
                                ?>
                                    <option <?php if($include == true){ echo 'selected'; } ?> value="<?= $id_tags_select ?>"><?= $name ?></option>
                                <?php } ?>
                                        
                                </select> 
                            </div>
                        </div>
                        
                        
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                  </div>
                  <div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                    <input type="hidden" class="form-control" id="catalogtalentID" name="catalogtalentID" <?php  echo 'value="'.$catalog_id .'"'; ?>  >
                    <a data-toggle="modal" href="#modal-xl" class="btn btn-app bg-gradient-primary" style="color:#fff;">
                        <i class="fas fa-plus"></i> Add Talent
                    </a>
                    <br/>
                    <br/>
                    <div class="agency-catalog-wrapper" id="sortableTalent">
                       
                    </div>
                    <div class="row" style="clear: both; margin-top: 20px;">
                      <button type="button" class="btn btn-lg btn-success" id="SubmitOrderCatalog">Save</button>
                      &nbsp;
                      <button type="button" class="btn btn-lg btn-danger" id="DeleteCatalog">Delete Selection</button>
                    </div>
                  
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.card -->
          </div>
        </div>  
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

                        <div class="modal " id="modal-xl">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Add Talent</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <table id="example6" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th>Talent ID</th>
                                      <th>Talent Headshot</th>
                                      <th>Talent Name</th>
                                      <th>Gender</th>
									                    <th>Ethnicity</th>
                                      <th>Category</th>
                                      <th>Compelxion</th>
                                      <th>Tags</th>
									                    <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody >
                                
                                  </tbody>
                                </table>  
                              </div>
                              <div class="modal-footer justify-content-between">
                                
                              </div>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        
<script>
 $(document).ready(function () {
    var inputTalentID  =$('#catalogtalentID').val(); 
    GetCatalogTalent(inputTalentID);  
    LoadCatalogTalent(inputTalentID);
  });
function LoadCatalogTalent(id){
  $.ajax({
        url:"<?php echo site_url('Showcase/ListTalent'); ?>",
        method :"POST",
        data : {id:id},
        success:function(data){
          $('#sortableTalent').html(data);
        }
  });
}  
function GetCatalogTalent(id){
  
  $('#example6').DataTable( {
          "ajax": "<?php echo site_url('Showcase/LoadTalentOutside/'); ?>"+id,
          "paging": true,
          "lengthChange": true,
          "order": [[ 2, "asc" ]],
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "columnDefs": [
              {
                  "targets": [ 0 ],
                  "visible": false,
                  "searchable": false
              }
          ],
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    } );
}

function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
function Newtab(name) {
  //alert("Welcome " + name + ".");
  //window.location.replace(name);
  window.open(name, '_blank');
}
function sync()
{
  //var n1 = document.getElementById('inputname');
  //var n2 = document.getElementById('inputSlug');
  //n2.value = n1.value;
  //alert($("#inputCatalogName").val());
}
function sync2()
{
    $("#inputCatalogName").copyTo("#inputSlug");
}
$.fn.copyTo = function(selector) {
    $(selector).val($(this[0]).val().replace(/\s/g, "-"));
    
};
function addTalent(catalogId,talentID){
  $.ajax({
        url  : "<?php echo base_url()?>Showcase/addTalent",
        data : {catalogId:catalogId , talentID:talentID},
        method : "POST",
        
         success :function(data){
                
          $('#example6').DataTable().ajax.reload();
          LoadCatalogTalent(catalogId);
        }
    });

}
function confirmDelete(catalogId,talent_id) {
  $.ajax({
        url: '<?php echo base_url();?>Showcase/RemoveTalent',
        method : "POST",
        data:{
          catalogId: catalogId,talentID: talent_id
            },
         success :function(data){
                
          $('#example6').DataTable().ajax.reload();
          LoadCatalogTalent(catalogId); 
        }
    });
}
$('#DeleteCatalog').click(function(){
    //alert('sukses');
    var catalogtalentID =$('#catalogtalentID').val();
    var checkboxes = document.querySelectorAll('input.gallery-checkbox:checked');
    for(var i=0;i<checkboxes.length;i++){
        //alert(checkboxes[i].value); 
        var id = checkboxes[i].value;
        $.ajax({
        url : "<?php echo site_url('Showcase/removeBulkTalent');?>",
            method : "POST",
            data : {id : id,catalogId: catalogtalentID},
            success :function(data){
                              
            }
    });
    
    }
    $('#example6').DataTable().ajax.reload();
    LoadCatalogTalent(catalogtalentID); 
    /*
    if ($('input.gallery-checkbox').is(':checked')) {
        //blah blah
        alert($('input.gallery-checkbox:checked').val());
    } 
    if(checkboxes[i].is(':checked')){
           alert(checkboxes[i].val()); 
        }
    */
});

</script>
