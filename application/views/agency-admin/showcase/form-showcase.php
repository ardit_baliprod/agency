<?php
  if ($type == 'update'){
    $username = $user['username'];
    $fname = $user['fname']; 
    $lname = $user['lname']; 
    $position = $user['position'];
    $phone = $user['phone'];
    $email = $user['email'];
    $status = $user['status'];

    $judul ='Update Package';
  }
  else{
    $judul ='Add Showcase';
    $position = '';
  }  
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Showcase">Data Showcase</a></li>
              <li class="breadcrumb-item active"><?= $judul ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><?= $judul ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php echo base_url('Showcase/created'); ?>" >
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputCatalogName" name="name" onkeyup="sync2()" placeholder="Name"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputSlug" class="col-sm-2 col-form-label">Slug</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputSlug" name="slug" placeholder="slug" style="text-transform: lowercase"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputCountry" class="col-sm-2 col-form-label">Category</label>
                    <div class="form-group col-sm-10">
                      <select class="form-control select2" name="category" style="width: 100%;">
                        <option value="" >Select Category</option>
                        <option value="-" >None</option>
                      <?php 
                          foreach ($category as $c) { 
                              $category_id=$c->category_id;
                              $name=$c->category_name;
                      ?>
                        <option value="<?= $category_id ?>"><?= $name ?></option>
                          <?php } ?>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputCountry" class="col-sm-2 col-form-label">Gender</label>
                    <div class="form-group col-sm-10">
                      <select class="form-control select2" name="gender" style="width: 100%;">
                        <option value="" >Select Gender</option>
                        <option value="-" >None</option>
                      <?php 
                          foreach ($gender as $g) { 
                              $gender_id=$g->gender_id;
                              $name=$g->gender_name;
                      ?>
                        <option value="<?= $gender_id ?>"><?= $name ?></option>
                          <?php } ?>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="City" class="col-sm-2 col-form-label">Tags</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" multiple="multiple" name="tags[ ]" style="width: 100%;">
                        <?php 
                          foreach ($tags as $t) { 
                              $id_tags=$t->id_tags;
                              $name=$t->name_tags;
                        ?>
                            <option value="<?= $id_tags ?>"><?= $name ?></option>
                        <?php } ?>
                                
                        </select> 
                    </div>
                  </div>
                  
                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function sync()
{
  //var n1 = document.getElementById('inputname');
  //var n2 = document.getElementById('inputSlug');
  //n2.value = n1.value;
  //alert($("#inputCatalogName").val());
}
function sync2()
{
    $("#inputCatalogName").copyTo("#inputSlug");
}
$.fn.copyTo = function(selector) {
    $(selector).val($(this[0]).val().replace(/\s/g, "-"));
    
};
</script>