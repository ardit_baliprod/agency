<?php
  $user=$this->session->userdata('user');
  $name=$this->session->userdata('name');
  $position=$this->session->userdata('position');
  $page=$this->session->userdata('page');
  $titlePage = "Balimodelagnecy Apps | ".$page;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $titlePage ?></title>
  <meta name="description" content="Balimodelagency apps">
  <meta name="keywords" content="model, agency, bali">
  <meta name="author" content="BMA">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
  <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
  <link rel="shortcut icon" href="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
  <link rel="apple-touch-icon" href="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
  
  
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/fontawesome-free/css/all.min.css">
  <!-- pace-progress -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/pace-progress/themes/black/pace-theme-flat-top.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/dist/css/adminlte.min.css"> 
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/summernote/summernote-bs4.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- jQuery UI 1.11.4 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/jquery-ui/jquery-ui.min.css">
  <!-- bootstrap tokenfield -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
        
  
  <!-- pace-progress -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/pace-progress/themes/black/pace-theme-flat-top.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- jQuery -->
   <script src="<?php echo base_url(); ?>src/admin_assets/plugins/jquery/jquery.min.js"></script> 
  <!-- <script
            data-require="jquery@2.1.3"
            data-semver="2.1.3"
            src="http://code.jquery.com/jquery-2.1.3.min.js"
        ></script> -->
  <!-- jQuery UI 1.11.4 -->
  <script src="<?php echo base_url(); ?>src/admin_assets/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <!-- Dropzone -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" />
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>

  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>
  <style>
    .galleryTalentImage{ 
      width: 100%;
      height: 200px;
      background-size: contain;
      background-position: center top;
      background-repeat: no-repeat;
    }
    body{
      font-family:'Nunito sans';
    }
    .agency-catalog-wrapper {
      width: 90%;
      margin: 0 auto;
      display: grid;
      grid-gap: 10px;
      grid-template-columns: repeat(5, 20%);
    }
    .agency-catalog-box {
      width: 100%;
      height: auto;
      position: relative;
      overflow: hidden;
    }
    .agency-catalog-headshot{
      width: 100%;
      height: 300px;
      background-size: cover;
      background-position: center top;
      background-repeat: no-repeat;
    }
  </style>

</head>
<body class="hold-transition sidebar-mini layout-fixed pace-primary">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    
    
  </nav>
  <!-- /.navbar -->

  <?php
  require_once ("sidebar.php");
  echo $contents;
  ?>

  
  <footer class="main-footer">
    <strong>Copyright &copy; 2019 Bali Model Agency.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 0.1
    </div>
  </footer>

  
</div>
<!-- ./wrapper -->

<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>src/admin_assets/dist/js/adminlte.js"></script>
<!-- pace-progress -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/pace-progress/pace.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- bootstrap tokenfield -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>  
<!-- DataTables -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<<!-- Select2 -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/select2/js/select2.full.min.js"></script>
<!-- Afterglow Player -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/afterglow/afterglow.min.js"></script>

<!-- page script -->
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote({
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link']],
        ['view', ['fullscreen', 'codeview', 'help']],
        ['height', ['height']]
      ]
    });

    var popup_btn = $('.popup-btn');
         popup_btn.magnificPopup({
         type : 'image',
         gallery : {
         	enabled : true
         }
         });
    // Initialize sortable
    $("#sortable").sortable();
    $( "#sortable" ).disableSelection();
    $("#sortablePolaroid").sortable();
    $( "#sortablePolaroid" ).disableSelection();
    $("#sortableTalent").sortable();
    $( "#sortableTalent" ).disableSelection();
    $("#sortablepackage").sortable();
    $( "#sortablepackage" ).disableSelection();
    //Initialize Select2 Elements
    $('.select2').select2();
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
    $('#example3').DataTable({
      "paging": false
    });
    
    
     $('#birth').daterangepicker({
      locale: {
        format: 'YYYY-MM-DD'
      }, 
      singleDatePicker: true,
      showDropdowns: true,
      minYear: 1901,
      maxYear: parseInt(moment().format('YYYY'),10)
    }, function(start, end, label) {
      var years = moment().diff(start, 'years');
      $('#inputAge').val(years);
      //alert("You are " + years + " years old!");
    });  
    
      //Date picker
    $('#datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    })
    //Date picker
    $("#datestart").datepicker({
      autoclose: true,
      todayHighlight: true,
      todayBtn: true,
      format: "yyyy-mm-dd",
    }).on('changeDate', function (selected) {
      var startDate = new Date(selected.date.valueOf());
      $('#dateend').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
      $('#dateend').datepicker('setStartDate', null); 
    });
    
    $("#dateend").datepicker({
      autoclose: true,
      todayHighlight: true,
      todayBtn: true,
      format: "yyyy-mm-dd",
    }).on('changeDate', function (selected) {
      var endDate = new Date(selected.date.valueOf());
      $('#datestart').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
      $('#datestart').datepicker('setEndDate', null);
    });
    //Date time picker
    $('#datestartproject').datetimepicker({
      
      format:'YYYY-MM-DD HH:mm:ss',
      icons:{
      time:'far fa-clock'
      },
      useCurrent: false
    });
    $('#dateendproject').datetimepicker({
      format:'YYYY-MM-DD HH:mm:ss',
      icons:{
      time:'far fa-clock'
      },
      useCurrent: false
    });
    $("#datestartproject").on("change.datetimepicker", function (e) {
      $('#dateendproject').datetimepicker('minDate', e.date);
      
    });
    $("#dateendproject").on("change.datetimepicker", function (e) {
      $('#datestartproject').datetimepicker('maxDate', e.date);
      
    });
    
    
    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'DD/MM/YYYY HH:mm:ss', 
      useCurrent: false,
      showTodayButton: true,
      showClear: true,
      toolbarPlacement: 'bottom',
      sideBySide: true,
      
    })
    $('#addEmail').tokenfield();
    $('#search_data').tokenfield({
        autocomplete :{
            source: function(request, response)
            {
                jQuery.get("<?php echo site_url('Talent/getTags'); ?>", {
                    query : request.term
                }, function(data){
                    data = JSON.parse(data);
                    response(data);
                });
            },
            delay: 100
        }
    });
    
    $('#exampleInputFile').on('change',function(){
                //get the file name
                var fileName = $(this).val(); 
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            });
            $('#VideoInputFile').on('change',function(){
                //get the file name
                var fileName = $(this).val(); 
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            });
            $('#embedVideoUpload').on('change',function(){
                //get the file name
                var fileName = $(this).val(); 
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            });
            $('#coverUpload').on('change',function(){
                //get the file name
                var fileName = $(this).val(); 
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            });
    list_image();
     
      function list_image()
      {
        var inputTalentID  =$('#inputTalentID').val();
        $.ajax({
        url:"<?php echo site_url('Talent/ListImageGallery'); ?>",
        method :"POST",
        data : {inputTalentID:inputTalentID},
        success:function(data){
          $('#sortable').html(data);
        }
        });
      }

      LoadPackageTable();
     
      function LoadPackageTable()
      {
        var inputTalentID  =$('#packagetalentID').val();
        $.ajax({
        url:"<?php echo site_url('Packages/LoadPackage'); ?>",
        method :"POST",
        data : {id:inputTalentID},
        success:function(data){
          $('#packageTalent').html(data);
          //alert(data);
        }
        });
      }

      LoadPackageTalent();
     
      function LoadPackageTalent()
      {
        var inputTalentID  =$('#packagetalentID').val();
        $.ajax({
        url:"<?php echo site_url('Packages/LoadPackageTalent'); ?>",
        method :"POST",
        data : {id:inputTalentID},
        success:function(data){
          $('#talentSearchPackage').html(data);
          //alert(data);
        }
        });
      }

      list_image_polaroid();
     
      function list_image_polaroid()
      {
        var inputTalentID  =$('#inputPolaroidTalentID').val();
        $.ajax({
        url:"<?php echo site_url('Talent/ListImagePolaroid'); ?>",
        method :"POST",
        data : {inputTalentID:inputTalentID},
        success:function(data){
          $('#sortablePolaroid').html(data);
        }
        });
      }
      list_video();
      function list_video()
      {
        var inputLocationID  =$('#inputLocationID').val();
        $.ajax({
        url:"<?php echo site_url('Listing/ListVideo'); ?>",
        method :"POST",
        data : {inputLocationID:inputLocationID},
        success:function(data){
          $('#previewVideo').html(data);
        }
        });
      } 
      // Save order
      $('#SubmitOrder').click(function(){
            var imageids_arr = [];
            // get image ids order
            $('#sortable .ui-state-default').each(function(){
                var id = $(this).attr('id');
                var split_id = id.split("_");
                imageids_arr.push(split_id[1]);
                //alert(split_id[1]);
            });

            // AJAX request
            
            $.ajax({
                url: '<?php echo site_url('Talent/Reorder'); ?>',
                type: 'post',
                data: {imageids: imageids_arr},
                success: function(response){
                  Swal.fire(
                        'Good job!',
                        'Photo has been reorder!!!',
                        'success'
                      );
                }
            });
        });
        // Save order
      $('#SubmitOrderPolaroid').click(function(){
            var imageids_arr = [];
            // get image ids order
            $('#sortablePolaroid .ui-state-default').each(function(){
                var id = $(this).attr('id');
                var split_id = id.split("_");
                imageids_arr.push(split_id[1]);
                //alert(split_id[1]);
            });

            // AJAX request
            
            $.ajax({
                url: '<?php echo site_url('Talent/ReorderPolaroid'); ?>',
                type: 'post',
                data: {imageids: imageids_arr},
                success: function(response){
                  Swal.fire(
                        'Good job!',
                        'Photo has been reorder!!!',
                        'success'
                      );
                }
            });
        });
        // Save order
      $('#SubmitOrderCatalog').click(function(){
            var imageids_arr = [];
            var catalogtalentID =$('#catalogtalentID').val();
            // get image ids order
            $('#sortableTalent .ui-state-default').each(function(){
                var id = $(this).attr('id');
                var split_id = id.split("_");
                imageids_arr.push(split_id[1]);
                //alert(split_id[1]);
            });
            //console.log(imageids_arr);
            // AJAX request
            
            $.ajax({
                url: '<?php echo site_url('Showcase/Reorder'); ?>',
                type: 'post',
                data: {imageids: imageids_arr,catalogtalentID: catalogtalentID},
                success: function(response){
                  Swal.fire(
                        'Good job!',
                        'Talent has been reorder!!!',
                        'success'
                      );
                }
            });
        });
          // Save order
      $('#SubmitOrderPacakge').click(function(){
            var imageids_arr = [];
            var packagetalentID =$('#packagetalentID').val();
            // get image ids order
            $('#sortablepackage .ui-state-default').each(function(){
                var id = $(this).attr('id');
                var split_id = id.split("_");
                imageids_arr.push(split_id[1]);
                //alert(split_id[1]);
            });
            //console.log(imageids_arr);
            // AJAX request
            
            $.ajax({
                url: '<?php echo site_url('Packages/Reorder'); ?>',
                type: 'post',
                data: {imageids: imageids_arr,packagetalentID: packagetalentID},
                success: function(response){
                  Swal.fire(
                        'Good job!',
                        'Talent has been reorder!!!',
                        'success'
                      );
                }
            });
        });
  });
</script>

</body>
</html>
