<?php
  if ($type == 'update'){
    $id_tags        = stripslashes($tag['id_tags']);
    $name           = stripslashes($tag['name_tags']);
    
 
    $judul ='Update Tags';
  }
  else{
    $judul ='Add Tags';
    $position = '';
  } 
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"> 
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('Tags'); ?>">Data Tags</a></li>
              <li class="breadcrumb-item active">Tags Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><?= $judul ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" enctype="multipart/form-data" <?php if($type=='create'){ ?> action="<?php echo base_url('Tags/created'); ?>" <?php } else { ?> action="<?php echo base_url('Tags/updated'); ?>" <?php } ?> >
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputname" name="name" required placeholder="Name" <?php if($type == 'update'){ echo 'value="'.$name .'"'; } ?> >
                      <?php if($type == 'update') { echo form_hidden('id', $id_tags); } ?>
                    </div>
                  </div>
                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function sync()
{
  var n1 = document.getElementById('inputname');
  var n2 = document.getElementById('inputSlug');
  n2.value = n1.value;
}
function sync2()
{
    $("#inputname").copyTo("#inputSlug");
}
$.fn.copyTo = function(selector) {
    $(selector).val($(this[0]).val().replace(/\s/g, "-"));
    
};
</script>