<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Tags</h1>
            <br/>
            
            <a href="<?php echo base_url(); ?>Tags/create" class="btn btn-app bg-gradient-primary" style="color:#fff;">
                  <i class="fas fa-plus"></i> Add Tags
            </a>
          </div> 
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Tags</li>
            </ol>
          </div>
        </div> 
      </div><!-- /.container-fluid -->

    </section>
    <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
    <?php } ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Tags</h3>
            </div>
            <!-- /.card-header -->
            
            <div class="card-body">
              <table id="example3" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    foreach ($tags as $t) { 
                        $id_tags=$t->id_tags; $datestring = '%d %M %Y - %h:%i:%s'; 
                ?>
                <tr>
                  <td><?= $t->name_tags ?></td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-warning" data-toggle="tooltip" onclick="myFunction('<?php echo base_url('Tags/update/').$id_tags; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-edit"></i></button>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete<?php echo $id_tags; ?>"  data-placement="top" data-original-title="Activated" ><i class="fa fa-trash"></i></button>
                    </div>
                  </td>
                    <div class="modal fade" id="modal-delete<?php echo $id_tags; ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete Gender</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                                                
                            </div>
                            
                            <div class="modal-body">
                            <form class="form-horizontal" method="post" action="<?php echo base_url().'Tags/delete/'.$id_tags;?>">
                              <div class="col-md-12">
                                <p>Are you sure to delete this Tags ?</p> 
                              </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default " data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-primary pull-left">Yes</button>
                            </form>
                            </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
</script>