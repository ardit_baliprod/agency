<?php
  if ($type == 'update'){
    $id_project         = stripslashes($project['id_project']);
    $name_project       = stripslashes($project['name_project']); 
    $type_project       = stripslashes($project['type_project']);
    $start_project      = stripslashes($project['start_project']);
    $start_project      = stripslashes($project['start_project']);
    $end_project        = stripslashes($project['end_project']);
    $location_project   = stripslashes($project['location_project']);
    $notes_project      = stripslashes($project['notes_project']);
    $status_project     = stripslashes($project['status_project']); 
    $project_client     = stripslashes($project['client_id']);
    $link=sha1(md5($id_project));

    $judul ='Update Project';
  }
  else{
    $judul ='Add Project';
    $client = '';
  }  
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Projects">Data Projects</a></li>
              <li class="breadcrumb-item active"><?= $judul ?></li>
            </ol>
          </div>
        </div> 
      </div><!-- /.container-fluid --> 
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
              <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-lg-12 col-sm-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-edit"></i>
                  <?= $judul ?>
                </h3>
              </div>
              <div class="card-body">
                
                <!-- <h4 class="">Custom Content Above</h4> -->
                <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Details</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Talent</a>
                  </li>
                  
                </ul>
                <div class="tab-custom-content">
                    <div class="btn-group">
                      
                    </div>
                </div>
                <div class="tab-content" id="custom-content-above-tabContent">
                  <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                    <!-- form start -->
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url('Project/updated'); ?>" >
                        <div class="card-body">
                        <div class="form-group row">
                            <label for="inputname" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputname" name="name" required placeholder="Name"  <?php if($type == 'update'){ echo 'value="'.$name_project .'"'; } ?> >
                            <?php if($type == 'update') { echo form_hidden('id', $id_project); } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputname" class="col-sm-2 col-form-label">Start</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                <div class="input-group date" id="datestartproject" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="datestartproject" data-target="#datestartproject" <?php if($type == 'update'){ echo 'value="'.$start_project .'"'; } ?> />
                                    <div class="input-group-append" data-target="#datestartproject" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputname" class="col-sm-2 col-form-label">End</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                <div class="input-group date" id="dateendproject" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="dateendproject" data-target="#dateendproject" <?php if($type == 'update'){ echo 'value="'.$end_project .'"'; } ?> />
                                    <div class="input-group-append" data-target="#dateendproject" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="form-group row">
                            <label for="inputCountry" class="col-sm-2 col-form-label">Customer</label>
                            <div class="form-group col-sm-10">
                            <select class="form-control select2" name="customer" style="width: 100%;">
                                <option value="" >Select Customer</option>
                            <?php 
                                foreach ($client as $c) { 
                                    $client_id=$c->client_id;
                                    $name=$c->client_name;
                            ?>
                                <option value="<?= $client_id ?>" <?php if($project_client == $client_id){ echo 'selected'; } ?> ><?= $name ?></option>
                                <?php } ?>
                                
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputType" class="col-sm-2 col-form-label">Type</label>
                                
                            <div class="col-sm-10">
                            <select class="form-control" name="type" >
                                <option value="Casting" <?php if($type_project == 'Casting'){ echo 'selected'; } ?> >Casting</option>
                                <option value="Option" <?php if($type_project == 'Option'){ echo 'selected'; } ?> >Option</option>
                                <option value="Confirmed Booking" <?php if($type_project == 'Confirmed Booking'){ echo 'selected'; } ?> >Confirmed Booking</option>
                                <option value="Not Available" <?php if($type_project == 'Not Available'){ echo 'selected'; } ?> >Not Available</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputlocation" class="col-sm-2 col-form-label">Location</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputlocation" name="location" required placeholder="Location" <?php if($type == 'update'){ echo 'value="'.$location_project .'"'; } ?> >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputDesc" class="col-sm-2 col-form-label">Notes</label>
                            <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="notes" placeholder="Notes ..."><?php if($type == 'update'){ echo $notes_project; } ?></textarea>
                            </div>
                        </div>
                        <?php if($type == 'update') { ?>
                                <div class="form-group row">
                                <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                                
                                    <div class="col-sm-10">
                                        <select class="form-control" name="status" >
                                            <option <?php if($status_project==1) { echo "selected"; } ?> value="1">Active</option>
                                            <option <?php if($status_project==0) { echo "selected"; } ?> value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                  </div>
                  <div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                  <input type="hidden" class="form-control" id="projecttalentID" name="projecttalentID" <?php  echo 'value="'.$id_project .'"'; ?>  >
                  <a data-toggle="modal" href="#modal-xl" class="btn btn-app bg-gradient-primary" style="color:#fff;">
                    <i class="fas fa-plus"></i> Add Talent
                  </a>
                    <table  id="example5" class="table table-bordered table-hover">

                      <thead>
                        <tr>
                        <th>Talent Name</th>
                        <th>Headshot</th>
                        <th>Action</th>
                        </tr>
                      </thead>
                      <tbody >

                      </tbody>
                      <tfoot>
                        <tr>
                        <th>Talent Name</th>
                        <th>Headshot</th>
                        <th>Action</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.card -->
          </div>
        </div>  
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
                        
                        <div class="modal " id="modal-xl">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Add Talent</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form id="myForm" method="post">
                                <div class="row">
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="gender" id="genderPackage"  style="width: 100%;">
                                          <option value="" >Select Gender</option>
                                        <?php 
                                            foreach ($gender as $g) { 
                                                $gender_id=$g->gender_id;
                                                $name=$g->gender_name;
                                        ?>
                                          <option value="<?= $gender_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="ethnicity" id="ethnicityPackage"  style="width: 100%;">
                                          <option value="" >Select Ethnicity</option>
                                        <?php 
                                            foreach ($ethnicity as $e) { 
                                                $ethnicity_id=$e->ethnicity_id;
                                                $name=$e->ethnicity_name;
                                        ?>
                                          <option value="<?= $ethnicity_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="category" id="categoryPackage"  style="width: 100%;">
                                          <option value="" >Select Category</option>
                                        <?php 
                                            foreach ($category as $c) { 
                                                $category_id=$c->category_id;
                                                $name=$c->category_name;
                                        ?>
                                          <option value="<?= $category_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="compelxion" id="compelxionPackage"  style="width: 100%;">
                                          <option value="" >Select Compelxion</option>
                                        <?php 
                                            foreach ($compelxion as $c) { 
                                                $compelxion_id=$c->compelxion_id;
                                                $name=$c->compelxion_name;
                                        ?>
                                          <option value="<?= $compelxion_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="tagPackage" name="tag" placeholder="Tag" >
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <button type="button" class="btn btn-primary" id="filterPackage"><i class="fa fa-filter"></i> Filter</button>
                                    </div>  
                                  </div>
                                </div>
                                </form>
                                <table id="example6" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th>Talent ID</th>
                                      <th>Talent Headshot</th>
                                      <th>Talent Name</th>
                                      <th>Gender</th>
									  <th>Ethnicity</th>
                                      <th>Category</th>
                                      <th>Compelxion</th>
                                      <th>Tags</th>
									  <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody >
                                
                                  </tbody>
                                </table>  
                              </div>
                              <div class="modal-footer justify-content-between">
                                
                              </div>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <div class="modal fade" id="modal-xl<?php echo $package_id; ?>">
                            <div class="modal-dialog modal-xl">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Send Email</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                      <label for="email">Additional Email</label>
                                      <input type="text" class="form-control" name="addemail" id="addEmail"  autocomplete="off"  >
                                    </div>
                                    <div class="form-group">
                                      <label for="fname">Message</label>
                                      <textarea class="form-control textarea" rows="3" name="messageEmail" id="messageEmail" placeholder="Message ..."></textarea>
                                      <input type="hidden" class="form-control" id="packageid" value="<?php echo $package_id; ?>" >
                                    </div>
                                    <button type="submit" class="btn btn-primary" id="saveEmail">save</button>
                                  
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  
                                </div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
  <script>
   $(document).ready(function () {
    var projecttalentID  =$('#projecttalentID').val(); 
    GetData(projecttalentID,'','','','','');
    GetTablePackage(projecttalentID);
      
  });
  function LoadPackageTalent(id){
  $.ajax({
        url:"<?php echo site_url('Packages/ListTalent'); ?>",
        method :"POST",
        data : {id:id},
        success:function(data){
          $('#sortablepackage').html(data);
        }
  });
}
function GetData(id,gender=null,ethnicity=null,tag=null,category=null,compelxion=null){
  
    $('#example6').DataTable( {
            "ajax": {
                      "type": "POST",
                      "url": "<?php echo site_url('Project/LoadProjectTalent/'); ?>",
                      "data": {
                        "id": id,
                        "gender": gender,
                        "ethnicity": ethnicity,
                        "tag": tag,
                        "category": category,
                        "compelxion": compelxion
                      }
                    },
            "destroy": true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "order": [[ 2, "asc" ]],
            "info": true,
            "autoWidth": false,
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        })
        .on( 'error.dt', function ( e, settings, techNote, message ) {
        console.log( 'Error: Calendar DataTables: ' + message ); // for test purpose
        return true;
      } );
}
function GetTablePackage(id){
  $('#example5').DataTable({
      "ajax": "<?php echo site_url('Project/LoadProject/'); ?>"+id,
      "destroy": true,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
}
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
function Newtab(name) { 
  //alert("Welcome " + name + ".");
  //window.location.replace(name);
  window.open(name, '_blank');
}
function confirmDelete(projectId,talent_id) {
  $.ajax({
        url: '<?php echo base_url();?>Project/RemoveTalent',
        method : "POST",
        data:{
            projectId: projectId,talent_id: talent_id
            },
         success :function(data){
                
          //$('#example4').DataTable().ajax.reload();
          GetData(projectId,'','','','','');
          GetTablePackage(projectId);
          //$('#example5').DataTable().ajax.reload();
        }
    });
}
function addTalent(projectId,talentID){
  $.ajax({
        url  : "<?php echo base_url()?>Project/addTalent",
        data : {projectId:projectId , talentID:talentID},
        method : "POST",
        
         success :function(data){
          //$("#genderPackage").val('').trigger('change');
          //$("#ethnicityPackage").val('').trigger('change');
          //$("#categoryPackage").val('').trigger('change');
          //$("#compelxionPackage").val('').trigger('change');
          //$('#tagPackage').val('');
          //$('#example4').DataTable().ajax.reload();
          GetData(projectId,'','','','','');
          GetTablePackage(projectId);
          
          //$('#example5').DataTable().ajax.reload();
        }
    });

}

     
      function LoadPackage()
      {
        var inputTalentID  =$('#packagetalentID').val();
        $.ajax({
        url:"<?php echo site_url('Packages/LoadPackage'); ?>",
        method :"POST",
        data : {id:inputTalentID},
        success:function(data){
          $('#packageTalent').html(data);
        }
        });

      }
      function getTalentPacakage(){
        var inputTalentID  =$('#projecttalentID').val();
        var table = $('#example4').DataTable( {
          "ajax": "<?php echo site_url('Packages/LoadPackageTalent/'); ?>"+inputTalentID
        } );
        
        setInterval( function () {
            table.ajax.reload();
        }, 100 );
      }
function sendmail(id) {
  
  Swal.fire({
  title: 'Are you sure?',
  text: "Are sure want to send email ?",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes!',
  showLoaderOnConfirm: true,
     
   preConfirm: function() {
     return new Promise(function(resolve) {
          
        $.ajax({
        url: '<?php echo base_url();?>Packages/sendmail',
        type: 'POST',
           data: 'id='+id,
           dataType: 'json'
        })
        .done(function(){
          //Swal.fire('Success!', response.message, response.status);
          Swal.fire('Success!', 'Your message has been sent ...', 'success');
        })
        .fail(function(){
          Swal.fire('Success!', 'Your message has been sent ...', 'success');
        });
     });
      },
   allowOutsideClick: false
})
}
$('#saveEmail').on('click',function(){
            
            var packageid         =$('#packageid').val();
            var messageEmail      =$('#messageEmail').val();
            var addEmail          =$('#addEmail').val(); 

             
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>Packages/sendmail",
                data : {packageid:packageid , messageEmail:messageEmail , addEmail:addEmail },
                success: function(data){
                    $('#packageid').val('');
                    $('#messageEmail').val('');
                    $('.modal').modal('hide');
                    //alert(data);
                    
                    Swal.fire({
                      title: 'Good Job!',
                      text: "Your message has been sent ...",
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'OK!'
                    }).then((result) => {
                      if (result.value) {
                        window.location.reload(true);
                      }
                    })
                }
            });
        });
$('#DeletePackageTalent').click(function(){
    //alert('sukses');
    var packagetalentID =$('#projecttalentID').val();
    var checkboxes = document.querySelectorAll('input.gallery-checkbox:checked');
    for(var i=0;i<checkboxes.length;i++){
        //alert(checkboxes[i].value); 
        var id = checkboxes[i].value;
        $.ajax({
        url : "<?php echo site_url('Showcase/removeBulkTalent');?>",
            method : "POST",
            data : {id : id,packagetalentID: packagetalentID},
            success :function(data){
                              
            }
    });
    
    }
    //$('#example4').DataTable().ajax.reload();
    GetData(packagetalentID,'','','','','');
    LoadPackageTalent(packagetalentID); 
    /*
    if ($('input.gallery-checkbox').is(':checked')) {
        //blah blah
        alert($('input.gallery-checkbox:checked').val());
    } 
    if(checkboxes[i].is(':checked')){
           alert(checkboxes[i].val()); 
        }
    */
});
$('#filterPackage').click(function(){
  var inputTalentID  =$('#projecttalentID').val();
  var e = document.getElementById("genderPackage");
  var strGender = e.options[e.selectedIndex].value;
  e = document.getElementById("ethnicityPackage");
  var strEthnicity = e.options[e.selectedIndex].value;
  e = document.getElementById("categoryPackage");
  var strCategory = e.options[e.selectedIndex].value;
  e = document.getElementById("compelxionPackage");
  var strCompelxion = e.options[e.selectedIndex].value;
  var tagPackage  =$('#tagPackage').val();
  GetData(inputTalentID,strGender,strEthnicity,tagPackage,strCategory,strCompelxion);
  /*
  $.ajax({
        url : "<?php echo site_url('Packages/LoadPackageTalent/'); ?>"+inputTalentID+"/"+strGender+"/"+strEthnicity+"/"+tagPackage+"/"+strCategory+"/"+strCompelxion,
            method : "POST",
            success :function(data){
              alert(data);  
              console.log(data);
            }
          });       */   

});
</script>
