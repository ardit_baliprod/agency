<?php
  if ($type == 'update'){
    $category_id      = '';
    $name           = '';
    $slug           = ''; 
    $desc           = ''; 
    $status         = '';
    
 
    $judul ='Update Project';
  }
  else{
    $judul ='Add Project';
    $position = '';
  } 
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"> 
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('Project'); ?>">Data Projects</a></li>
              <li class="breadcrumb-item active">Project Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><?= $judul ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" enctype="multipart/form-data" <?php if($type=='create'){ ?> action="<?php echo base_url('Project/created'); ?>" <?php } else { ?> action="<?php echo base_url('Project/updated'); ?>" <?php } ?> >
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputname" name="name" required placeholder="Name"  <?php if($type == 'update'){ echo 'value="'.$name .'"'; } ?> >
                      <?php if($type == 'update') { echo form_hidden('id', $category_id); } ?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">Start</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                          <div class="input-group date" id="datestartproject" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" name="datestartproject" data-target="#datestartproject"/>
                            <div class="input-group-append" data-target="#datestartproject" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">End</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                          <div class="input-group date" id="dateendproject" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" name="dateendproject" data-target="#dateendproject"/>
                            <div class="input-group-append" data-target="#dateendproject" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  
                 
                  <div class="form-group row">
                    <label for="inputCountry" class="col-sm-2 col-form-label">Customer</label>
                    <div class="form-group col-sm-10">
                      <select class="form-control select2" name="customer" style="width: 100%;">
                        <option value="" >Select Customer</option>
                      <?php 
                          foreach ($client as $c) { 
                              $client_id=$c->client_id;
                              $name=$c->client_name;
                      ?>
                        <option value="<?= $client_id ?>"><?= $name ?></option>
                          <?php } ?>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputType" class="col-sm-2 col-form-label">Type</label>
                        
                    <div class="col-sm-10">
                      <select class="form-control" name="type" >
                        <option value="Casting">Casting</option>
                        <option value="Option">Option</option>
                        <option value="Confirmed Booking">Confirmed Booking</option>
                        <option value="Not Available">Not Available</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputlocation" class="col-sm-2 col-form-label">Location</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputlocation" name="location" required placeholder="Location" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputDesc" class="col-sm-2 col-form-label">Notes</label>
                    <div class="col-sm-10">
                       <textarea class="form-control" rows="3" name="notes" placeholder="Notes ..."><?php if($type == 'update'){ echo $desc; } ?></textarea>
                    </div>
                  </div>
                  <?php if($type == 'update') { ?>
                        <div class="form-group row">
                        <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                        
                            <div class="col-sm-10">
                                <select class="form-control" name="status" >
                                    <option <?php if($status==1) { echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($status==0) { echo "selected"; } ?> value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function sync()
{
  var n1 = document.getElementById('inputname');
  var n2 = document.getElementById('inputSlug');
  n2.value = n1.value;
}
function sync2()
{
    $("#inputname").copyTo("#inputSlug");
}
$.fn.copyTo = function(selector) {
    $(selector).val($(this[0]).val().replace(/\s/g, "-"));
    
};
</script>