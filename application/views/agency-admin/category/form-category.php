<?php
  if ($type == 'update'){
    $category_id      = stripslashes($category['category_id']);
    $name           = stripslashes($category['category_name']);
    $slug           = stripslashes($category['category_slug']); 
    $desc           = stripslashes($category['category_desc']); 
    $status         = stripslashes($category['category_status']);
    
 
    $judul ='Update Category';
  }
  else{
    $judul ='Add Category';
    $position = '';
  } 
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"> 
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url('Category'); ?>">Data Category</a></li>
              <li class="breadcrumb-item active">Category Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><?= $judul ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" enctype="multipart/form-data" <?php if($type=='create'){ ?> action="<?php echo base_url('Category/created'); ?>" <?php } else { ?> action="<?php echo base_url('Category/updated'); ?>" <?php } ?> >
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputname" name="name" required placeholder="Name" onkeyup="sync2()" <?php if($type == 'update'){ echo 'value="'.$name .'"'; } ?> >
                      <?php if($type == 'update') { echo form_hidden('id', $category_id); } ?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputSlug" class="col-sm-2 col-form-label">Slug</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputSlug" name="slug" placeholder="slug" style="text-transform: lowercase" <?php if($type == 'update'){ echo 'value="'.$slug .'"'; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputDesc" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                       <textarea class="form-control" rows="3" name="desc" placeholder="Description ..."><?php if($type == 'update'){ echo $desc; } ?></textarea>
                    </div>
                  </div>
                  <?php if($type == 'update') { ?>
                        <div class="form-group row">
                        <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                        
                            <div class="col-sm-10">
                                <select class="form-control" name="status" >
                                    <option <?php if($status==1) { echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($status==0) { echo "selected"; } ?> value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function sync()
{
  var n1 = document.getElementById('inputname');
  var n2 = document.getElementById('inputSlug');
  n2.value = n1.value;
}
function sync2()
{
    $("#inputname").copyTo("#inputSlug");
}
$.fn.copyTo = function(selector) {
    $(selector).val($(this[0]).val().replace(/\s/g, "-"));
    
};
</script>