<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Client</h1>
            <br/>
            
            <a href="<?php echo base_url(); ?>Client/create" class="btn btn-app bg-gradient-primary" style="color:#fff;">
                  <i class="fas fa-plus"></i> Add Client
            </a>
          </div> 
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Client</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->

    </section>
    <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
    <?php } ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Client</h3>
            </div>
            <!-- /.card-header -->
            
            <div class="card-body">
              <table id="example3" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    foreach ($client as $c) { 
                        $client_id=$c->client_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                ?>
                <tr>
                  <td><?= $c->client_name ?></td>
                  <td><?= $c->client_phone ?></td>
                  <td><?php if($c->client_status == 1)  echo 'Active'; else echo 'Inactive'; ?></td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default<?php echo $client_id; ?>" data-placement="top" data-original-title="Detail" ><i class="fa fa-search"></i></button>
                      <button type="button" class="btn btn-warning" data-toggle="tooltip" onclick="myFunction('<?php echo base_url('Client/update/').$client_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-edit"></i></button>
                      
                    </div>
                  </td>
                  
                  <div class="modal fade" id="modal-default<?php echo $client_id; ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Client Detail</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                                                
                            </div>
                            <div class="modal-body">
                            <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Client Name</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->client_name; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Phone Number</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->client_phone; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Address</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->client_address; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>City</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->client_city; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>State</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->client_state; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>ZIP Code</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->client_zip; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Country</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->short_name; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Website</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $c->client_website; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Status</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php if($c->client_status == 1)  echo 'Active'; else echo 'Inactive'; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                
                            </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
</script>