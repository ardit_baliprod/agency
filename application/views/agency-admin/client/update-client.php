<?php
  if ($type == 'update'){
    $client_id        = stripslashes($client['client_id']);
    $client_name      = stripslashes($client['client_name']);
    $client_phone     = stripslashes($client['client_phone']); 
    $client_address   = stripslashes($client['client_address']); 
    $client_city      = stripslashes($client['client_city']);
    $client_state     = stripslashes($client['client_state']);
    $client_zip       = stripslashes($client['client_zip']);
    $client_country   = stripslashes($client['client_country']); 
    $client_website   = stripslashes($client['client_website']);
    $client_status    = stripslashes($client['client_status']);

    $judul ='Update Client';
  }
  else{
    $judul ='Add Client';
    $position = '';
  }  
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Client">Data Client</a></li>
              <li class="breadcrumb-item active"><?= $judul ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
    <?php } ?>
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        
        <div class="row">
          <div class="col-12">
            <h4>Client Info</h4>
          </div>
        </div>
        <!-- ./row -->
        <div class="row">
          <div class="col-12 col-sm-12 col-lg-12">
            <div class="card card-primary">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Client Details</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Contacts</a>
                  </li>
                  
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <form class="form-horizontal" method="post" action="<?php echo base_url('Client/updated'); ?>" >
                      <div class="card-body">
                        <div class="form-group row">
                          <label for="inputName" class="col-sm-2 col-form-label">Company Name</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" <?php echo 'value="'.$client_name .'"'; ?>  >
                            <?php  echo form_hidden('id', $client_id);  ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputPhone" class="col-sm-2 col-form-label">Phone</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone" <?php echo 'value="'.$client_phone .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputAdress" class="col-sm-2 col-form-label">Address</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputAdress" name="address" placeholder="Address" <?php echo 'value="'.$client_address .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputCity" class="col-sm-2 col-form-label">City</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputCity" name="city" placeholder="City" <?php echo 'value="'.$client_city .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputState" class="col-sm-2 col-form-label">State</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputState" name="state" placeholder="State" <?php echo 'value="'.$client_state .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputZIP" class="col-sm-2 col-form-label">ZIP Code</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputZIP" name="zip" placeholder="ZIP Code" <?php echo 'value="'.$client_zip .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputCountry" class="col-sm-2 col-form-label">Country</label>
                          <div class="form-group col-sm-10">
                            <select class="form-control select2" name="country" style="width: 100%;">
                              <option value="" >Select Country</option>
                            <?php 
                                foreach ($country as $c) { 
                                    $country_id=$c->country_id;
                                    $name=$c->short_name;
                            ?>
                              <option value="<?= $country_id ?>" <?php if($client_country==$country_id){ echo 'selected'; } ?> ><?= $name ?></option>
                                <?php } ?>
                              
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputWebsite" class="col-sm-2 col-form-label">Website</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputWebsite" name="website" placeholder="Website" <?php echo 'value="'.$client_website .'"'; ?> >
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                        
                            <div class="col-sm-10">
                                <select class="form-control" name="status" >
                                    <option <?php if($client_status==1) { echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($client_status==0) { echo "selected"; } ?> value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                                
                        
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                      </div>
                      <!-- /.card-footer -->
                    </form>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-xl" data-placement="top" data-original-title="Add" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> &nbsp;Add Contact</button>
                    <br/>
                    <table id="example1" class="table table-bordered table-hover">
                      <thead>
                      <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php 
                          
                            foreach ($contact as $c) { 
                                $contact_id=$c->contact_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                      ?>
                      <tr>
                        <td><?= $c->contact_fname .' '. $c->contact_lname ?></td>
                        <td><?= $c->contact_position ?></td>
                        <td><?= $c->contact_email ?></td>
                        <td><?= $c->contact_phone ?></td>
                        <td><?php if($c->status == 1)  echo 'Active'; else echo 'Inactive'; ?></td>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default<?= $contact_id ?>" data-placement="top" data-original-title="Update" ><i class="fa fa-edit"></i></button>
                                               
                          </div>
                        </td>
                        
                        <div class="modal fade" id="modal-default<?= $contact_id ?>">
                              <div class="modal-dialog modal-xl">
                                  <div class="modal-content">
                                  <div class="modal-header">
                                      <h4 class="modal-title">Contact Detail</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                                                      
                                  </div>
                                  <div class="modal-body">
                                  <form method="post" action="<?php echo base_url('Client/updatecontact'); ?>" >
                                    <div class="form-group">
                                      <label for="fname">First Name</label>
                                      <input type="text" class="form-control" id="Updatefname" name="fname" placeholder="First Name" value="<?php echo $c->contact_fname; ?>"  required autofocus>
                                      <input type="hidden" class="form-control" id="Updateclientid" name="clientid" value="<?php echo $client_id; ?>" >
                                      <input type="hidden" class="form-control" id="contact_id" name="contactid" value="<?php echo $contact_id; ?>" >
                                    </div>
                                    <div class="form-group">
                                      <label for="lname">Last Name</label>
                                      <input type="text" class="form-control" id="Updatelname" placeholder="Last Name" name="lname" value="<?= $c->contact_lname ?>"  autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="alamat">Title</label>
                                      <select class="form-control" id="Updatetitle" name="title" >
                                          <option <?php if($c->contact_title=='Mr'){echo 'selected';} ?> value="Mr">Mr</option>
                                          <option <?php if($c->contact_title=='Mrs'){echo 'selected';} ?> value="Mrs">Mrs</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="email">Email</label>
                                      <input type="email" class="form-control" id="Updateemail" name="email" placeholder="Email" value="<?= $c->contact_email ?>"  required autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="phone">Phone</label>
                                      <input type="text" class="form-control" id="Updatephone" name="phone" placeholder="Phone" value="<?= $c->contact_phone ?>"  autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="position">Position</label>
                                      <input type="text" class="form-control" id="Updateposition" name="position" placeholder="Position" value="<?= $c->contact_position ?>"  autofocus>
                                    </div>
                                    <div class="form-check">
                                      <input type="checkbox" class="form-check-input" id="Updateisprimary" name="primary" value="true" <?php if($c->is_primary==1){echo 'checked';} ?>>
                                      <label for="position">Is Primary</label>
                                    </div>
                                    <div class="form-group">
                                      <label for="alamat">Status</label>
                                      <select class="form-control" id="Updatestatus" name="status" >
                                          <option <?php if($c->status==1){echo 'selected';} ?> value="1">Active</option>
                                          <option <?php if($c->status==0){echo 'selected';} ?> value="0">Inactive</option>
                                      </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary" id="Update">save</button>
                                  </form>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                      
                                  </div>
                                  </div>
                                  <!-- /.modal-content -->
                              </div>
                              <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
                          

                      </tr>
                          <?php }  ?>
                      </tbody>
                      <tfoot>
                      <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                      </tfoot>
                    </table> 
                  </div>
                  
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

                          <div class="modal fade" id="modal-xl">
                            <div class="modal-dialog modal-xl">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Add Contact</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <form>
                                    <div class="form-group">
                                      <label for="fname">First Name</label>
                                      <input type="text" class="form-control" id="fname" placeholder="First Name" required autofocus>
                                      <input type="hidden" class="form-control" id="clientid" value="<?php echo $client_id; ?>" >
                                    </div>
                                    <div class="form-group">
                                      <label for="lname">Last Name</label>
                                      <input type="text" class="form-control" id="lname" placeholder="Last Name"  autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="alamat">Title</label>
                                      <select class="form-control" id="title" >
                                          <option value="Mr">Mr</option>
                                          <option value="Mrs">Mrs</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="email">Email</label>
                                      <input type="email" class="form-control" id="email" placeholder="Email" required autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="phone">Phone</label>
                                      <input type="text" class="form-control" id="phone" placeholder="Phone"  autofocus>
                                    </div>
                                    <div class="form-group">
                                      <label for="position">Position</label>
                                      <input type="text" class="form-control" id="position" placeholder="Position"  autofocus>
                                    </div>
                                    <div class="form-check">
                                      <input type="checkbox" class="form-check-input" id="isprimary">
                                      <label for="position">Is Primary</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary" id="save">save</button>
                                  </form>
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  
                                </div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
                          
                         


<script>

//insert data
$('#save').on('click',function(){
             
            var clientid  =$('#clientid').val();
            var fname     =$('#fname').val();
            var lname     =$('#lname').val();
            var email     =$('#email').val();
            var phone     =$('#phone').val();
            var position  =$('#position').val();
            var f         = document.getElementById("title");
            var title     = f.options[f.selectedIndex].value;
            var primary   = document.getElementById("isprimary").checked;

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>Client/addcontact",
                data : {clientid:clientid , fname:fname, lname:lname,email:email,phone:phone,position:position,title:title,primary:primary},
                success: function(data){
                    $('#clientid').val('');
                    $('#fname').val('');
                    $('#lname').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('#position').val('');
                    $('.modal').modal('hide');
                    Swal.fire({
                      title: 'Good Job!',
                      text: "Data contact succesfully added!!",
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'OK!'
                    }).then((result) => {
                      if (result.value) {
                        window.location.reload(true);
                      }
                    })
                }
            });
        });

       


</script>