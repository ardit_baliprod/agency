<?php
  if ($type == 'update'){
    $username = stripslashes($user['username']);
    $fname = stripslashes($user['fname']); 
    $lname = stripslashes($user['lname']); 
    $position = stripslashes($user['position']);
    $phone = stripslashes($user['phone']);
    $email = stripslashes($user['email']);
    $status = stripslashes($user['status']);

    $judul ='Update Client';
  }
  else{
    $judul ='Add Client';
    $position = '';
  }  
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Client">Data Client</a></li>
              <li class="breadcrumb-item active"><?= $judul ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            
            <!-- Horizontal Form -->
            <div class="card card-info"> 
              <div class="card-header">
                <h3 class="card-title"><?= $judul ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" <?php if($type=='create'){ ?> action="<?php echo base_url('Client/created'); ?>" <?php } else { ?> action="<?php echo base_url('Client/updated'); ?>" <?php } ?>>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputName" class="col-sm-2 col-form-label">Company Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="name" required placeholder="Name" <?php if($type == 'update'){ echo 'value="'.$fname .'"'; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPhone" class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone" <?php if($type == 'update'){ echo 'value="'.$phone .'"'; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputAdress" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputAdress" name="address" placeholder="Address" <?php if($type == 'update'){ echo 'value="'.$username .'" readonly'; } ?> >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputCity" class="col-sm-2 col-form-label">City</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputCity" name="city" placeholder="City">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputState" class="col-sm-2 col-form-label">State</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputState" name="state" placeholder="State">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputZIP" class="col-sm-2 col-form-label">ZIP Code</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputZIP" name="zip" placeholder="ZIP Code">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputCountry" class="col-sm-2 col-form-label">Country</label>
                    <div class="form-group col-sm-10">
                      <select class="form-control select2" name="country" style="width: 100%;">
                        <option value="" >Select Country</option>
                      <?php 
                          foreach ($country as $c) { 
                              $country_id=$c->country_id;
                              $name=$c->short_name;
                      ?>
                        <option value="<?= $country_id ?>"><?= $name ?></option>
                          <?php } ?>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputWebsite" class="col-sm-2 col-form-label">Website</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputWebsite" name="website" placeholder="Website">
                    </div>
                  </div>
                 
                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->