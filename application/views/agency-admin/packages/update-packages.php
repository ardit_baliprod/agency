<?php
  if ($type == 'update'){
    $package_id = stripslashes($packages['package_id']);
    $package_name = stripslashes($packages['package_name']); 
    $package_description = stripslashes($packages['package_description']);
    $package_status = stripslashes($packages['package_status']); 
    $package_client = stripslashes($packages['client_id']);
    $link=sha1(md5($package_id));

    $judul ='Update Package';
  }
  else{
    $judul ='Add Package';
    $client = '';
  }  
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $judul ?></h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Packages">Data Packages</a></li>
              <li class="breadcrumb-item active"><?= $judul ?></li>
            </ol>
          </div>
        </div> 
      </div><!-- /.container-fluid --> 
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
              <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-lg-12 col-sm-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-edit"></i>
                  <?= $judul ?>
                </h3>
              </div>
              <div class="card-body">
                
                <!-- <h4 class="">Custom Content Above</h4> -->
                <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Details</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Talent</a>
                  </li>
                  
                </ul>
                <div class="tab-custom-content">
                    <div class="btn-group">
                      <!--<button type="button" class="btn btn-primary" data-toggle="tooltip" onclick="sendmail('<?php //echo $package_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-envelope"></i></button> -->
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-xl<?php echo $package_id; ?>"   data-placement="top" data-original-title="Update" ><i class="fa fa-envelope"></i></button>
                      <button type="button" class="btn btn-info" data-toggle="tooltip" onclick="Newtab('<?php echo base_url('Catalog/package/').$link; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-eye"></i></button>
                      <button type="button" class="btn btn-success" data-toggle="tooltip" onclick="Newtab('<?php echo base_url('Export/package/').$package_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-file-pdf"></i></button>
                    </div>
                </div>
                <div class="tab-content" id="custom-content-above-tabContent">
                  <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="<?php echo base_url('Packages/updated'); ?>" >
                      <div class="card-body">
                        <div class="form-group row"> 
                          <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" <?php if($type == 'update'){ echo 'value="'.$package_name .'"'; } ?> >
                            <?php if($type == 'update') { echo form_hidden('id', $package_id); } ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputCountry" class="col-sm-2 col-form-label">Customer</label>
                          <div class="form-group col-sm-10">
                            <select class="form-control select2" name="customer" style="width: 100%;">
                              <option value="" >Select Customer</option>
                            <?php 
                                foreach ($client as $c) { 
                                    $client_id=$c->client_id;
                                    $name=$c->client_name;
                            ?>
                              <option value="<?= $client_id ?>" <?php if($package_client == $client_id){ echo 'selected'; } ?> ><?= $name ?></option>
                                <?php } ?>
                              
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputDesc" class="col-sm-2 col-form-label">Description</label>
                          <div class="col-sm-10">
                            <textarea class="form-control textarea" rows="3" name="desc" placeholder="Description ..."><?php if($type == 'update'){ echo $package_description; } ?></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                        
                            <div class="col-sm-10">
                                <select class="form-control" name="status" >
                                    <option <?php if($package_status==1) { echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($package_status==0) { echo "selected"; } ?> value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                      </div>
                      <!-- /.card-footer -->
                    </form>
                  </div>
                  <div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                  <input type="hidden" class="form-control" id="packagetalentID" name="packagetalentID" <?php  echo 'value="'.$package_id .'"'; ?>  >
                  <a data-toggle="modal" href="#modal-xl" class="btn btn-app bg-gradient-primary" style="color:#fff;">
                    <i class="fas fa-plus"></i> Add Talent
                  </a>
                      <div class="agency-catalog-wrapper" id="sortablepackage">
                       
                       </div>
                       <div class="row" style="clear: both; margin-top: 20px;">
                         <button type="button" class="btn btn-lg btn-success" id="SubmitOrderPacakge">Save</button>
                         &nbsp;
                         <button type="button" class="btn btn-lg btn-danger" id="DeletePackageTalent">Delete Selection</button>
                       </div>
                  </div>
                  
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.card -->
          </div>
        </div>  
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
                        
                        <div class="modal " id="modal-xl">
                          <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Add Talent</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form id="myForm" method="post">
                                <div class="row">
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="gender" id="genderPackage"  style="width: 100%;">
                                          <option value="" >Select Gender</option>
                                        <?php 
                                            foreach ($gender as $g) { 
                                                $gender_id=$g->gender_id;
                                                $name=$g->gender_name;
                                        ?>
                                          <option value="<?= $gender_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="ethnicity" id="ethnicityPackage"  style="width: 100%;">
                                          <option value="" >Select Ethnicity</option>
                                        <?php 
                                            foreach ($ethnicity as $e) { 
                                                $ethnicity_id=$e->ethnicity_id;
                                                $name=$e->ethnicity_name;
                                        ?>
                                          <option value="<?= $ethnicity_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="category" id="categoryPackage"  style="width: 100%;">
                                          <option value="" >Select Category</option>
                                        <?php 
                                            foreach ($category as $c) { 
                                                $category_id=$c->category_id;
                                                $name=$c->category_name;
                                        ?>
                                          <option value="<?= $category_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <select class="form-control select2" name="compelxion" id="compelxionPackage"  style="width: 100%;">
                                          <option value="" >Select Compelxion</option>
                                        <?php 
                                            foreach ($compelxion as $c) { 
                                                $compelxion_id=$c->compelxion_id;
                                                $name=$c->compelxion_name;
                                        ?>
                                          <option value="<?= $compelxion_id ?>" ><?= $name ?></option>
                                            <?php } ?>
                                          
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="tagPackage" name="tag" placeholder="Tag" >
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-4">
                                    <div class="form-group row">
                                      <button type="button" class="btn btn-primary" id="filterPackage"><i class="fa fa-filter"></i> Filter</button>
                                    </div>  
                                  </div>
                                </div>
                                </form>
                                <table id="example4" class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th>Talent ID</th>
                                      <th>Talent Headshot</th>
                                      <th>Talent Name</th>
                                      <th>Gender</th>
										                  <th>Ethnicity</th>
                                      <th>Category</th>
                                      <th>Compelxion</th>
                                      <th>Tags</th>
										                  <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody >
                                
                                  </tbody>
                                </table>  
                              </div>
                              <div class="modal-footer justify-content-between">
                                
                              </div>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                          <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <div class="modal fade" id="modal-xl<?php echo $package_id; ?>">
                            <div class="modal-dialog modal-xl">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Send Email</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                      <label for="email">Additional Email</label>
                                      <input type="text" class="form-control" name="addemail" id="addEmail"  autocomplete="off"  >
                                    </div>
                                    <div class="form-group">
                                      <label for="fname">Message</label>
                                      <textarea class="form-control textarea" rows="3" name="messageEmail" id="messageEmail" placeholder="Message ..."></textarea>
                                      <input type="hidden" class="form-control" id="packageid" value="<?php echo $package_id; ?>" >
                                    </div>
                                    <button type="submit" class="btn btn-primary" id="saveEmail">save</button>
                                  
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  
                                </div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
  <script>
   $(document).ready(function () {
    var inputTalentID  =$('#packagetalentID').val(); 
    GetData(inputTalentID,'','','','','');
    LoadPackageTalent(inputTalentID);
      
  });
  function LoadPackageTalent(id){
  $.ajax({
        url:"<?php echo site_url('Packages/ListTalent'); ?>",
        method :"POST",
        data : {id:id},
        success:function(data){
          $('#sortablepackage').html(data);
        }
  });
}
function GetData(id,gender=null,ethnicity=null,tag=null,category=null,compelxion=null){
  
    $('#example4').DataTable( {
            //"ajax": "<?php //echo site_url('Packages/LoadPackageTalent/'); ?>"+id+"/"+gender+"/"+ethnicity+"/"+tag+"/"+category+"/"+compelxion,
            "ajax": {
                      "type": "POST",
                      "url": "<?php echo site_url('Packages/LoadPackageTalent/'); ?>",
                      "data": {
                        "id": id,
                        "gender": gender,
                        "ethnicity": ethnicity,
                        "tag": tag,
                        "category": category,
                        "compelxion": compelxion
                      }
                    },
            "destroy": true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "order": [[ 2, "asc" ]],
            "info": true,
            "autoWidth": false,
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
      } );
}
function GetTablePackage(id){
  $('#example5').DataTable({
      "ajax": "<?php echo site_url('Packages/LoadPackage/'); ?>"+id,
      "destroy": true,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
}
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
function Newtab(name) { 
  //alert("Welcome " + name + ".");
  //window.location.replace(name);
  window.open(name, '_blank');
}
function confirmDelete(package_id,talent_id) {
  $.ajax({
        url: '<?php echo base_url();?>Packages/RemoveTalent',
        method : "POST",
        data:{
              package_id: package_id,talent_id: talent_id
            },
         success :function(data){
                
          //$('#example4').DataTable().ajax.reload();
          GetData(package_id,'','','','','');
          LoadPackageTalent(package_id);
          //$('#example5').DataTable().ajax.reload();
        }
    });
}
function addTalent(packageId,talentID){
  $.ajax({
        url  : "<?php echo base_url()?>Packages/addTalent",
        data : {packageId:packageId , talentID:talentID},
        method : "POST",
        
         success :function(data){
          //;$("#genderPackage").val('').trigger('change');
          //$("#ethnicityPackage").val('').trigger('change');
          //$("#categoryPackage").val('').trigger('change');
          //$("#compelxionPackage").val('').trigger('change');
          //$('#tagPackage').val('');
          //$('#example4').DataTable().ajax.reload();
          var e = document.getElementById("genderPackage");
          var strGender = e.options[e.selectedIndex].value;
          e = document.getElementById("ethnicityPackage");
          var strEthnicity = e.options[e.selectedIndex].value;
          e = document.getElementById("categoryPackage");
          var strCategory = e.options[e.selectedIndex].value;
          e = document.getElementById("compelxionPackage");
          var strCompelxion = e.options[e.selectedIndex].value;
          var tagPackage  =$('#tagPackage').val();
          GetData(packageId,strGender,strEthnicity,tagPackage,strCategory,strCompelxion);
          //GetData(packageId,'','','','','');
          LoadPackageTalent(packageId);
          
          //$('#example5').DataTable().ajax.reload();
        }
    });

}

     
      function LoadPackage()
      {
        var inputTalentID  =$('#packagetalentID').val();
        $.ajax({
        url:"<?php echo site_url('Packages/LoadPackage'); ?>",
        method :"POST",
        data : {id:inputTalentID},
        success:function(data){
          $('#packageTalent').html(data);
        }
        });

      }
      function getTalentPacakage(){
        var inputTalentID  =$('#packagetalentID').val();
        var table = $('#example4').DataTable( {
          "ajax": "<?php echo site_url('Packages/LoadPackageTalent/'); ?>"+inputTalentID
        } );
        
        setInterval( function () {
            table.ajax.reload();
        }, 100 );
      }
function sendmail(id) {
  
  Swal.fire({
  title: 'Are you sure?',
  text: "Are sure want to send email ?",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes!',
  showLoaderOnConfirm: true,
     
   preConfirm: function() {
     return new Promise(function(resolve) {
          
        $.ajax({
        url: '<?php echo base_url();?>Packages/sendmail',
        type: 'POST',
           data: 'id='+id,
           dataType: 'json'
        })
        .done(function(){
          //Swal.fire('Success!', response.message, response.status);
          Swal.fire('Success!', 'Your message has been sent ...', 'success');
        })
        .fail(function(){
          Swal.fire('Success!', 'Your message has been sent ...', 'success');
        });
     });
      },
   allowOutsideClick: false
})
}
$('#saveEmail').on('click',function(){
            
            var packageid         =$('#packageid').val();
            var messageEmail      =$('#messageEmail').val();
            var addEmail          =$('#addEmail').val(); 

             
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>Packages/sendmail",
                data : {packageid:packageid , messageEmail:messageEmail , addEmail:addEmail },
                success: function(data){
                    $('#packageid').val('');
                    $('#messageEmail').val('');
                    $('.modal').modal('hide');
                    //alert(data);
                    
                    Swal.fire({
                      title: 'Good Job!',
                      text: "Your message has been sent ...",
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'OK!'
                    }).then((result) => {
                      if (result.value) {
                        window.location.reload(true);
                      }
                    })
                }
            });
        });
$('#DeletePackageTalent').click(function(){
    //alert('sukses');
    var packagetalentID =$('#packagetalentID').val();
    var checkboxes = document.querySelectorAll('input.gallery-checkbox:checked');
    for(var i=0;i<checkboxes.length;i++){
        //alert(checkboxes[i].value); 
        var id = checkboxes[i].value;
        $.ajax({
        url : "<?php echo site_url('Showcase/removeBulkTalent');?>",
            method : "POST",
            data : {id : id,packagetalentID: packagetalentID},
            success :function(data){
                              
            }
    });
    
    }
    //$('#example4').DataTable().ajax.reload();
    GetData(packagetalentID,'','','','','');
    LoadPackageTalent(packagetalentID); 
    /*
    if ($('input.gallery-checkbox').is(':checked')) {
        //blah blah
        alert($('input.gallery-checkbox:checked').val());
    } 
    if(checkboxes[i].is(':checked')){
           alert(checkboxes[i].val()); 
        }
    */
});
$('#filterPackage').click(function(){
  var inputTalentID  =$('#packagetalentID').val();
  var e = document.getElementById("genderPackage");
  var strGender = e.options[e.selectedIndex].value;
  e = document.getElementById("ethnicityPackage");
  var strEthnicity = e.options[e.selectedIndex].value;
  e = document.getElementById("categoryPackage");
  var strCategory = e.options[e.selectedIndex].value;
  e = document.getElementById("compelxionPackage");
  var strCompelxion = e.options[e.selectedIndex].value;
  var tagPackage  =$('#tagPackage').val();
  GetData(inputTalentID,strGender,strEthnicity,tagPackage,strCategory,strCompelxion);
  /*
  $.ajax({
        url : "<?php echo site_url('Packages/LoadPackageTalent/'); ?>"+inputTalentID+"/"+strGender+"/"+strEthnicity+"/"+tagPackage+"/"+strCategory+"/"+strCompelxion,
            method : "POST",
            success :function(data){
              alert(data);  
              console.log(data);
            }
          });       */   

});
</script>