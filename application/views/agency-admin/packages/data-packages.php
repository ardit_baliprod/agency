<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Packages</h1> 
            <br/>
            
            <a href="<?php echo base_url(); ?>Packages/create" class="btn btn-app bg-gradient-primary" style="color:#fff;">
                  <i class="fas fa-plus"></i> Add Package
            </a>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Packages</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
 
    </section>
    <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
    <?php } ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header"> 
              <h3 class="card-title">Data Packages</h3>
            </div>
            <!-- /.card-header -->
            
            <div class="card-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Customer</th>
                  <th>Action</th>
                </tr>
                </thead> 
                <tbody>
                <?php 
                    foreach ($packages as $p) { 
                        $package_id=$p->package_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                        $link=sha1(md5($package_id));
                ?> 
                <tr>
                  <td><?= $p->package_name ?></td> 
                  <td><?= $p->client_name ?></td>
                  <td>
                    <div class="btn-group"> 
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default<?php echo $package_id; ?>" data-placement="top" data-original-title="Detail" ><i class="fa fa-search"></i></button>
                      <button type="button" class="btn btn-warning" data-toggle="tooltip" onclick="myFunction('<?php echo base_url('Packages/update/').$package_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-edit"></i></button>
                      <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-xl<?php //echo $package_id; ?>"   data-placement="top" data-original-title="Update" ><i class="fa fa-envelope"></i></button> -->
                      <!--<button type="button" class="btn btn-primary" data-toggle="tooltip" onclick="sendmail('<?php //echo $package_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-envelope"></i></button>-->
                      <button type="button" class="btn btn-info" data-toggle="tooltip" onclick="Newtab('<?php echo base_url('Catalog/package/').$link; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-eye"></i></button>
                      <button type="button" class="btn btn-success" data-toggle="tooltip" onclick="Newtab('<?php echo base_url('Export/package/').$package_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-file-pdf"></i></button>
                    </div>
                  </td>
                  <div class="modal fade" id="modal-xl<?php echo $package_id; ?>">
                            <div class="modal-dialog modal-xl">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Send Email</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <div class="form-group">
                                      <label for="email">Additional Email</label>
                                      <input type="text" class="form-control" name="addemail" id="addEmail"  autocomplete="off"  >
                                    </div>
                                    <div class="form-group">
                                      <label for="fname">Message</label>
                                      <textarea class="form-control textarea" rows="3" name="messageEmail" id="messageEmail" placeholder="Message ..."></textarea>
                                      <input type="hidden" class="form-control" id="packageid" value="<?php echo $package_id; ?>" >
                                    </div>
                                    <button type="submit" class="btn btn-primary" id="saveEmail">save</button>
                                  
                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  
                                </div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
                          
                  <div class="modal fade" id="modal-default<?php echo $package_id; ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Packages Detail</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                                                
                            </div>
                            <div class="modal-body">
                            <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $p->package_name; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Description</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $p->package_description; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Client</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $p->client_name; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Status</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php if($p->package_status == 1)  echo 'Active'; else echo 'Inactive'; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                
                            </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Client</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row --> 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
function Newtab(name) {
  //alert("Welcome " + name + ".");
  //window.location.replace(name);
  window.open(name, '_blank');
}
function sendmail(id) {
  
  Swal.fire({
  title: 'Are you sure?',
  text: "Are sure want to send email ?",
  type: 'warning',
  input: 'text',
  inputAttributes: {
    autocapitalize: 'off'
  },
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes!',
  showLoaderOnConfirm: true,
     
   preConfirm: function(inputValue) {
     return new Promise(function(resolve) {
          
        $.ajax({
        url: '<?php echo base_url();?>Packages/sendmail',
        type: 'POST',
           data: {id:id,input:inputValue},
           dataType: 'json'
        })
        .done(function(){
          //Swal.fire('Success!', response.message, response.status);
          Swal.fire('Success!', 'Your message has been sent ...'+inputValue, 'success');
        })
        .fail(function(){
          Swal.fire('Success!', 'Your message has been sent ...'+inputValue, 'success');
        });
     });
      },
   allowOutsideClick: false
})
}

$('#saveEmail').on('click',function(){
            
            var packageid         =$('#packageid').val();
            var messageEmail      =$('#messageEmail').val();
            
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>Packages/sendmail",
                data : {packageid:packageid , messageEmail:messageEmail, addEmail:addEmail },
                success: function(data){
                    $('#packageid').val('');
                    $('#messageEmail').val('');
                    $('.modal').modal('hide');
                    //alert(data);
                    Swal.fire({
                      title: 'Good Job!',
                      text: "Your message has been sent ...",
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'OK!'
                    }).then((result) => {
                      if (result.value) {
                        window.location.reload(true);
                      }
                    })
                }
            });
        });

</script>