<?php
$datestring = '%d/%m/%Y ';
//============================================================+
// File name   : example_003.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 003 for TCPDF class
//               Custom Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni 
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF 
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).



// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		//$image_file = K_PATH_IMAGES.'logo-upperground.png';
		//$this->Image($image_file, 10, 10, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B');
		// Title
		//$this->Cell(0, 15, 'INVOICE', 0, false, 'R', 0, '', 0, false, 'M', 'M');
		// create some HTML content
		//$html = '<p>INVOICE</p><span style="font-size:8px;font-style: italic;">#INV.09.2018.000007</span><br><span style="font-size:8px;color:green;">LUNAS</span>';

		// output the HTML content
		//$this->writeHTML($html, true, 0, true, 0,'R');
		
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		//$this->SetY(-15);
		// Set font
		//$this->SetFont('helvetica', 'I', 8);
		// Page number
        //$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        //$image_file = K_PATH_IMAGES.'BMA-Logo-Footer.png';
		//$this->Image($image_file, 10, 10, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	}

	
}

// create new PDF document
$pdf = new MYPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Baliprod Location');
$pdf->SetTitle('Package');
$pdf->SetSubject('Package');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
$pdf->SetFooterMargin(0);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', 12);

// add a page
$pdf->AddPage();

// set some text to print
// create some HTML content
// Test fonts nesting



// draw jpeg image
//$pdf->SetAlpha(0.3);

//$image_file = K_PATH_IMAGES.'baliprod-logo.png';
//$pdf->Image($image_file, 100, 100, 150, 150, '', '', '', true, 72);

// restore full opacity
//$pdf->SetAlpha(1);







// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
//$pdf->SetAlpha(0.3);
//$img_file = K_PATH_IMAGES.'Baliprod-Location-PDF-Cover-1.jpg';
$img_file = base_url().'/src/admin_assets/dist/img/BMA-2.0-PDF-Cover.jpg';
$pdf->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();
//$pdf->SetAlpha(1);


$html ='
        <h1 style="font-size:36px;color:#fff;text-transform: uppercase;">'.$packages['package_name'].'</h1>
        ';

// output the HTML content
//$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->writeHTML($html, true, false, true, false, 'C');
$y = 160;
$x = 10;
$pdf->writeHTMLCell('', '', $x, $y, $html, 0, 0, 0, true, 'C', true);

$cat_lct='';
foreach ($talents as $t) {
    $talent_id = $t->talent_id;
    $categoryname = $t->categoryname;
    
     
    $image5 = K_PATH_IMAGES .'BMA-Logo-Footer.png';
    $image6 = K_PATH_IMAGES .'production-word.png';
    $landscape1 = false;
    $landscape2 = false;
    $potrait1 = false;
    $potrait2 = false;
    $potrait3 = false;
    $potrait4 = false;

    foreach ($talentMedia as $tm) {
        if($talent_id == $tm->talent_id){
            $media_order = $tm->media_order;
            $full_url = base_url(). $tm->media_url;
            list($width, $height) = getimagesize($full_url);
            if ($width > $height) {
                // Landscape
                if($landscape1 == false){
                    $image1 = base_url() . $tm->media_url;
                    $landscape1 = true;
                }elseif($landscape2 == false){
                    $image2 = base_url() . $tm->media_url;
                    $landscape2 = true;
                }
            } else {
                // Portrait or Square
                if($potrait1 == false){
                    $image3 = base_url() . $tm->media_url;
                    $potrait1 = true;
                }elseif($potrait2 == false){
                    $image4 = base_url() . $tm->media_url;
                    $potrait2 = true;
                }
            }
            

        }
    }

    if($landscape1 == false || $landscape2 == false){
        foreach ($talentMedia as $tm) {
            if($talent_id == $tm->talent_id){
                $tempurl = '';
                if($landscape1 == false){
                    $tempurl = base_url() . $tm->media_url;
                    if($tempurl <> $image3 && $tempurl <> $image4){
                        
                        if($potrait3 == false){
                            $potrait3 = true;
                            $image1 = base_url() . $tm->media_url;
                        }
                    }
                }elseif($landscape2 == false){
                    $tempurl = base_url() . $tm->media_url;
                    if($tempurl <> $image3 && $tempurl <> $image4){
                        
                        if($potrait4 == false){
                            $potrait4 = true;
                            $image2 = base_url() . $tm->media_url;
                        }
                    }
                }
            }
            
        }
    }

    $imageHeadshot = base_url() . $t->media_url;

    $pdf->AddPage();
    // get the current page break margin
    $bMargin = $pdf->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $pdf->getAutoPageBreak();
    // disable auto-page-break
    $pdf->SetAutoPageBreak(false, 0);
    $pdf->Image($imageHeadshot, 80, '', 140, 140, '', '', 'C', true, 300, '', false, false, 0, 'CM', false, false);
    

    $pdf->Image($image5, 110, 180, 90, 25, '', '', 'L', false, 300, '', false, false, 0, 'CM', false, false);
    //$pdf->Image($image6, 235, 170, 50, 10, '', '', 'L', false, 300, '', false, false, 0, false, false, false);

    
    $html ='
        <p><strong>'.$categoryname.'</strong> | '.$t->talent_nickname.'</p>
		';
        $pdf->writeHTMLCell('', '', '', 10, $html, 0, 0, 0, true, 'C', true);


    if($landscape1 == true || $potrait3 == true){
    $pdf->AddPage();
    // get the current page break margin
    $bMargin = $pdf->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $pdf->getAutoPageBreak();
    // disable auto-page-break
    $pdf->SetAutoPageBreak(false, 0);
    $pdf->Image($image1, '', '', 270, 140, '', '', '', true, 300, '', false, false, 0, 'CM', false, false);
    

    $pdf->Image($image5, 110, 180, 90, 25, '', '', 'L', false, 300, '', false, false, 0, 'CM', false, false);
    //$pdf->Image($image6, 235, 170, 50, 10, '', '', 'L', false, 300, '', false, false, 0, false, false, false);

    
    $html ='
        <p><strong>'.$categoryname.'</strong> | '.$t->talent_nickname.'</p>
		';
        $pdf->writeHTMLCell('', '', '', 10, $html, 0, 0, 0, true, 'C', true);
    }

    if($landscape2 == true || $potrait4 == true){

    $pdf->AddPage();
    // get the current page break margin
    $bMargin = $pdf->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $pdf->getAutoPageBreak();
    // disable auto-page-break
    $pdf->SetAutoPageBreak(false, 0);
    $pdf->Image($image2, '', '', 270, 140, '', '', '', true, 300, '', false, false, 0, 'CM', false, false);   


    $pdf->Image($image5, 110, 180, 90, 25, '', '', 'L', false, 300, '', false, false, 0, 'CM', false, false);
    //$pdf->Image($image6, 235, 170, 50, 10, '', '', 'L', false, 300, '', false, false, 0, false, false, false);

    
    $html ='
        <p><strong>'.$categoryname.'</strong> | '.$t->talent_nickname.'</p>
		';
        $pdf->writeHTMLCell('', '', '', 10, $html, 0, 0, 0, true, 'C', true);

    }

    if($potrait3 == true && $potrait4 == true){
        $pdf->AddPage();
        // get the current page break margin
        $bMargin = $pdf->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $pdf->getAutoPageBreak();
        // disable auto-page-break
        $pdf->SetAutoPageBreak(false, 0);

        $pdf->Image($image1, '', '', 145, 140, '', '', 'C', true, 300, '', false, false, 0, 'CM', false, false);
        $pdf->Image($image2, 145, '', 140, 140, '', '', 'C', true, 300, '', false, false, 0, 'CM', false, false);   


        $pdf->Image($image5, 110, 180, 90, 25, '', '', 'L', false, 300, '', false, false, 0, 'CM', false, false);
        //$pdf->Image($image6, 235, 170, 50, 10, '', '', 'L', false, 300, '', false, false, 0, false, false, false);

        
        $html ='
            <p><strong>'.$categoryname.'</strong> | '.$t->talent_nickname.'</p>
            ';
            $pdf->writeHTMLCell('', '', '', 10, $html, 0, 0, 0, true, 'C', true);
    }

    $pdf->AddPage();
    // get the current page break margin
    $bMargin = $pdf->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $pdf->getAutoPageBreak();
    // disable auto-page-break
    $pdf->SetAutoPageBreak(false, 0);

    $pdf->Image($image3, '', '', 145, 140, '', '', 'C', true, 300, '', false, false, 0, 'CM', false, false);
    $pdf->Image($image4, 145, '', 140, 140, '', '', 'C', true, 300, '', false, false, 0, 'CM', false, false);   


    $pdf->Image($image5, 110, 180, 90, 25, '', '', 'L', false, 300, '', false, false, 0, 'CM', false, false);
    //$pdf->Image($image6, 235, 170, 50, 10, '', '', 'L', false, 300, '', false, false, 0, false, false, false);

    
    $html ='
        <p><strong>'.$categoryname.'</strong> | '.$t->talent_nickname.'</p>
		';
        $pdf->writeHTMLCell('', '', '', 10, $html, 0, 0, 0, true, 'C', true);

}

$pdf->AddPage();
// -- set new background ---

// get the current page break margin
$bMargin = $pdf->getBreakMargin();
// get current auto-page-break mode
$auto_page_break = $pdf->getAutoPageBreak();
// disable auto-page-break
$pdf->SetAutoPageBreak(false, 0);
// set bacground image
//$pdf->SetAlpha(0.3);
//$img_file = K_PATH_IMAGES.'Baliprod-Location-PDF-Cover-1.jpg';
$img_file = base_url().'/src/admin_assets/dist/img/BMA-2.0-PDF-Cover-Back.jpg';
$pdf->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
// restore auto-page-break status
$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
// set the starting point for the page content
$pdf->setPageMark();
//$pdf->SetAlpha(1);

// ---------------------------------------------------------

//Close and output PDF document
$namafile="Package Client";
$pdf->Output($namafile, 'I');

//============================================================+
// END OF FILE
//============================================================+
