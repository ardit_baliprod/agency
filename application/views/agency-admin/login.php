<?php
    $image = base_url() .'src/admin_assets/dist/img/login-background.jpg';
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>BMA Agency APPS - Login</title>
        <meta name="description" content="Balimodelagency apps">
        <meta name="keywords" content="model, agency, bali">
        <meta name="author" content="BMA">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
        <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        
        <!------ Include the above in your HEAD tag ---------->
        <style>
            body {
                margin:0;
                color:#000000;
                background:#c8c8c8;
                background:url(<?= $image ?>) fixed;
                background-size: cover;
                font:600 16px/18px 'Nunito Sans',sans-serif;
            }
            :after,:before{box-sizing:border-box}
            .clearfix:after,.clearfix:before{content:'';display:table}
            .clearfix:after{clear:both;display:block}
            a{color:inherit;text-decoration:none}

            .login-wrap{
                width: 100%;
                margin:auto;
                max-width:510px;
                min-height:610px;
                position:relative;
                
            }
            .login-html{
                width:100%;
                height:100%;
                position:absolute;
                padding:20px 70px 50px 70px;
                background:rgba(255,255,255,0.9);
                margin-top: 30%;
            }
            .login-html .sign-in-htm,
            .login-html .for-pwd-htm{
                top:0;
                left:0;
                right:0;
                bottom:0;
                position:absolute;
                -webkit-transform:rotateY(180deg);
                        transform:rotateY(180deg);
                -webkit-backface-visibility:hidden;
                        backface-visibility:hidden;
                -webkit-transition:all .4s linear;
                transition:all .4s linear;
            }
            .login-html .sign-in,
            .login-html .for-pwd,
            .login-form .group .check{
                display:none;
            }
            .login-html .tab,
            .login-form .group .label,
            .login-form .group .button{
                text-transform:uppercase;
            }
            .login-html .tab{
                font-size:22px;
                margin-right:15px;
                padding-bottom:5px;
                margin:0 15px 10px 0;
                display:inline-block;
                border-bottom:2px solid transparent;
            }
            .login-html .sign-in:checked + .tab,
            .login-html .for-pwd:checked + .tab{
                color:#000;
                border-color:#1161ee;
            }
            .login-form{
                min-height:345px;
                position:relative;
                -webkit-perspective:1000px;
                        perspective:1000px;
                -webkit-transform-style:preserve-3d;
                        transform-style:preserve-3d;
            }
            .login-form .group{
                margin-bottom:15px;
            }
            .login-form .group .label,
            .login-form .group .input,
            .login-form .group .button{
                width:100%;
                color:#000000;
                display:block;
            }
            .login-form .group .input,
            .login-form .group .button{
                border:none;
                padding:15px 20px;
                border-radius:25px;
                background:rgba(152,152,152,.2);
            }
            .login-form .group input[data-type="password"]{
                text-security:circle;
                -webkit-text-security:circle;
            }
            .login-form .group .label{
                color:#000000;
                font-size:14px;
            }
            .login-form .group .button{
                background:#000000;
                color:#fff;
            }
            .login-form .group label .icon{
                width:15px;
                height:15px;
                border-radius:2px;
                position:relative;
                display:inline-block;
                background:rgba(255,255,255,.1);
            }
            .login-form .group label .icon:before,
            .login-form .group label .icon:after{
                content:'';
                width:10px;
                height:2px;
                background:#fff;
                position:absolute;
                -webkit-transition:all .2s ease-in-out 0s;
                transition:all .2s ease-in-out 0s;
            }
            .login-form .group label .icon:before{
                left:3px;
                width:5px;
                bottom:6px;
                -webkit-transform:scale(0) rotate(0);
                        transform:scale(0) rotate(0);
            }
            .login-form .group label .icon:after{
                top:6px;
                right:0;
                -webkit-transform:scale(0) rotate(0);
                        transform:scale(0) rotate(0);
            }
            .login-form .group .check:checked + label{
                color:#fff;
            }
            .login-form .group .check:checked + label .icon{
                background:#1161ee;
            }
            .login-form .group .check:checked + label .icon:before{
                -webkit-transform:scale(1) rotate(45deg);
                        transform:scale(1) rotate(45deg);
            }
            .login-form .group .check:checked + label .icon:after{
                -webkit-transform:scale(1) rotate(-45deg);
                        transform:scale(1) rotate(-45deg);
            }
            .login-html .sign-in:checked + .tab + .for-pwd + .tab + .login-form .sign-in-htm{
                -webkit-transform:rotate(0);
                        transform:rotate(0);
            }
            .login-html .for-pwd:checked + .tab + .login-form .for-pwd-htm{
                -webkit-transform:rotate(0);
                        transform:rotate(0);
            }

            .hr{
                height:2px;
                margin:60px 0 50px 0;
                background:rgba(10,10,10,.2);
            }
            .foot-lnk{
                text-align:center;
            }
            .logo-login{
                width:100%;
            }
        </style>
        <?php echo $script_captcha; // javascript recaptcha ?>
    </head>
    <body>
        
        <div class="login-wrap">
            
            <div class="login-html">
            <img class="logo-login" src="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-app.png" />
            <?php if($this->session->flashdata('error')) { ?>
                  <div class="text-danger">
                    
                    <p><strong>ERROR !</strong> <?php echo $this->session->flashdata('error'); ?></p>
                  </div>
                <?php } ?>
                
                <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab"></label>
                <input id="tab-2" type="radio" name="tab" class="for-pwd"><label for="tab-2" class="tab"></label>
                
                <div class="login-form">
                    
                    <div class="sign-in-htm">
                    <form action="<?php echo base_url('AdminLogin/validation'); ?>" method="post">
                        <div class="group">
                            <label for="user" class="label">Username or Email</label>
                            <input id="user" type="text" name="user_name" class="input">
                            <span class="text-danger"><?php echo form_error('user_name'); ?></span>
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Password</label>
                            <input id="pass" type="password" class="input" name="user_password" data-type="password">
                            <span class="text-danger"><?php echo form_error('user_password'); ?></span>
                        </div>
                        <div class="group">
                        <?php echo $captcha // tampilkan recaptcha ?>
                        </div>
                        <div class="group">
                            <input type="submit" class="button" value="Sign In">
                        </div>
                        <div class="hr"></div>
                        
                    </form>
                    </div>
                    
                </div>
                <div class="for-pwd-htm">
				</div>
            </div>
        </div>

    </body>
</html>