<?php
  
    $judul ='Add Talent';
    
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">  
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Talent">Data Talent</a></li>
              <li class="breadcrumb-item active">Talent Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            
            <!-- Horizontal Form -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><?= $judul ?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal" method="post" enctype="multipart/form-data"  action="<?php echo base_url('Talent/created'); ?>" >
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputFname" class="col-sm-2 col-form-label">First Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputFname" name="fname" required placeholder="First Name"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputLname" class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputLname" name="lname" placeholder="Last Name"  >
                    </div>
                  </div>  
                  <div class="form-group row">
                    <label for="inputname" class="col-sm-2 col-form-label">Nickname</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputname" name="name" required placeholder="Nickname" onkeyup="sync2()"  >
                      
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputSlug" class="col-sm-2 col-form-label">Slug</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputSlug" name="slug" placeholder="slug" style="text-transform: lowercase"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" name="email"  placeholder="Email"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPhone" class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputBirth" class="col-sm-2 col-form-label">Date of Birth</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="birth" name="birth" placeholder="Date of Birth">
                        
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputAge" class="col-sm-2 col-form-label">Age</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputAge" name="age" readonly placeholder="Age"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputHeight" class="col-sm-2 col-form-label">Height (cm)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputHeight" name="height" placeholder="Height" min="0" max="300" step="0.1" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputBust" class="col-sm-2 col-form-label">Bust (cm)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputBust" name="bust" placeholder="Bust" min="0" max="300" step="0.1" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputChest" class="col-sm-2 col-form-label">Chest (cm)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputChest" name="chest" placeholder="Chest" min="0" max="300" step="0.1" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputWaist" class="col-sm-2 col-form-label">Waist (cm)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputWaist" name="waist" placeholder="Waist" min="0" max="300" step="0.1" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputHips" class="col-sm-2 col-form-label">Hips (cm)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputHips" name="hips" placeholder="Hips" min="0" max="300" step="0.1" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputShoe" class="col-sm-2 col-form-label">Shoe </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputShoe" name="shoe" placeholder="Shoe" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputCollar" class="col-sm-2 col-form-label">Collar (cm)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputCollar" name="collar" placeholder="Collar" min="0" max="300" step="0.1" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPants" class="col-sm-2 col-form-label">Pants (cm)</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputPants" name="pants" placeholder="Pants" min="0" max="300" step="0.1" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputDress" class="col-sm-2 col-form-label">Dress </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputDress" name="dress" placeholder="Dress"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputShirt" class="col-sm-2 col-form-label">Shirt </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputShirt" name="shirt" placeholder="Shirt"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputSuit" class="col-sm-2 col-form-label">Suit </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputSuit" name="suit" placeholder="Suit"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputBra" class="col-sm-2 col-form-label">Bra </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputBra" name="bra" placeholder="Bra"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputHair" class="col-sm-2 col-form-label">Hair </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputHair" name="hair" placeholder="Hair"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEye" class="col-sm-2 col-form-label">Eye </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEye" name="eye" placeholder="Eye"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="City" class="col-sm-2 col-form-label">Gender</label>
                    <div class="col-sm-10">
                      <div class="form-group">
                          <select class="form-control select2" name="gender" style="width: 100%;">
                          <?php 
                              foreach ($gender as $g) { 
                                  $gender_id=$g->gender_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                  $gender_name = $g->gender_name;
                          ?>
                            <option value="<?= $gender_id ?>"><?= $gender_name ?></option>
                          <?php } ?>
                            
                          </select>
                      </div>
                      
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="City" class="col-sm-2 col-form-label">Category</label>
                    <div class="col-sm-10">
                      <select class="form-control select2" multiple="multiple" name="category[ ]" style="width: 100%;">
                      <?php 
                          foreach ($category as $c) { 
                              $category_id=$c->category_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                              $category_name = $c->category_name; 
                      ?>
                        <option value="<?= $category_id ?>"><?= $category_name ?></option>
                      <?php } ?>
                        
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="Ethnicity" class="col-sm-2 col-form-label">Ethnicity</label>
                    <div class="col-sm-10">
                      <div class="form-group">
                          <select class="form-control select2" name="ethnicity" style="width: 100%;">
                          <?php 
                              foreach ($ethnicity as $e) { 
                                  $ethnicity_id=$e->ethnicity_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                  $ethnicity_name = $e->ethnicity_name;
                          ?>
                            <option value="<?= $ethnicity_id ?>"><?= $ethnicity_name ?></option>
                          <?php } ?>
                            
                          </select>
                      </div>
                      
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="Compelxion" class="col-sm-2 col-form-label">Complexion</label>
                    <div class="col-sm-10">
                      <div class="form-group">
                          <select class="form-control select2" multiple="multiple" name="compelxion[ ]"  style="width: 100%;">
                          <?php 
                              foreach ($compelxion as $c) { 
                                  $compelxion_id=$c->compelxion_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                  $compelxion_name = $c->compelxion_name;
                          ?>
                            <option value="<?= $compelxion_id ?>"><?= $compelxion_name ?></option>
                          <?php } ?>
                            
                          </select>
                      </div>
                      
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputInstagram" class="col-sm-2 col-form-label">Instagram </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputInstagram" name="instagram" placeholder="Instagram"  >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPortofolio" class="col-sm-2 col-form-label">Portofolio Link </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPortofolio" name="portofolio" placeholder="Portofolio Link"  >
                    </div>
                  </div>                
                  <div class="form-group row">
                    <label for="inputDesc" class="col-sm-2 col-form-label">Bio</label>
                    <div class="col-sm-10">
                       <textarea class="form-control" rows="3" name="bio" placeholder="Bio ..."></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputDesc" class="col-sm-2 col-form-label">Notes</label>
                    <div class="col-sm-10">
                       <textarea class="form-control" rows="3" name="notes" placeholder="Notes ..."></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                          <label for="inputtags" class="col-sm-2 col-form-label">Tags</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="tags" id="search_data"  autocomplete="off" >
                            
                          </div>
                  </div>
                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="reset" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid --> 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function sync()
{
  var n1 = document.getElementById('inputname');
  var n2 = document.getElementById('inputSlug');
  n2.value = n1.value;
}
function sync2()
{
    $("#inputname").copyTo("#inputSlug");
}
$.fn.copyTo = function(selector) {
    $(selector).val($(this[0]).val().replace(/\s/g, "-"));
    
};
</script>