<?php
  
    $judul ='Update Talent';

    $talent_id              = stripslashes($talent['talent_id']);
    $talent_fname           = stripslashes($talent['talent_fname']);
    $talent_lname           = stripslashes($talent['talent_lname']);
    $talent_nickname        = stripslashes($talent['talent_nickname']);
    $talent_slug            = stripslashes($talent['talent_slug']);
    $talent_email           = stripslashes($talent['talent_email']);
    $talent_phone           = stripslashes($talent['talent_phone']);
    $talent_height          = stripslashes($talent['talent_height']);
    $talent_bust            = stripslashes($talent['talent_bust']);
    $talent_waist           = stripslashes($talent['talent_waist']);
    $talent_hips            = stripslashes($talent['talent_hips']);
    $talent_shoe            = stripslashes($talent['talent_shoe']);
    $talent_datebirth       = stripslashes($talent['talent_datebirth']);
    if($talent_datebirth == '0000-00-00'){
        $talent_datebirth = '';
    }
    $talent_age             = stripslashes($talent['talent_age']);
    $talent_bio             = stripslashes($talent['talent_bio']);
    $talent_hairColor       = stripslashes($talent['talent_hairColor']);
    $talent_eyeColor        = stripslashes($talent['talent_eyeColor']);
    $talent_tag             = stripslashes($talent['talent_tag']);
    $talent_status          = stripslashes($talent['talent_status']);
    $gender_id              = stripslashes($talent['gender_id']);
    $ethnicity_id           = stripslashes($talent['ethnicity_id']);
    $talent_notes           = stripslashes($talent['talent_notes']);
    $talent_instagram       = stripslashes($talent['talent_instagram']);
    $talent_portofolio      = stripslashes($talent['talent_portofolio']);
    $talent_dress           = stripslashes($talent['talent_dress']);
    $talent_chest           = stripslashes($talent['talent_chest']);
    $talent_suit            = stripslashes($talent['talent_suit']);
    $talent_collar          = stripslashes($talent['talent_collar']);
    $talent_bra             = stripslashes($talent['talent_bra']);
    $talent_shirt           = stripslashes($talent['talent_shirt']);
    $talent_pants           = stripslashes($talent['talent_pants']);
    

    $headshot_image = null;
    $headshot_video = null;
    $headshot_image_id = null;
    $headshot_video_id = null;
    $include = false;
    foreach ($headshot as $h) {
        if (strpos($h->media_mime, 'image') !== false) {
            //echo 'true';
            $headshot_image = base_url().$h->media_url;
            $headshot_image_id = $h->media_id; 
        }
        if (strpos($h->media_mime, 'video') !== false) {
            //echo 'true';
            $headshot_video = base_url().$h->media_url; 
            $headshot_video_id = $h->media_id;
        }
    }

?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">  
          <div class="col-sm-6">
            <h1><?= $judul ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Listing">Data Listing</a></li>
              <li class="breadcrumb-item active">Update Talent</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Attention!</h4>
            attention. Please check your data!!.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
    <?php } ?>
        <div class="row">
            <div class="card card-primary card-outline" style="width:100%">
            <div class="card-header">
                <h3 class="card-title">
                <i class="fas fa-edit"></i>
                <?= $judul ?>
                </h3>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="custom-content-above-home-tab" data-toggle="pill" href="#custom-content-above-home" role="tab" aria-controls="custom-content-above-home" aria-selected="true">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-profile-tab" data-toggle="pill" href="#custom-content-above-profile" role="tab" aria-controls="custom-content-above-profile" aria-selected="false">Headshot</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-messages-tab" data-toggle="pill" href="#custom-content-above-messages" role="tab" aria-controls="custom-content-above-messages" aria-selected="false">Photo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-settings-tab" data-toggle="pill" href="#custom-content-above-settings" role="tab" aria-controls="custom-content-above-settings" aria-selected="false">Video</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-settings-tab" data-toggle="pill" href="#custom-content-above-polaroid" role="tab" aria-controls="custom-content-above-polaroid" aria-selected="false">Polaroid</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="custom-content-above-settings-tab" data-toggle="pill" href="#custom-content-above-cover" role="tab" aria-controls="custom-content-above-cover" aria-selected="false">Cover Video</a>
                </li>
                </ul>
                <div class="tab-custom-content">
                <p class="lead mb-0">
                    <button type="button" class="btn btn-info" data-toggle="tooltip" onclick="Newtab('<?php echo base_url('catalog/talent/').$talent_slug; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-eye"></i></button>
                </p>
                </div>
                <div class="tab-content" id="custom-content-above-tabContent">
                <div class="tab-pane fade show active" id="custom-content-above-home" role="tabpanel" aria-labelledby="custom-content-above-home-tab">
                    <!-- Horizontal Form -->
                    <div class="card card-info">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" enctype="multipart/form-data"  action="<?php echo base_url('Talent/updated'); ?>" >
                        <div class="card-body">
                        <div class="form-group row">
                            <label for="inputFname" class="col-sm-2 col-form-label">First Name</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputFname" name="fname" required placeholder="First Name" value="<?= $talent_fname ?>" >
                              <?php if($type == 'update') { echo form_hidden('id', $talent_id); } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputLname" class="col-sm-2 col-form-label">Last Name</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputLname" name="lname" placeholder="Last Name" value="<?= $talent_lname ?>" >
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label for="inputname" class="col-sm-2 col-form-label">Nickname</label>
                            <div class="col-sm-10">
                               <input type="text" class="form-control" id="inputname" name="name" required placeholder="Nickname" onkeyup="sync2()" value="<?= $talent_nickname ?>" >
                            
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputSlug" class="col-sm-2 col-form-label">Slug</label>
                            <div class="col-sm-10">
                               <input type="text" class="form-control" id="inputSlug" name="slug" placeholder="slug" style="text-transform: lowercase" value="<?= $talent_slug ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputEmail" name="email"  placeholder="Email" value="<?= $talent_email ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPhone" class="col-sm-2 col-form-label">Phone</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="Phone" value="<?= $talent_phone ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBirth" class="col-sm-2 col-form-label">Date of Birth</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="birth" name="birth" placeholder="Date of Birth" value="<?= $talent_datebirth ?>" >
                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputAge" class="col-sm-2 col-form-label">Age</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputAge" name="age" readonly placeholder="Age" value="<?= $talent_age ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputHeight" class="col-sm-2 col-form-label">Height (cm)</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputHeight" name="height" placeholder="Height" min="0" max="300" step="0.1" value="<?= round($talent_height,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBust" class="col-sm-2 col-form-label">Bust (cm)</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputBust" name="bust" placeholder="Bust" min="0" max="300" step="0.1" value="<?= round($talent_bust,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputChest" class="col-sm-2 col-form-label">Chest (cm)</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputChest" name="chest" placeholder="Chest" min="0" max="300" step="0.1" value="<?= round($talent_chest,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputWaist" class="col-sm-2 col-form-label">Waist (cm)</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputWaist" name="waist" placeholder="Waist" min="0" max="300" step="0.1" value="<?= round($talent_waist,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputHips" class="col-sm-2 col-form-label">Hips (cm)</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputHips" name="hips" placeholder="Hips" min="0" max="300" step="0.1" value="<?= round($talent_hips,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputShoe" class="col-sm-2 col-form-label">Shoe</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputShoe" name="shoe" placeholder="Shoe" value="<?= round($talent_shoe,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputCollar" class="col-sm-2 col-form-label">Collar (cm)</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputCollar" name="collar" placeholder="Collar" min="0" max="300" step="0.1" value="<?= round($talent_collar,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPants" class="col-sm-2 col-form-label">Pants (cm)</label>
                            <div class="col-sm-10">
                            <input type="number" class="form-control" id="inputPants" name="pants" placeholder="Pants" min="0" max="300" step="0.1" value="<?= round($talent_pants,0) ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputDress" class="col-sm-2 col-form-label">Dress </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputDress" name="dress" placeholder="Dress" value="<?= $talent_dress ?>"  >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputShirt" class="col-sm-2 col-form-label">Shirt </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputShirt" name="shirt" placeholder="Shirt" value="<?= $talent_shirt ?>"  >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputSuit" class="col-sm-2 col-form-label">Suit </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputSuit" name="suit" placeholder="Suit" value="<?= $talent_suit ?>"  >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputBra" class="col-sm-2 col-form-label">Bra </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputBra" name="bra" placeholder="Bra" value="<?= $talent_bra ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputHair" class="col-sm-2 col-form-label">Hair </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputHair" name="hair" placeholder="Hair" value="<?= $talent_hairColor ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEye" class="col-sm-2 col-form-label">Eye </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputEye" name="eye" placeholder="Eye" value="<?= $talent_eyeColor ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="City" class="col-sm-2 col-form-label">Gender</label>
                            <div class="col-sm-10">
                            <div class="form-group">
                                <select class="form-control select2" name="gender" style="width: 100%;">
                                <?php 
                                    foreach ($gender as $g) { 
                                        $gender_id_select=$g->gender_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                        $gender_name = $g->gender_name;
                                ?>
                                    <option <?php if($gender_id_select == $gender_id){ echo 'selected'; } ?> value="<?= $gender_id_select ?>"><?= $gender_name ?></option>
                                <?php } ?>
                                    
                                </select>
                            </div>
                            
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="City" class="col-sm-2 col-form-label">Category</label>
                            <div class="col-sm-10">
                            <select class="form-control select2" multiple="multiple" name="category[ ]" style="width: 100%;">
                            <?php 
                                foreach ($category as $c) { 
                                    $include = false;
                                    $category_id_select=$c->category_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                    $category_name = $c->category_name;
                                    foreach ($talentCategory as $tc) {
                                        if($category_id_select == $tc->category_id)
                                        {
                                          $include = true;
                                        } 
                                      }
                            ?>
                                <option <?php if($include == true){ echo 'selected'; } ?> value="<?= $category_id_select ?>"><?= $category_name ?></option>
                            <?php } ?>
                                
                            </select> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Ethnicity" class="col-sm-2 col-form-label">Ethnicity</label>
                            <div class="col-sm-10">
                            <div class="form-group">
                                <select class="form-control select2" name="ethnicity" style="width: 100%;">
                                <?php 
                                    foreach ($ethnicity as $e) { 
                                        $ethnicity_id_select=$e->ethnicity_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                        $ethnicity_name = $e->ethnicity_name;
                                ?>
                                    <option <?php if($ethnicity_id_select == $ethnicity_id){ echo 'selected'; } ?> value="<?= $ethnicity_id_select ?>"><?= $ethnicity_name ?></option>
                                <?php } ?>
                                    
                                </select>
                            </div>
                            
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Compelxion" class="col-sm-2 col-form-label">Complexion</label>
                            <div class="col-sm-10">
                            <div class="form-group">
                                <select class="form-control select2" multiple="multiple" name="compelxion[ ]" style="width: 100%;">
                                <?php 
                                    foreach ($compelxion as $c) { 
                                        $include = false;
                                        $compelxion_id_select=$c->compelxion_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                        $compelxion_name = $c->compelxion_name;
                                        foreach ($talentCompelxion as $tc) {
                                            if($compelxion_id_select == $tc->compelxion_id)
                                            {
                                              $include = true;
                                            } 
                                          }
                                ?>
                                    <option <?php if($include == true){ echo 'selected'; } ?> value="<?= $compelxion_id_select ?>"><?= $compelxion_name ?></option>
                                <?php } ?>
                                    
                                </select>
                            </div>
                            
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputInstagram" class="col-sm-2 col-form-label">Instagram </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputInstagram" name="instagram" placeholder="Instagram" value="<?= $talent_instagram ?>"  >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPortofolio" class="col-sm-2 col-form-label">Portofolio Link </label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPortofolio" name="portofolio" placeholder="Portofolio Link" value="<?= $talent_portofolio ?>" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputDesc" class="col-sm-2 col-form-label">Bio</label>
                            <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="bio" placeholder="Bio ..."><?= $talent_bio ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputDesc" class="col-sm-2 col-form-label">Notes</label>
                            <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="notes" placeholder="Notes ..."><?= $talent_notes ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                                <label for="inputtags" class="col-sm-2 col-form-label">Tags</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="tags" id="search_data"  autocomplete="off" value="<?= $talent_tag ?>" >
                                    
                                </div>
                        </div>
                        <div class="form-group row">
                                <label for="inputStatus" class="col-sm-2 col-form-label">Status</label>
                                
                                    <div class="col-sm-10">
                                        <select class="form-control" name="status" >
                                            <option <?php if($talent_status==1) { echo "selected"; } ?> value="1">Active</option>
                                            <option <?php if($talent_status==0) { echo "selected"; } ?> value="0">Inactive</option>
                                        </select>
                                    </div>
                        </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                    </div>
                    <!-- /.card --> 
                </div>
                <div class="tab-pane fade" id="custom-content-above-profile" role="tabpanel" aria-labelledby="custom-content-above-profile-tab">
                    <!-- Horizontal Form -->
                    <div class="card card-info">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url('Talent/headshot'); ?>" >
                        <div class="card-body">
                        
                        <div class="form-group row">
                                <label for="exampleInputFile">Headshot</label>
                                <div class="input-group">
                                    <div class="custom-file"> 
                                    <input type="file"  class="custom-file-input" id="exampleInputFile" name="image" >
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    
                                </div>
                        </div>
                        
                        <div class="form-group row">
                          <label for="VideoInputFile">Video Preview</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="hidden" class="form-control" id="videoLocationID" name="videoLocationID" <?php  echo 'value="'.$talent_id .'"'; ?>  >
                              <input type="file" class="custom-file-input" name="video" id="VideoInputFile"  >
                              <label class="custom-file-label" for="VideoInputFile">Choose file</label>
                            </div>
                            
                          </div>
                        </div>    
                            
                       
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                    </div>
                    <!-- /.card -->

                    <div id="PreviewHeadshot">
                      <div class="row">
                          <div class="col-md-6">
                            <?php if($headshot_image){ ?>
                             <img class="img-fluid" src="<?= $headshot_image ?>" alt="Photo">
                             <br/>
                             <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="<?= $headshot_image_id ?>" 
                             data-toggle="modal" data-target="#modal-delete<?php echo $headshot_image_id; ?>" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                             <div class="modal fade" id="modal-delete<?php echo $headshot_image_id; ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Delete Media</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                                                        
                                    </div>
                                    
                                    <div class="modal-body">
                                    <form class="form-horizontal" method="post" action="<?php echo base_url().'Talent/deleteMedia/'.$headshot_image_id;?>">
                                    <div class="col-md-12">
                                        <p>Are you sure to delete this Media ?</p> 
                                        <?php echo form_hidden('Mediaid', $headshot_image_id); ?>
                                        <?php echo form_hidden('TalentId', $talent_id); ?>
                                    </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary pull-left">Yes</button>
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                                        
                                    </form>
                                    </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <?php } ?> 
                          </div>
                          <div class="col-md-6">
                            <?php if($headshot_video){ ?>
                            <video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" style="width: 600px; height: 400px" >
                                <source src="<?= $headshot_video ?>" type="video/mp4">
                                <source src="<?= $headshot_video ?>" type="video/webm">
                                <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a web browser that
                                <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                </p>
                            </video>
                            <br/>
                             <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="<?= $headshot_video_id ?>" 
                             data-toggle="modal" data-target="#modal-delete<?php echo $headshot_video_id; ?>" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                             <div class="modal fade" id="modal-delete<?php echo $headshot_video_id; ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Delete Media</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                                                        
                                    </div>
                                    
                                    <div class="modal-body">
                                    <form class="form-horizontal" method="post" action="<?php echo base_url().'Talent/deleteMedia/'.$headshot_video_id;?>">
                                    <div class="col-md-12">
                                        <p>Are you sure to delete this Media ?</p> 
                                        <?php echo form_hidden('Mediaid', $headshot_video_id); ?>
                                        <?php echo form_hidden('TalentId', $talent_id); ?>
                                    </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary pull-left">Yes</button>
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                                        
                                    </form>
                                    </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <?php } ?>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="custom-content-above-messages" role="tabpanel" aria-labelledby="custom-content-above-messages-tab">
                    <form action="<?= base_url('Talent/fileupload') ?>" class="dropzone" id='fileupload'>
                        <input type="hidden" class="form-control" id="inputTalentID" name="inputTalentID" <?php  echo 'value="'.$talent_id .'"'; ?>  >
	   	            </form>
                     <br />
                     <br />
                      
                     <br />
                     <br />
                    <!--<div id="preview"></div>  -->
                    <strong><i class="fas fa-bars  mr-1"></i> Gallery</strong>
                    <div class="row" id="sortable">
                              
                    </div>
                    <div class="row" style="clear: both; margin-top: 20px;">
                      <button type="button" class="btn btn-lg btn-success" id="SubmitOrder">Save</button>
                    </div>
                    
                </div>
                <div class="tab-pane fade" id="custom-content-above-settings" role="tabpanel" aria-labelledby="custom-content-above-settings-tab">
                    <!-- Horizontal Form -->
                    <div class="card card-info">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url('Talent/embed'); ?>" >
                    <div class="card-body">
                        
                        <div class="form-group">
                          <label for="embedVideoUpload">File input</label>
                          <div class="input-group">
                            <div class="custom-file">
                            <input type="hidden" class="form-control" name="Talentid" <?php  echo 'value="'.$talent_id .'"'; ?>  >
                              <input type="file" class="custom-file-input" name="file" id="embedVideoUpload"  >
                              <label class="custom-file-label" for="embedVideoUpload">Choose file</label>
                            </div>
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputembed" class="col-sm-2 col-form-label">Embed</label>
                          
                            <input type="text" class="form-control" id="inputembed" name="embed" placeholder="Embed Video"   >
                            <?php echo form_hidden('title', $talent_slug);  ?>
                          
                        </div>
                        
                      </div>
                      <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-info">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Cancel</button>
                        </div>
                        <!-- /.card-footer -->

                    </form>

                    <br />
                    <div id="PreviewGalleryVideo">
                        <div class="row">
                                <?php 
                                    foreach ($galleryVideo as $gv) {
                                        $idGalVideo = $gv->media_id;
                                        if($gv->media_mime == 'video/embed'){
                                            $media_url = stripslashes($gv->media_url);
                                            
                                ?>
                                <div class="col-md-6">
                                    <p><?= $media_url ?></p>
                                    <br/>
                                    <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="<?= $idGalVideo ?>" 
                             data-toggle="modal" data-target="#modal-delete<?php echo $idGalVideo; ?>" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                                    <div class="modal fade" id="modal-delete<?php echo $idGalVideo; ?>">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete Media</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                                                
                                            </div>
                                            
                                            <div class="modal-body">
                                            <form class="form-horizontal" method="post" action="<?php echo base_url().'Talent/deleteMedia/'.$idGalVideo;?>">
                                            <div class="col-md-12">
                                                <p>Are you sure to delete this Media ?</p> 
                                                <?php echo form_hidden('Mediaid', $idGalVideo); ?>
                                                <?php echo form_hidden('TalentId', $talent_id); ?>
                                            </div>
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary pull-left">Yes</button>
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                                                
                                            </form>
                                            </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </div>
                                <?php
                                        }
                                        else{
                                ?>
                                <div class="col-md-6">
                                
                                    <video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" style="width: 600px; height: 400px" >
                                        <source src="<?= base_url(). $gv->media_url ?>" type="video/mp4">
                                        <source src="<?= base_url(). $gv->media_url ?>" type="video/webm">
                                        <p class="vjs-no-js">
                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                        </p>
                                    </video>
                                    
                                    <br/>
                                    <button type="button" class="btn btn-danger" data-placement="top" data-original-title="remove" id="<?= $idGalVideo ?>" 
                             data-toggle="modal" data-target="#modal-delete<?php echo $idGalVideo; ?>" style="margin: 10px;"><i class="fa fa-trash"></i> &nbsp;Remove</button>
                                    <div class="modal fade" id="modal-delete<?php echo $idGalVideo; ?>">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete Media</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                                                
                                            </div>
                                            
                                            <div class="modal-body">
                                            <form class="form-horizontal" method="post" action="<?php echo base_url().'Talent/deleteMedia/'.$idGalVideo;?>">
                                            <div class="col-md-12">
                                                <p>Are you sure to delete this Media ?</p> 
                                                <?php echo form_hidden('Mediaid', $idGalVideo); ?>
                                                <?php echo form_hidden('TalentId', $talent_id); ?>
                                            </div>
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary pull-left">Yes</button>
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                                                
                                            </form>
                                            </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </div>
                                <?php } } ?>
                        </div>
                    </div>
                    </div>
                    <!-- /.card --> 

                </div>
                <div class="tab-pane fade" id="custom-content-above-polaroid" role="tabpanel" aria-labelledby="custom-content-above-polaroid-tab">
                    <form action="<?= base_url('Talent/polaroidUpload') ?>" class="dropzone" id='polaroidUpload'>
                        <input type="hidden" class="form-control" id="inputPolaroidTalentID" name="inputPolaroidTalentID" <?php  echo 'value="'.$talent_id .'"'; ?>  >
	   	            </form>
                     <br />
                     <br />
                     <br />
                     <br />
                    <!--<div id="preview"></div>  -->
                    <strong><i class="fas fa-bars  mr-1"></i> Polaroid</strong>
                    <div class="row" id="sortablePolaroid">
                              
                    </div>
                    <div class="row" style="clear: both; margin-top: 20px;">
                      <button type="button" class="btn btn-lg btn-success" id="SubmitOrderPolaroid">Save</button>
                    </div>
                     
                </div>
                <div class="tab-pane fade" id="custom-content-above-cover" role="tabpanel" aria-labelledby="custom-content-above-cover-tab">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Cover Video</h3>

                            <div class="card-tools">
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover">
                            <thead>
                                <tr>
                                <th>No</th>
                                <th>Video Name</th>
                                <th>Cover</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $noCover=0;
                                    foreach ($coverVideo as $cv) {
                                        $noCover++;
                                ?>
                                <tr>
                                    <td><?= $noCover ?></td>
                                    <td><?= $cv->videoname ?></td>
                                    <td><img clas="img" src="<?= base_url().$cv->media_url ?>" width="280px" height="180px" /></td>
                                    <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-cover<?php echo $cv->media_id; ?>"  data-placement="top" data-original-title="Activated" ><i class="fa fa-trash"></i></button></td>
                                    <div class="modal fade" id="modal-cover<?php echo $cv->media_id; ?>">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete Media</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                                                
                                            </div>
                                            
                                            <div class="modal-body">
                                            <form class="form-horizontal" method="post" action="<?php echo base_url().'Talent/deleteMedia/'.$cv->media_id;?>">
                                            <div class="col-md-12">
                                                <p>Are you sure to delete this Media ?</p> 
                                                <?php echo form_hidden('Mediaid', $cv->media_id); ?>
                                                <?php echo form_hidden('TalentId', $talent_id); ?>
                                            </div>
                                                
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary pull-left">Yes</button>
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                                                
                                            </form>
                                            </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </tr>
                                <?php } ?>
                            </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Add Cover</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url('Talent/coverUpload'); ?>">
                            <div class="card-body">
                            <div class="form-group row">
                                <label for="Ethnicity" >Video</label>
                                    <select class="form-control select2" name="video" style="width: 100%;">
                                    <?php 
                                        foreach ($videoUncover as $v) { 
                                            $media_id=$v->media_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                                            $media_name = $v->media_name;
                                    ?>
                                        <option value="<?= $media_id ?>"><?= $media_name ?></option>
                                    <?php } ?>
                                        
                                    </select>
                                
                            </div>
                            <div class="form-group row">
                                <label for="coverUpload">Cover</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                    <input type="hidden" class="form-control" name="Talentid" <?php  echo 'value="'.$talent_id .'"'; ?>  >
                                    <input type="file" class="custom-file-input" name="file" id="coverUpload"  >
                                    <label class="custom-file-label" for="coverUpload">Choose file</label>
                                    </div>
                                    
                                </div>
                            </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="submit" class="btn btn-default float-right">Cancel</button>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <!-- /.card -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid --> 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
function Newtab(name) {
  //alert("Welcome " + name + ".");
  //window.location.replace(name);
  window.open(name, '_blank');
}
function load_image()
      {
        var inputTalentID  =$('#inputTalentID').val();
        $.ajax({
        url:"<?php echo site_url('Talent/ListImageGallery'); ?>",
        method :"POST",
        data : {inputTalentID:inputTalentID},
        success:function(data){
          $('#sortable').html(data);
        }
        });
      }
      Dropzone.options.fileupload  = {
        acceptedFiles: 'image/*',
        init: function() {
          this.on('queuecomplete', function(){
            if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
              //window.location.replace(name);
              //window.location.reload(true);
              var _this = this;
              _this.removeAllFiles();
              //Swal.fire('Good job!','Photo has been uploaded!!!','success');
            }
            load_image();
            });
            this.on("sending",function(a,b,c){
          var inputTalentID  =$('#inputTalentID').val();
          c.append("inputTalentID",inputTalentID); //Menmpersiapkan token untuk masing masing foto
        });
        }

        
    };
    function load_image_polaroid()
      {
        var inputTalentID  =$('#inputPolaroidTalentID').val();
        $.ajax({
        url:"<?php echo site_url('Talent/ListImagePolaroid'); ?>",
        method :"POST",
        data : {inputTalentID:inputTalentID},
        success:function(data){
          $('#sortablePolaroid').html(data);
        }
        });
      }
      Dropzone.options.polaroidUpload  = {
        acceptedFiles: 'image/*',
        init: function() {
          this.on('queuecomplete', function(){
            if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
              //window.location.replace(name);
              //window.location.reload(true);
              var _this = this;
              _this.removeAllFiles();
              //Swal.fire('Good job!','Photo has been uploaded!!!','success');
            }
            load_image_polaroid();
            });
            this.on("sending",function(a,b,c){
          var inputTalentID  =$('#inputPolaroidTalentID').val();
          c.append("inputTalentID",inputTalentID); //Menmpersiapkan token untuk masing masing foto
        });
        }

        
    };      
function sync()
{
  var n1 = document.getElementById('inputname');
  var n2 = document.getElementById('inputSlug');
  n2.value = n1.value;
}
function sync2()
{
    $("#inputname").copyTo("#inputSlug");
}
$.fn.copyTo = function(selector) {
    $(selector).val($(this[0]).val().replace(/\s/g, "-"));
    
};


function confirmDelete(id) {
  
  Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!',
  showLoaderOnConfirm: true,
     
   preConfirm: function() {
     return new Promise(function(resolve) {
          
        $.ajax({
        url: '<?php echo base_url();?>Talent/removeMedia',
        type: 'POST',
           data: 'id='+id,
           dataType: 'json'
        })
        .done(function(response){
          Swal.fire('Deleted!', response.message, response.status);
          //setTimeout(function(){ document.location.reload(true) }, 3000);
         //load_image(); 
         //load_video();
         load_image();
         load_image_polaroid();
        })
        .fail(function(){
          Swal.fire('Oops...', 'Something went wrong with ajax !', 'error');
          //Swal.fire('Deleted!', response.message, response.status);
          //setTimeout(function(){ document.location.reload(true) }, 3000);
        });
     });
      },
   allowOutsideClick: false
})
}

function confirmDelete2(Mediaid,TalentId) {
    Swal.fire({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!"
    }, function (isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url: '<?php echo base_url();?>Talent/removeMedia',
            type: "POST",
            data: {
                Mediaid: Mediaid,TalentId: TalentId
            },
            dataType: 'JSON',
            success: function () {
                Swal.fire("Done!", "It was succesfully deleted!", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                Swal.fire("Error deleting!", "Please try again", "error");
            }
        });
    });
}
</script>