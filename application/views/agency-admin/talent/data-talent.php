<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Talent</h1>
            <br/>
            
            <a href="<?php echo base_url(); ?>Talent/create" class="btn btn-app bg-gradient-primary" style="color:#fff;">
                  <i class="fas fa-plus"></i> Add Talent
            </a>
          </div> 
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('Dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active">Data Talent</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->

    </section>
    <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
    <?php } ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Talent</h3>
            </div>
            <!-- /.card-header -->
            
            <div class="card-body">
              <table id="example3" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Slug</th>
                  <th>Gender</th>
                  <th>Category</th>
                  <th>Ethnicity</th>
                  <th>Complexion</th>
                  <th>Tag</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    foreach ($talent as $t) { 
                        $talent_id=$t->talent_id; $datestring = '%d %M %Y - %h:%i:%s'; 
                ?>
                <tr>
                  <td><?= $t->talent_fname . ' '. $t->talent_lname ?></td>
                  <td><?= $t->talent_slug ?></td>
                  <td><?= $t->gender_name ?></td>
                  <td><?= $t->categoryname ?></td>
                  <td><?= $t->ethnicity_name ?></td>
                  <td><?= $t->compelxionname ?></td>
                  <td><?= $t->talent_tag ?></td>
                  <td><?php if($t->talent_status == 1)  echo 'Active'; else echo 'Inactive'; ?></td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default<?php echo $talent_id; ?>" data-placement="top" data-original-title="Detail" ><i class="fa fa-search"></i></button>
                      <button type="button" class="btn btn-warning" data-toggle="tooltip" onclick="myFunction('<?php echo base_url('Talent/update/').$talent_id; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-edit"></i></button>
                      <button type="button" class="btn btn-info" data-toggle="tooltip" onclick="Newtab('<?php echo base_url('catalog/talent/').$t->talent_slug; ?>')"  data-placement="top" data-original-title="Update" ><i class="fa fa-eye"></i></button>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete<?php echo $talent_id; ?>"  data-placement="top" data-original-title="Activated" ><i class="fa fa-trash"></i></button>
                    </div>
                  </td>
                    <div class="modal fade" id="modal-delete<?php echo $talent_id; ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete Talent</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                                                
                            </div>
                            
                            <div class="modal-body">
                            <form class="form-horizontal" method="post" action="<?php echo base_url().'Talent/delete/'.$talent_id;?>">
                              <div class="col-md-12">
                                <p>Are you sure to delete this Talent ?</p> 
                              </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default " data-dismiss="modal">No</button>
                                <button type="submit" class="btn btn-primary pull-left">Yes</button>
                            </form>
                            </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                  <div class="modal fade" id="modal-default<?php echo $talent_id; ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Talent Detail</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                                                
                            </div>
                            <div class="modal-body">
                            <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Talent Name</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $t->talent_fname . ' '. $t->talent_lname; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Gender</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $t->gender_name; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Category</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $t->categoryname; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Ethnicity</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $t->ethnicity_name; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Compelxion</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $t->compelxionname; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Tag</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $t->talent_tag; ?></p>
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-4">
                                        <p>Status</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php if($t->talent_status == 1)  echo 'Active'; else echo 'Inactive'; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                
                            </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Slug</th>
                  <th>Gender</th>
                  <th>Category</th>
                  <th>Ethnicity</th>
                  <th>Complexion</th>
                  <th>Tag</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
function Newtab(name) {
  //alert("Welcome " + name + ".");
  //window.location.replace(name);
  window.open(name, '_blank');
}
</script>