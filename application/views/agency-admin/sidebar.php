<?php
  $user=$this->session->userdata('user');
  $name=$this->session->userdata('name');
  $position=$this->session->userdata('position');
  
?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-2 sidebar-light-olive">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('Dashboard'); ?>" class="brand-link">
      <img src="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-app.png" alt="BMA Logo" class="brand-image" style="float: none;max-height: 55px;" >
      
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>src/admin_assets/dist/img/user-animated.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $name ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item ">
            <a href="<?php echo base_url('Dashboard'); ?>" class="nav-link ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                
              </p>
            </a>
            
          </li>
          <li class="nav-header">DATA</li>
          <li class="nav-item">
            <a href="<?php echo base_url('AdminUser'); ?>" class="nav-link">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                Admin User
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Gender'); ?>" class="nav-link">
              <i class="nav-icon fas fa-transgender-alt"></i>
              <p>
                Gender
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Category'); ?>" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Category
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Tags'); ?>" class="nav-link">
              <i class="nav-icon fas fa-tags"></i>
              <p>
                Tags
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Ethnicity'); ?>" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Ethnicity
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Compelxion'); ?>" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
              Complexion
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('Talent'); ?>" class="nav-link">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                Talent
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>Client" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>Client</p>
            </a>
          </li>
          <li class="nav-header">ACTIVITY</li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>Packages" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Packages
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>Showcase" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Showcase
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>Project" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Projects
              </p>
            </a>
          </li>

          <li class="nav-header">SYSTEM</li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>Log" class="nav-link">
              <i class="nav-icon fas fa-history"></i>
              <p>
                Activity History
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>Setting" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Setting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>AdminLogin/logout" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>