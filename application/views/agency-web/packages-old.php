<style>
#feature1 {
    margin: 0;
    width: 100%;
    min-height: 500px;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    position: relative;
    text-align: center;
    z-index: 0;
}
#feature1::before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    background-position: 50% 60%;
    background-repeat: no-repeat;
    background-size: cover;
    filter: brightness(1);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index:0;
}
#feature1:hover::before{
    
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    filter: brightness(0.7);
}
#feature2 {
    margin: 0;
    width: 100%;
    min-height: 500px;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    position: relative;
    text-align: center;
    z-index: 0;
}
#feature2::before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/denisa-c-headshot.jpg');
    background-position: 50% 60%;
    background-repeat: no-repeat;
    background-size: cover;
    filter: brightness(1);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index:0;
}
#feature2:hover::before{
    
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/denisa-c-headshot.jpg');
    filter: brightness(0.7);
}
#feature3 {
    margin: 0;
    width: 100%;
    min-height: 500px;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    position: relative;
    text-align: center;
    z-index: 0;
}
#feature3::before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    background-position: 50% 60%;
    background-repeat: no-repeat;
    background-size: cover;
    filter: brightness(1);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index:0;
}
#feature3:hover::before{
    
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    filter: brightness(0.7);
}
#feature4 {
    margin: 0;
    width: 100%;
    min-height: 500px;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    position: relative;
    text-align: center;
    z-index: 0;
}
#feature4::before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    background-position: 50% 60%;
    background-repeat: no-repeat;
    background-size: cover;
    filter: brightness(1);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index:0;
}
#feature4:hover::before{
    
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    filter: brightness(0.7);
}
#feature5 {
    margin: 0;
    width: 100%;
    min-height: 500px;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    position: relative;
    text-align: center;
    z-index: 0;
}
#feature5::before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    background-position: 50% 60%;
    background-repeat: no-repeat;
    background-size: cover;
    filter: brightness(1);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index:0;
}
#feature5:hover::before{
    
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    filter: brightness(0.7);
}
#feature6 {
    margin: 0;
    width: 100%;
    min-height: 500px;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    position: relative;
    text-align: center;
    z-index: 0;
}
#feature6::before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    background-position: 50% 60%;
    background-repeat: no-repeat;
    background-size: cover;
    filter: brightness(1);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index:0;
}
#feature6:hover::before{
    
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    filter: brightness(0.7);
}
#feature7 {
    margin: 0;
    width: 100%;
    min-height: 500px;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    position: relative;
    text-align: center;
    z-index: 0;
}
#feature7::before{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    background-position: 50% 60%;
    background-repeat: no-repeat;
    background-size: cover;
    filter: brightness(1);
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    z-index:0;
}
#feature7:hover::before{
    
    background-image: url('http://localhost/bma-agency/src/admin_assets/dist/img/daria-b-headshot.jpg');
    filter: brightness(0.7);
}
.featured-box .card-body {
    padding: 1.25rem 60px;
}
.featured-box h3 {
    position: relative;
    z-index: 1;
    text-align: center;
    color: #fff;
    float: none;
    font-size: 32px;
    font-weight: 700;
    font-family: "Montserrat", Arial, Tahoma, sans-serif;
    padding: 200px 0 20px 0;
    visibility: hidden;
    opacity: 0;
    -webkit-transition: visibility 0s, opacity 0.2s linear;
    -moz-transition: visibility 0s, opacity 0.2s linear;
    -o-transition: visibility 0s, opacity 0.2s linear;
    transition: visibility 0s, opacity 0.2s linear;
}  
.featured-box p {
    position: relative;
    text-align: center;
    z-index: 1;
    color: #fff;
    float: none;
    font-size: 16px;
    visibility: hidden;
    opacity: 0;
    -webkit-transition: visibility 0s, opacity 0.2s linear;
    -moz-transition: visibility 0s, opacity 0.2s linear;
    -o-transition: visibility 0s, opacity 0.2s linear;
    transition: visibility 0s, opacity 0.2s linear;
}
.featured-box:hover h3{
    visibility: visible;
    opacity: 1;
}
.featured-box:hover p {
    visibility: visible;
    opacity: 1;
}
.thin-box{
    padding: 0 4px;
}
.content-website{
    background:#fff;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper content-website">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

<!-- Main content -->
<div class="content">
      <div class="">
      
        <div class="row">
            <div class="col-lg-12 text-center" style=" margin-bottom:50px;">
                <h1 class="m-0 "> French TV Show</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1 thin-box">

            </div>
            <div class="col-lg-2 text-center thin-box">
                <a class="button-card" href="https://baliprod.com/location/explore/detail/sekumpul-waterfall">        
            
                    <div id="feature1" class="featured-box">
                        <div class="card-body">
                            <h3 class="card-title">Daria B</h3>

                            <p class="card-text">Female</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 thin-box">
                <a class="button-card" href="https://baliprod.com/location/explore/detail/sekumpul-waterfall">        
                    
                    <div id="feature2" class="featured-box">
                        <div class="card-body">
                            <h3 class="card-title">Denisa C</h3>

                            <p class="card-text">Female</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 thin-box">
                <a class="button-card" href="https://baliprod.com/location/explore/detail/sekumpul-waterfall">        
                    
                    <div id="feature3" class="featured-box">
                        <div class="card-body">
                            <h3 class="card-title">Daria B</h3>

                            <p class="card-text">Female</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 thin-box">
                <a class="button-card" href="https://baliprod.com/location/explore/detail/sekumpul-waterfall">        
                    
                    <div id="feature4" class="featured-box">
                        <div class="card-body">
                            <h3 class="card-title">Daria B</h3>

                            <p class="card-text">Female</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 thin-box">
                <a class="button-card" href="https://baliprod.com/location/explore/detail/sekumpul-waterfall">        
                    
                    <div id="feature5" class="featured-box">
                        <div class="card-body">
                            <h3 class="card-title">Daria B</h3>

                            <p class="card-text">Female</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-1 thin-box">

            </div>
            <div class="col-lg-1 thin-box">

            </div>
            <div class="col-lg-2 thin-box">
                <a class="button-card" href="https://baliprod.com/location/explore/detail/sekumpul-waterfall">        
                    
                    <div id="feature6" class="featured-box">
                        <div class="card-body">
                            <h3 class="card-title">Daria B</h3>

                            <p class="card-text">Female</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 thin-box">
                <a class="button-card" href="https://baliprod.com/location/explore/detail/sekumpul-waterfall">        
                    
                    <div id="feature7" class="featured-box">
                        <div class="card-body">
                            <h3 class="card-title">Daria B</h3>

                            <p class="card-text">Female</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->