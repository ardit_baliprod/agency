<style>
.agency-catalog-wrapper {
      width: 90%;
      margin: 0 auto;
      display: grid;
      grid-gap: 10px;
      grid-template-columns: repeat(5, 20%);
}
.agency-catalog-box {
	  min-height: 500px;
	  height: 500px;
	  position: relative;
      overflow: hidden;
	  font-size: 150%;  	  
	    -webkit-transition: all 0.2s ease-in-out;
        -moz-transition: all 0.2s ease-in-out;
        -o-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
}
.agency-catalog-box:hover {
  cursor: pointer;
}
 .agency-catalog-novideo::before {
  content: "";
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: inherit;
  transition: inherit;
  -webkit-transition: all 0.2s ease-in-out;
  -moz-transition: all 0.2s ease-in-out;
  -o-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.agency-catalog-novideo:hover::before {
  transform: scale(1.1);
}
.agency-catalog-box .agency-box-overlay {
  padding: 220px 20px 0px;
  box-sizing: border-box;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: 7;
  overflow: hidden;
  display: block;
  -webkit-transition: all 0.2s ease-in-out;
  -moz-transition: all 0.2s ease-in-out;
  -o-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.agency-catalog-box .agency-box-overlay .agency-talent-title {
  position: relative;
  z-index: 1;
  text-align: center;
  color: #fff;
  float: none;
  font-size: 32px;
  letter-spacing: 2px;
  font-weight: 300;
  font-family: "Nunito Sans"; 
  visibility: hidden;
  opacity: 0;
  -webkit-transition: visibility 0s, opacity 0.2s linear;
  -moz-transition: visibility 0s, opacity 0.2s linear;
  -o-transition: visibility 0s, opacity 0.2s linear;
  transition: visibility 0s, opacity 0.2s linear;
}
.agency-catalog-box:hover .agency-box-overlay {
  background-color: rgba(0, 0, 0, 0.2);
}
.agency-catalog-box:hover .agency-box-overlay .agency-talent-title {
  visibility: visible;
  opacity: 1;
}

.featured-box .card-body {
    padding: 1.25rem 60px;
}
.featured-box h3 {
    position: relative;
    z-index: 1;
    text-align: center;
    color: #fff;
    float: none;
    font-size: 32px;
    font-weight: 700;
    font-family: "Nunito Sans", Arial, Tahoma, sans-serif;
    padding: 200px 0 20px 0;
    visibility: hidden;
    opacity: 0;
    -webkit-transition: visibility 0s, opacity 0.2s linear;
    -moz-transition: visibility 0s, opacity 0.2s linear;
    -o-transition: visibility 0s, opacity 0.2s linear;
    transition: visibility 0s, opacity 0.2s linear;
}  
.featured-box p {
    position: relative;
    text-align: center;
    z-index: 1;
    color: #fff;
    float: none;
    font-size: 16px;
    visibility: hidden;
    opacity: 0;
    -webkit-transition: visibility 0s, opacity 0.2s linear;
    -moz-transition: visibility 0s, opacity 0.2s linear;
    -o-transition: visibility 0s, opacity 0.2s linear;
    transition: visibility 0s, opacity 0.2s linear;
}
.featured-box:hover h3{
    visibility: visible;
    opacity: 1;
}
.featured-box:hover p {
    visibility: visible;
    opacity: 1;
}
.thin-box{
    padding: 0 4px;
}
.content-website{
    background:#fff;
}
h1{
    font-family: 'Nunito Sans', serif;
    font-weight: 400;
}
.agency-catalog-box video {
  top: 50%;
  left: 50%;
  max-width: 100%;
  max-height: 500px;
  width: 100%;
  height: 500px;
  z-index: -1000;
  overflow: hidden;
  object-fit: cover;
  -webkit-transition: all 400ms ease-out;
  -moz-transition: all 400ms ease-out;
  -o-transition: all 400ms ease-out;
  -ms-transition: all 400ms ease-out;
  transition: all 400ms ease-out;
  opacity: 0;
}
.agency-catalog-box:hover video {
  opacity: 1;
}
.agency-catalog-box .agency-box-overlay .agency-talent-category {
  position: relative;
  text-align: center;
  z-index: 1;
  color: #fff;
  float: none;
  font-size: 16px;
  letter-spacing: 2px;
  text-transform: uppercase;
  font-weight: 500;
  visibility: hidden;
  font-family: "Nunito Sans";
  opacity: 0;
  -webkit-transition: visibility 0s, opacity 0.2s linear;
  -moz-transition: visibility 0s, opacity 0.2s linear;
  -o-transition: visibility 0s, opacity 0.2s linear;
  transition: visibility 0s, opacity 0.2s linear;
}
.agency-catalog-box:hover .agency-box-overlay .agency-talent-category {
  visibility: visible;
  opacity: 1;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper content-website">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

<!-- Main content -->
<div class="content">
      <div class="">
      
        <div class="row">
            <div class="col-lg-12 text-center" style=" margin-bottom:50px;">
                <h1 class="m-0 "><?= stripslashes($packages['package_name']) ?></h1>
            </div>
        </div>
        <div class="row">
        <div class="agency-catalog-wrapper">
            <?php
                $nomor=0;
                foreach ($PackagesTalent as $p) {
                    $media_url = $p->media_url;
                    $talent_id = $p->talent_id;
                    $nickname = $p->talent_nickname;
                    $full_url = base_url(). $media_url;
                    $link = base_url() . 'catalog/talent/'. $p->talent_slug;
                    if($p->urlvideo){
                        $video_url = $p->urlvideo;
                        $full_video_url = base_url(). $video_url;
            ?>
            <div class="agency-catalog-box agency-catalog-hasvideo" style="background-image: url('<?= $full_url ?>');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                <video muted loop playsinline class="video">
                      <source src="<?= $full_video_url ?>" type="<?= $p->mimevideo ?>" />
                      Your browser does not support video
                </video>
                <div class="agency-box-overlay">
                    <a href="<?php echo $link ?>" >
                        <h3 class="agency-talent-title"><?= $nickname ?></h3>
                        <p class="agency-talent-category"><?= $p->categoryname ?></p>
                    </a>
                </div>
            </div>
            <?php }else{ ?>
            <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?= $full_url ?>');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                <div class="agency-box-overlay">
                    <a href="<?php echo $link ?>" >
                        <h3 class="agency-talent-title"><?= $nickname ?></h3>
                        <p class="agency-talent-category"><?= $p->categoryname ?></p>
                    </a>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
            </div>
            
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->