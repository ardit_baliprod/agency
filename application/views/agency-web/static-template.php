<?php
  $page=$this->session->userdata('page');
  $titlePage = "Balimodelagnecy Apps | ".$page;
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?= $titlePage ?></title>
  <meta name="description" content="Balimodelagency apps">
  <meta name="keywords" content="model, agency, bali">
  <meta name="author" content="BMA">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
  <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
  <link rel="shortcut icon" href="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />
  <link rel="apple-touch-icon" href="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-fav.jpg" />

  <!-- jQuery -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
  

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/dist/css/adminlte.min.css">
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->
  <!-- VideoJS -->
  <link rel="stylesheet" href="https://vjs.zencdn.net/7.2.3/video-js.css">
  <!-- Magnific Popup -->
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" />

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>src/admin_assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Frank+Ruhl+Libre:wght@300;400;500;700;900&display=swap" rel="stylesheet">
  <style>
    body{
      font-family: 'Nunito Sans', sans-serif;
    }
    .content-wrapper{
      background : #fff;
    }
    .navbar {
      padding: 30px .5rem;
    }
    .navbar-brand{
      position: absolute;
      left: 40%;
      display: block;
      text-align: center;
    }
    .logo-agency{
      width: 300px;
    }
    .navbar-light .navbar-nav .nav-link i{
      font-size:24px;
    }
    #footer {
	background: #fff !important;

}
#footer .footer-above {
	padding: 60px 0;
}
#footer .footer-above .logo-footer{
  width:90%;
}
#footer h5 {
	font-size: 18px;
	line-height: 25px;
	font-weight: 300;
	letter-spacing: 1px;
	color: #000000;
}
#footer .textwidget {
	color: #000000;
	font-size: 20px;
	font-weight: 300;
}
#footer .textwidget strong {
	font-weight: 700;
}
#footer a {
	color: #000000;
	text-decoration: none;
	background-color: transparent;
	-webkit-text-decoration-skip: objects;
}
#footer a:hover {
	color: #000000;
	text-decoration:  underline !important;
	background-color: transparent;
	-webkit-text-decoration-skip: objects;
}

#footer ul.social li {
	padding: 3px 0;
}
#footer ul.social li a i {
	margin-right: 5px;
	font-size: 16px;
}

#footer ul.social li a,
#footer ul.quick-links li a {
	color: #000000;
}
#footer ul.social li a:hover {
	color: #eeeeee;
}
#footer ul.quick-links {
	list-style: none;
	padding-left: 30px;
	font-size: 18px;
}
#footer .title-col{
  padding-left: 30px;
}
#footer ul.quick-links li {
	padding: 3px 0;
}
#footer ul.quick-links li:hover {
	padding: 3px 0;
	text-decoration: none;
}
#footer .form-group input[type="text"],
#footer .form-group input[type="email"],
#footer .form-group textarea {
	border-radius: 0;
	background-clip: unset;
	color: #626262;
	background-color: rgba(255, 255, 255, 1);
	border-color: #ebebeb;
	box-shadow: inset 0 0 2px 2px rgba(0, 0, 0, 0.02);
	padding: 10px;
	outline: 0;
	margin: 0;
	max-width: 100%;
	display: block;
	font-size: 13px;
	border-width: 1px;
	border-style: solid;
	border-radius: 0;

	-webkit-appearance: none;
}
#footer .form-group label {
	color: #fff;
	margin-bottom: 5px;
	font-weight: 700;
}
#footer .checkbox-inline {
	margin-bottom: 20px;
	position: relative;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
}
#footer .checkbox-inline input[type="checkbox"] {
	margin: 0 0 0 -20px;
}

#footer .checkbox-inline.show:before {
	content: "\e013";
	color: #1fa67b;
	font-size: 17px;
	margin: 1px 0 0 3px;
	position: absolute;
	pointer-events: none;
}
#footer .checkbox-inline .character-checkbox {
	width: 25px;
	height: 25px;
	cursor: pointer;
	border-radius: 3px;
	border: 1px solid #ccc;
	vertical-align: middle;
	display: inline-block;
}
#footer .checkbox-inline .label {
	color: #000000;
	font-size: 100%;
}
#footer input[type="submit"].btn-block {
	font-size: 20px;
	background-color: #f7f7f7;
	color: #000000;
	border-radius: 5px;
	font-weight: bold;
	letter-spacing: 3px;
	display: inline-block;
	padding: 11px 20px;
	width: 100%;
	cursor: pointer;
	margin-right: 7px;
	border: 0;
	border-radius: 5px;
	position: relative;
	overflow: hidden;
	-webkit-box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.03);
	box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.03);
	background-image: url(../images/box_shadow_button.png);
	background-repeat: repeat-x;
}
#footer .footer-bottom {
	border-top: 1px solid #c7ceda;
	text-align: center;
	font-size: 16px;
}
#footer .footer-bottom .container .row .one {
	margin-bottom: 20px;
	padding-top: 30px;
	min-height: 33px;
	margin: 0 0 10px;
}
#footer .footer-bottom .social {
	float: none;
	margin: 0;
	list-style: none outside;
}
#footer .footer-bottom .social li {
	display: inline-block;
	margin-right: 6px;
}
#footer .footer-bottom .social li a {
	color: #65666c;
	font-size: 15px;
	line-height: 15px;
	-webkit-transition: all 0.3s ease-in-out;
	-moz-transition: all 0.3s ease-in-out;
	-o-transition: all 0.3s ease-in-out;
	-ms-transition: all 0.3s ease-in-out;
	transition: all 0.3s ease-in-out;
}
#footer .footer-bottom .paragraf-footer {
	font-size: 20px;
	font-weight: 300;
}
#footer .footer-above .col-footer {
	margin-top: 30px;
	padding: 15px 0;
}
.btn-remove-talent{
  font-size: 10px;
}
.vertical-alignment-helper {
    display:table;
    height: 100%;
    width: 100%;
    pointer-events:none;
}
.vertical-align-center {
    /* To center vertically */
    display: table-cell;
    vertical-align: middle;
    pointer-events:none;
}
.modal-content {
    /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
    width:inherit;
 max-width:inherit; /* For Bootstrap 4 - to avoid the modal window stretching full width */
    height:inherit;
    /* To center horizontally */
    margin: 0 auto;
    pointer-events:all;
}
.g-recaptcha {
    margin: auto;
}
  </style>
  <?php
    echo $script_captcha; // javascript recaptcha
  ?>
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          
        </ul>

        <!-- SEARCH FORM -->
        
      </div>
      <a href="<?php echo base_url('catalog/mockup'); ?>" class="navbar-brand" >
        <!--
        <img src="<?php echo base_url(); ?>src/admin_assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span> -->
        <img src="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-app.png" alt="BMA Agency Apps Logo" class="logo-agency" >
      </a>
      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown"  href="#">
            <i class="fa fa-user"></i>
            
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="<?php echo base_url('Member'); ?>" class="dropdown-item">
              Dashboard
            </a>
            <a data-toggle="modal" href="#modal-login" class="dropdown-item">
              Sign In
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              Sign Out
            </a>
            
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fa fa-heart"></i>
            <span class="badge badge-danger navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header">Talents</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              Lente
              <span class="float-right "><button type="button" class="btn btn-danger btn-remove-talent" data-placement="top" data-original-title="Activated"><i class="fa fa-trash"></i></button></span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              Hanna K
              <span class="float-right "><button type="button" class="btn btn-danger btn-remove-talent" data-placement="top" data-original-title="Activated"><i class="fa fa-trash"></i></button></span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              Andreas B
              <span class="float-right "><button type="button" class="btn btn-danger btn-remove-talent" data-placement="top" data-original-title="Activated"><i class="fa fa-trash"></i></button></span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Talents</a>
          </div>
        </li>
        
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

  
<?php
  echo $contents;
?>
    


  <!-- Main Footer -->
  <footer id="footer">
    <div class="footer-above">
        <div class="w-75 mx-auto">
                <div class="row text-xs-left text-sm-left text-md-left ">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-footer" >
                        <img src="<?php echo base_url(); ?>src/admin_assets/dist/img/BMA-Logo-header.png" class="logo-footer" />
                        <br>
                        <div class="textwidget mt-3">
                            <p>BMA is the sister company of Baliprod, a photo & video production house based in the trendy Pererenan, Canggu.<br>
                            Not only can Bali Model Agency provides models and actors but also photographers, stylists, hair & make up artists and any talent or production staff for any kind of photo & video shoot.</p>
                            
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-footer">
                        
                        <h5 class="title-col">CORPORATE</h5>
                        <br>
                        <ul class="quick-links">
                            <li><a href="https://www.balimodelagency.com/club/">BMA Club</a></li>
                            <li><a href="https://www.balimodelagency.com/blog/">Blog</a></li>
                            <li><a href="https://www.balimodelagency.com/become-a-model/">Be a model</a></li>
                            <li><a href="https://www.balimodelagency.com/about-us/">About us</a></li>
                            <li><a href="https://www.balimodelagency.com/privacy-policy/">Privacy policy</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-footer">
                        <h5 class="title-col">PARENT COMPANY</h5>
                        <br>
                        <ul class="quick-links">
                            <li><a href="http://www.kosongsatu.com/">Kosong Satu Group</a></li>
                            
                        </ul>
                        <br>
                        <h5 class="title-col">SISTER BRANDS</h5>
                        <br>
                        <ul class="quick-links">
                            <li><a href="https://www.balimodelagency.com/">Baliprod</a></li>
                            <li><a href="http://www.balishoot.com/">Balishoot</a></li>
                            <li><a href="http://www.balifilmgear.com/">Bali Film Gear Rental</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-footer">
                        <h5>OUR OFFICE</h5>
                        <br> 
                        <p>Jalan Pantai Pererenan<br>
                        98B Pererenan, Mengwi<br>
                        Bali 80351, Indonesia (<a style="text-decoration: underline;" class="linkdesc" title="" href="https://goo.gl/maps/1kordExhA892" target="_blank" rel="nofollow noreferrer noopener">map</a>)</>
                            <p><strong>Phone (UTC+8):</strong><br>
                            <a class="linkdesc" href="tel:+6281338384715">+62(0)81338384715</a></p>
                    </div>
                </div>
                
        </div>
    </div>  
   
    <div class="footer-bottom">
        <div class="w-75 m-auto">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-5 text-center one">
                    <p class="paragraf-footer">Bali Model Agency is part of <a style="text-decoration: underline;" href="http://www.kosongsatu.com" target="_blank">Kosong Satu Group</a>, a Bali based business development group.<br>Copyright © 2013 - 2020. All rights reserved</p>
                   
                </div>
                
                
            </div>
        </div>
        	
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<div class="modal " id="modal-login">
  <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-lg vertical-align-center" >
              <div class="modal-content" style="vertical-align: middle;">
                <div class="modal-header">
                  <h4 class="modal-title">Sign In</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="<?php echo base_url('account/do_login'); ?>" method="post">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" name="username" placeholder="Email" style="font-size: 14px;height: 34px;">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control pwd" id="passwordfield" name="password" placeholder="Password" style="font-size: 14px;height: 34px;" >  
                            
                        </div>
                    </div> 
                    <div class="input-group form-group">
                            <?php echo $captcha // tampilkan recaptcha ?>
                    </div>
                    
                    
                    <br /> <center><div style="border:1px solid black;height:1px;width:300px;"></div></center><br />
                   
                    <p class="mb-0 text-center">
                        Don't have an Account! <a href="<?php echo base_url('Member/register'); ?>" class="text-center">Sign up here</a>
                    </p>
                    <center><button class="btn btn-primary btn-login mt-5 mb-3 " type="submit">Sign in</button></center>
                    
                    
                  </form>
                </div>
                <div class="modal-footer justify-content-between" >
                  
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
            </div>
            <!-- /.vertical -->
          </div>
          <!-- /.modal -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>src/admin_assets/dist/js/adminlte.min.js"></script> 
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script> -->
<!-- VideoJS -->
<script src="http://vjs.zencdn.net/6.6.3/video.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>src/admin_assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

<script src="https://polyfill.io/v3/polyfill.min.js?features=Promise"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/nanogram.js@2.0.0/dist/nanogram.iife.min.js"></script>

<script>

$(document).ready(function() {
  $('.popup-btn').magnificPopup({
           type: 'image',
           gallery: {
              enabled: true
           }
  }); 
  $('.popup-polaroid').magnificPopup({
           type: 'image',
           gallery: {
              enabled: true
           }
  });
  $('.agency-box-button a').click(function(){
      $(this).addClass('selected-talent');
  });
  if ($(".agency-catalog-hasvideo")[0]) {
    // Do something if class exists
    $(".agency-catalog-hasvideo").hover(
      function () {
        var thePromise = $(this).find("video")[0].play();

        if (thePromise != undefined) {
          thePromise.then(function (_) {
            //var el = $(this).find("video")[0];
            //el.pause();
            //el.currentTime = 0;
            //$(this).find("video")[0].play();
          });
      },
      function () {
        var el = $(this).find("video")[0];
        el.pause();
        //el.currentTime = 0;
      }
    );
  } else {
    // Do something if class does not exist
  }

  $('.dropdown').hover(function() {
            $(this).addClass('show');
            var dropdownMenu = $(this).children(".dropdown-menu");
            dropdownMenu.addClass('show');
      },
      function() {
            $(this).removeClass('show');
            var dropdownMenu = $(this).children(".dropdown-menu");
            dropdownMenu.removeClass('show');
            //$('.dropdown-menu').removeClass('show');
  });

  $('#example2').DataTable({
        "paging": false,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        });
        //oTable = $('#example2').DataTable();
        $('#myInputTextField').keyup(function(){
            filterGlobal () ;
            //alert('test');
  });
  function filterGlobal () {
            $('#example2').DataTable().search(
                $('#myInputTextField').val()
            ).draw();
        }
  $(".agency-catalog-box").click(function (e) {
    //window.location = $(this).find('.overlay').find('a').attr('href');
    //return false;
    //alert(jQuery(window).width());
    e.preventDefault();
    if (jQuery(window).width() < 768) {
      $(this).dblclick(function (e) {
        //window.location = this.href;
        window.location = $(this)
          .find(".agency-box-overlay")
          .find("a")
          .attr("href");
        return false;
        //alert('yes');
      });
    } else {
      //window.location = this.href;
      window.location = $(this)
        .find(".agency-box-overlay")
        .find("a")
        .attr("href");
      return false;
    }
  });      

  // Initialize library
  var lib = new Nanogram();

  function buildPorfolio() {
    var preloader = document.getElementById("preloader");
    preloader.classList.add("preloader--loading");
    var myEle = document.getElementById("agency-instagram-user");
    myEleValue = "";
    if (myEle) {
      myEleValue = myEle.value;
    }
    // Get content from https://www.instagram.com/instagram/
    return lib
      .getMediaByUsername(myEleValue)
      .then(function (response) {
        if (console.table) {
          console.table(response.profile);
        }

        // Get photos
        var photos = response.profile.edge_owner_to_timeline_media.edges;
        var items = [];

        // Create new elements
        // <div class="portfolio__item">
        //   <a href="..." target="_blank" class="portfolio__link">
        //     <img src="..." alt="..." width="..." height="..." class="portfolio__img">
        //   </a>
        // </div>

        for (var i = 0; i <= photos.length - 1; i++) {
          var current = photos[i].node;

          var div = document.createElement("div");
          var link = document.createElement("a");
          var img = document.createElement("img");

          var thumbnail = current.thumbnail_resources[4];
          var imgSrc = thumbnail.src;
          var imgWidth = thumbnail.config_width;
          var imgHeight = thumbnail.config_height;
          var imgAlt = current.accessibility_caption;

          var shortcode = current.shortcode;
          var linkHref = "https://www.instagram.com/p/" + shortcode;

          div.classList.add("portfolio__item");

          img.classList.add("portfolio__img");
          img.src = imgSrc;
          img.width = imgWidth;
          img.height = imgHeight;
          img.alt = imgAlt;

          link.classList.add("portfolio__link");
          link.href = "javascript: void(0)";

          link.appendChild(img);
          div.appendChild(link);

          items.push(div);
        }

        // Create container for our portfolio
        var container = document.createElement("div");
        container.id = "portfolio";
        container.classList.add("portfolio");

        // Append all photos to our container
        for (var j = 0; j <= items.length - 1; j++) {
          container.appendChild(items[j]);
        }

        // Append our container to body
        //document.body.appendChild(container);
        $("#agency-instagram").append(container);

        preloader.classList.remove("preloader--loading");
      })
      .catch(function (error) {
        console.log(error);
        preloader.classList.remove("preloader--loading");
      });
  }

  var preEle = document.getElementById("preloader");
  if (preEle) {
    buildPorfolio();
  }
  
});

</script>
</body>
</html>
