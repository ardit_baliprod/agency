<?php
    $datestring = '%d / %m / %Y';
 ?>
<style>
.subtitle-dashboard {
    font-size: 16px;
}
.checkout-card .card-header .checkout-title {
    font-weight: 700;
    font-size: 20px;
}
.checkout-card .card-header .checkout-title > span {
    font-weight: normal;
    font-size: 14px;
    margin-left: 30px;
}
.btn-login {
    color: #fff;
    background-color: #000;
    border-color: #000;
    box-shadow: none;
    border-radius: 0;
    transition: all 0.3s linear;
    text-transform: uppercase;
    padding: 10px;
    font-size: 12px;
}
.btn-cancel {
    color: #000000;
    background-color: #f8f9fa;
    border-color: #f8f9fa;
    box-shadow: none;
    border-radius: 0;
    transition: all 0.3s linear;
    text-transform: uppercase;
    padding: 10px;
    font-size: 12px;
}
</style>
<div class="content-wrapper min-vh-100 register-wrapper" style="padding-top: 60px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      
        <div class="row mb-2">
          <div class="col-sm-6">
           
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="padding-bottom:100px;">
    <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
        <?php } ?>
        <div class="row mb-4 mx-auto w-75" >
          <div class="col-lg-2">
             
          </div><!-- /.col -->
          <div class="col-lg-8">
            <h2 class="m-0 text-dark float-left subtitle-dashboard px-2">Sign Up</h2>
            <h2 class="m-0 text-dark float-right subtitle-dashboard px-2">Date : <?= mdate($datestring, strtotime("now")) ?></h2>
          </div><!-- /.col -->
          <div class="col-lg-2">
             
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mx-auto w-75" >
          <div class="col-lg-2 ">
            
          </div>
          
          <div class="col-lg-8 ">
            <div class="dashboard-right px-2">
                <div class="card checkout-card">
                    <div class="card-header">
                        
                        <div class="checkout-title">New Member <span>Please fill form bellow</span></div>
                        <p class="text-danger" id="error_message_checkout"></p>
                        <?php if($this->session->flashdata('error')) { ?>
                          <div class="text-danger">
                            
                            <p><strong>ERROR !</strong> <?php echo $this->session->flashdata('error'); ?></p>
                          </div>
                        <?php } ?>
                        <div class="card-tools">
                        </div>
                    </div>
                   <form class="form-horizontal" action="<?php echo base_url('account/do_register'); ?>" method="post"  style="width:100%;" >
                    <div class="card-body py-3" style="display: block;">
                        
                        <div class="form-group row">
                          <label for="inputfname" class="col-sm-2 col-form-label">First Name</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputfname" name="FirstName" required placeholder="First Name"  >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputlname" class="col-sm-2 col-form-label">Last Name</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputlname" name="LastName" required placeholder="Last Name"  >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputaddress" class="col-sm-2 col-form-label">Address</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputaddress" name="Address" placeholder="Address"  >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputcompany" class="col-sm-2 col-form-label">Company</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputcompany" name="company" placeholder="Company"  >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputCountry" class="col-sm-2 col-form-label">Country</label>
                            <div class="col-sm-10">
                            <select class="form-control select2" name="country" id="selectcountry" style="width: 100%;">
                                <option value="" >Select Country</option>
                            <?php 
                                foreach ($country as $c) { 
                                    $country_id=$c->country_id;
                                    $name=$c->short_name;
                            ?>
                                <option value="<?= $country_id ?>"><?= $name ?></option>
                                <?php } ?>
                                
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputphone" class="col-sm-2 col-form-label">Phone</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputphone" name="phone" placeholder="Phone"  >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputemail" class="col-sm-2 col-form-label">Email</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="inputemail" name="email" required placeholder="Email" >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="inputpass" class="col-sm-2 col-form-label">Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputpass" name="pass" required placeholder="Password" >
                            
                          </div> 
                        </div>
                        <div class="form-group row">
                          <label for="inputrepass" class="col-sm-2 col-form-label">Confirm Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="inputrepass" name="repass" required placeholder="Confirm Password" >
                            
                          </div>
                        </div>
                        <div class="form-group row">
                            <?php echo $captcha // tampilkan recaptcha ?>
                        </div>
                        
                    </div>
                    <div class="card-footer" style="background-color:white;">
                        <button type="button" onclick="myFunction('<?php echo base_url('account'); ?>')" class="btn btn-default float-right mx-2 btn-cancel">Cancel</button>
                        <button type="submit" class="btn btn-login float-right mx-2" >Submit</button>
                    
                    </div>
                    </form> 
                </div>
            </div>  
              
          </div>
          <div class="col-lg-2 ">
            
          </div>
        </div><!-- /.row -->
        
    </div>
    <!-- /.content -->
  </div>

  <script>
    function myFunction(name) {
        //alert("Welcome " + name + ".");
        window.location.replace(name);
    }
    function copyLink() {
        var copyText = document.getElementById("link-package");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
      }
  </script>
