<div class="col-lg-4 ">
            <div class="dashboard-left px-3">
                <div class="card menu-dashboard">
                    <div class="card-header">
                        <h3 class="card-title py-4">Hi, Ardit</h3>

                        <div class="card-tools">
                        </div>
                    </div>
                    <div class="card-body p-0" style="display: block;">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item active">
                                <a href="<?php echo base_url('account'); ?>" class="nav-link">
                                  <img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages.png" width="27px" height="27px" class="mr-4" /> My Packages
                                    
                                </a>
                            </li>
                            <li class="nav-item">
                            <a href="<?php echo base_url('account/details'); ?>" class="nav-link">
                              <img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-user.png" width="27px" height="27px" class="mr-4" /> Account Details
                            </a>
                            </li>
                            <li class="nav-item">
                            <a href="<?php echo base_url('account/reset_password'); ?>" class="nav-link">
                                <img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-account-details.png" width="27px" height="27px" class="mr-4" /> Change Password
                            </a>
                            </li>
                            <li class="nav-item">
                            <a href="<?php echo base_url('account/logout'); ?>" class="nav-link">
                              <img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-sign-out.png" width="27px" height="27px" class="mr-4" /> Sign Out
                                
                            </a>
                            </li>
                            
                         </ul>
                    </div>
                <!-- /.card-body -->
                </div>
            </div>
          </div>