<?php
    $datestring = '%d / %m / %Y';
    //$mmbruser=$this->session->userdata('mmbruser');
    //$mmbrname=$this->session->userdata('mmbrname');
    //$guestid=$this->session->userdata('guestid');
?>
<style>
.subtitle-dashboard {
    font-size: 16px; 
}
.table > tbody > tr > td,
.table > tbody > tr > th,
.table > tfoot > tr > td,
.table > tfoot > tr > th,
.table > thead > tr > td,
.table > thead > tr > th {
	border-top: 0 solid #ddd !important;
}
.btn-np {
	padding: 0.375rem 0px;
}
.table > thead > tr > th {
	border-bottom: 0 solid #ddd !important;
}
.menu-dashboard .card-header .card-title, .content-dashboard .card-header .card-title, .checkout-card .card-header .card-title {
    font-weight: 700;
    font-size: 20px;
}
#example2_filter {
	display: none;
}
#myInputTextField {
    border: 1px solid silver;
    border-bottom-left-radius: 50px;
    border-bottom-right-radius: 50px;
    border-top-left-radius: 50px;
    border-top-right-radius: 50px;
    padding: 0 20px;
}
</style>
<div class="content-wrapper min-vh-100 login-wrapper" style="padding-top: 60px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      
        <div class="row mb-2">
          <div class="col-sm-6">
           
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="padding-bottom:100px;">
    <?php if($this->session->flashdata('sukses')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <strong>Congratulation.</strong> <?php echo $this->session->flashdata('sukses'); ?>.
                </div>
        <?php } ?>
        <div class="row mb-4 mx-auto w-75" >
          <div class="col-lg-4">
             
          </div><!-- /.col -->
          <div class="col-lg-8">
            <h2 class="m-0 text-dark float-left subtitle-dashboard px-2">Home</h2>
            <h2 class="m-0 text-dark float-right subtitle-dashboard px-2">Date : <?= mdate($datestring, strtotime("now")) ?></h2>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mx-auto w-75" >
        <?php 
            require_once ("member-menu.php");

          ?>
          
          
          <div class="col-lg-8 ">
            <div class="dashboard-right px-2">
                <div class="card content-dashboard">
                    <div class="card-header py-5">
                        <h3 class="card-title ">My Packages</h3>

                        <div class="card-tools ">
                        <input type="text" id="myInputTextField" placeholder="Search">
                        </div>
                    </div>
                    <div class="card-body py-3" style="display: block;">
                     <table id="example2" class="table  table-striped dataTable">
                        <thead>
                           <tr>
                            <th>No</th>
                            <th style="width: 70%;">Talent</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td style="padding-top: 15px;font-size: 14px;">1</td>
                        <td style="padding-top: 15px;font-size: 14px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                        <td>
                        <button type="button" class="btn btn-np" data-toggle="tooltip"   data-placement="top" data-original-title="View" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-view.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="tooltip"  data-placement="top" data-original-title="Update" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-edit.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np delete-packages-guest" data-toggle="tooltip"  data-placement="top" data-original-title="Delete"  ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-delete.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="modal"  ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-share.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="tooltip"  data-placement="top" data-original-title="PDF" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-pdf.png" width="30px" height="30px" /></button>
                        </td>
                        
                        </tr>
                        <tr>
                        <td style="padding-top: 15px;font-size: 14px;">2</td>
                        <td style="padding-top: 15px;font-size: 14px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                        <td>
                        <button type="button" class="btn btn-np" data-toggle="tooltip"   data-placement="top" data-original-title="View" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-view.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="tooltip"  data-placement="top" data-original-title="Update" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-edit.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np delete-packages-guest" data-toggle="tooltip"  data-placement="top" data-original-title="Delete"  ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-delete.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="modal"  ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-share.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="tooltip"  data-placement="top" data-original-title="PDF" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-pdf.png" width="30px" height="30px" /></button>  
                        </td>
                        
                        </tr>
                        <tr>
                        <td style="padding-top: 15px;font-size: 14px;">3</td>
                        <td style="padding-top: 15px;font-size: 14px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                        <td>
                        <button type="button" class="btn btn-np" data-toggle="tooltip"   data-placement="top" data-original-title="View" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-view.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="tooltip"  data-placement="top" data-original-title="Update" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-edit.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np delete-packages-guest" data-toggle="tooltip"  data-placement="top" data-original-title="Delete"  ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-delete.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="modal"  ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-share.png" width="30px" height="30px" /></button>
                            <button type="button" class="btn btn-np" data-toggle="tooltip"  data-placement="top" data-original-title="PDF" ><img src="<?php echo base_url(''); ?>src/admin_assets/dist/img/icon-packages-pdf.png" width="30px" height="30px" /></button>
                        </td>
                        
                        </tr>

                        
                        </tbody> 
                        
                      </table>   
                    </div>
                </div>
            </div>  
              
          </div>
        </div><!-- /.row -->
        
    </div>
    <!-- /.content -->
  </div>

 

<script>
function myFunction(name) {
  //alert("Welcome " + name + ".");
  window.location.replace(name);
}
function Newtab(name) {
  //alert("Welcome " + name + ".");
  //window.location.replace(name);
  window.open(name, '_blank');
}
</script>