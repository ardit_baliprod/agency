<style>
.col-profile{
    margin-bottom:50px;
    padding:0 80px;
}
.col-media{
    padding:0 100px;
}
.content-website{
    background:#fff;
}
.card-media{
    box-shadow: none;
}
.card-media .card-header{
    border-bottom: 1px solid rgba(0,0,0,0);
}
.card-media .card-body{
    
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #000000;
    background-color: #ffffff;
    border-bottom: 2px solid rgba(0,0,0,1);
}
.nav-pills .nav-link {
    border-radius: 0;
}
.row-media{
    margin: 50px 0;
}
.center-pills {
    display: flex;
    justify-content: center;
}
.galleryTalentImage {
    background-size: cover;
    background-position: center top;
    background-repeat: no-repeat;
    height: 600px;
    width: 100%;
}
.imgGall{
    height: 400px;
    width: 100%;
    padding: 6px 6px;
}
video {
  width: 100%;
  height: auto;
}

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper content-website">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

<!-- Main content -->
<div class="content">
      <div class="">
      
        <div class="row">
            <div class="container">
                <div class="col-lg-12 text-center col-profile" >
                    <h1 class="mb-4">Sasha P</h1>
                    <p class="measurement mb-5">Height: 175cm / Bust: 85cm / Waist: 60cm / Hips: 89cm / Shoe size: 40cm / Hair: Brown / Eyes: Brown</p>
                    <p class="bio">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec sapien sit amet eros molestie ullamcorper. Vestibulum et est interdum enim laoreet tempor. Morbi volutpat ante vitae vehicula ultrices. Pellentesque tincidunt, risus sed euismod lobortis, justo mi sodales lectus, vitae suscipit libero turpis vel eros. Nullam congue iaculis nibh, a pulvinar lorem accumsan non. Sed eget justo tempus, imperdiet est vel, volutpat ante. Morbi tempor velit eu volutpat sollicitudin. Donec quis eros id diam gravida mollis interdum at eros. Cras egestas, velit egestas faucibus facilisis, leo tortor gravida ante, nec efficitur mauris arcu non arcu. In aliquam at mauris non semper. Mauris euismod purus sed eros laoreet iaculis. Pellentesque quis felis sodales, volutpat tortor non, dapibus nisi. Integer maximus aliquet mollis. Pellentesque ac massa lectus.
                    </p>
                    <p class="bio">
                        Sed molestie egestas convallis. Phasellus et mi sed nisi laoreet posuere. Morbi ipsum sapien, pharetra eget vehicula ut, aliquam ut tortor. Cras fermentum lorem nibh, a luctus magna congue eu. Donec ut orci eu justo pulvinar suscipit vel nec diam. Cras congue mattis neque et sagittis. Donec elementum eget mauris molestie mattis. Vestibulum auctor molestie urna, at venenatis velit imperdiet nec. Donec luctus nisi vel ipsum consequat sagittis. Nam non erat mi. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                    </p>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="w-75 mx-auto">
                <div class="col-lg-12">
                    <div class="card card-media">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills ">
                                <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Photos</a></li>
                                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Videos</a></li>
                                <li class="nav-item"><a class="nav-link" href="#polaroid" data-toggle="tab">Polaroid</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="activity">
                                    <div class="row">
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-22.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-23.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-46.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-24.jpg" >
                                        <img class="col-md-6 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-14.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-29.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-19.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-22.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-23.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-46.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-24.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-29.jpg" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-19.jpg" >
                                        <img class="col-md-6 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/sasha-p/sasha-p-balimodelagency-14.jpg" >
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane text-center" id="settings">
                                    <div class="container">
                                    <video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto"  width="800" height="600"  data-setup='{}'>
                                        <source src="<?php echo base_url(); ?>src/admin_assets/dist/img/Amina_Web..mp4" type='video/mp4'>
                                        <source src="<?php echo base_url(); ?>src/admin_assets/dist/img/Amina_Web..mp4" type='video/webm'>
                                        <p class="vjs-no-js">
                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                        </p>
                                    </video>
                                    </div>    
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="polaroid">
                                    <div class="row">
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-001.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-002.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-003.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-004.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-005.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-006.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-007.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-008.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-009.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-010.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-011.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-012.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-013.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-014.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-015.JPG" >
                                        <img class="col-md-3 imgGall" src="<?php echo base_url(); ?>src/admin_assets/dist/img/polaroid/polaroid-016.JPG" >
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.card-body -->    
                    </div>
                    <!-- /.nav-tabs-custom -->        

                </div>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->