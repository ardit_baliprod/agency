<style>
.col-profile{
    margin-bottom:50px;
    padding:0 80px;
}
.col-media{
    padding:0 100px;
}
.content-website{
    background:#fff;
}
.card-media{
    box-shadow: none;
}
.card-media .card-header{
    border-bottom: 1px solid rgba(0,0,0,0);
}
.card-media .card-body{
    
}
.nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #000000;
    background-color: #ffffff;
    border-bottom: 2px solid rgba(0,0,0,1);
    font-family: 'Frank Ruhl Libre', serif;
    text-transform: uppercase;
    letter-spacing: 5px;
}
.nav-pills .nav-link {
    border-radius: 0;
    font-family: 'Frank Ruhl Libre', serif;
    text-transform: uppercase;
    letter-spacing: 5px;
}
.row-media{
    margin: 50px 0;
}
.center-pills {
    display: flex;
    justify-content: center;
}
.galleryTalentImage {
    background-size: cover;
    background-position: center top;
    background-repeat: no-repeat;
    height: 600px;
    width: 100%;
}
.imgGall{
    height: 500px;
    width: 100%;
    object-fit: cover;
}
.linkGall{
    padding: 4px;
}
video {
  width: 100%;
  height: auto;
}
h1{
    font-family: 'Frank Ruhl Libre', serif;
    font-weight: 400;
}
.agency-instagram .portfolio {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  padding: 0 100px;
}
.agency-instagram .portfolio__link {
  display: block;
  width: 100%;
  height: 100%;
}

.agency-instagram .portfolio__img {
  display: block;
  width: inherit;
  height: inherit;
  object-fit: cover;
  height: 450px !important;
}

.agency-instagram .portfolio__item {
  width: 100%;
  -ms-flex: 33%; /* IE10 */
  flex: 33%;
  max-width: 33%;
  padding: 4px;
  margin-bottom: 0px;
}

.preloader {
  display: none;
  position: fixed;
  left: 0;
  top: 0;
  z-index: 1;
  width: 100%;
  height: 100%;
  overflow: visible;
  background-image: url("../img/preloader.gif");
  background-position: center;
  background-repeat: no-repeat;
  background-size: 256px 256px;
}

.preloader--loading {
  display: block;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper content-website">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

<!-- Main content -->
<div class="content">
      <div class="">
      
        <div class="row">
            <div class="container">
                <div class="col-lg-12 text-center col-profile" >
                    <h1 class="mb-4"><?= $talent['talent_fname'] . ' ' . $talent['talent_lname'] ?></h1>
                    <p class="measurement mb-5"> 
                        <?php if(round($talent['talent_height'],0) <> 0) { ?>
                        <strong>Height:</strong> <?= round($talent['talent_height'],0) ?>cm &nbsp; 
                        <?php } if(round($talent['talent_bust'],0) <> 0) { ?>
                        <strong>Bust:</strong> <?= round($talent['talent_bust'],0) ?>cm &nbsp;
                        <?php } if(round($talent['talent_chest'],0) <> 0) { ?>
                        <strong>Chest:</strong> <?= round($talent['talent_chest'],0) ?>cm &nbsp; 
                        <?php } if(round($talent['talent_waist'],0) <> 0) { ?> 
                        <strong>Waist:</strong> <?= round($talent['talent_waist'],0) ?>cm &nbsp; 
                        <?php } if(round($talent['talent_hips'],0) <> 0) { ?>
                        <strong>Hips:</strong> <?= round($talent['talent_hips'],0) ?>cm &nbsp; 
                        <?php } if($talent['talent_shoe'] <> '' && $talent['talent_shoe'] <> '0') { ?>
                        <strong>Shoe size:</strong> <?= round($talent['talent_shoe'],0) ?> &nbsp; 
                        <?php } if($talent['talent_hairColor'] <> '') { ?>
                        <strong>Hair:</strong> <?= $talent['talent_hairColor'] ?> &nbsp; 
                        <?php } if($talent['talent_eyeColor'] <> '') { ?>
                        <strong>Eyes:</strong> <?= $talent['talent_eyeColor'] ?>
                        <?php }  ?>
                    </p>
                    <?php if($talent['talent_bio'] <> '' && substr($talent['talent_bio'],0,1) <> '-'){  ?>
                    <p class="bio">
                       <?= stripcslashes($talent['talent_bio']) ?>
                    </p>
                    <?php } ?>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="w-75 mx-auto">
                <div class="col-lg-12">
                    <div class="card card-media">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills center-pills">
                                <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Photos</a></li>
                                <?php
                                    if($tot_video > 0) {
                                ?>
                                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Videos</a></li>
                                <?php 
                                    }
                                    if($tot_polaroid > 0) {
                                ?>
                                <li class="nav-item"><a class="nav-link" href="#polaroid" data-toggle="tab">Polaroid</a></li>
                                <?php 
                                    } 
                                    $instagram = $talent['talent_instagram'];
                                    if($instagram <> ''){
                                        echo '<input type="hidden" id="agency-instagram-user" class="agency-instagram-user" value="'.$instagram.'" >';
                                ?>
                                <li class="nav-item"><a class="nav-link" href="#instagram" data-toggle="tab">Instagram</a></li>    
                                    <?php } ?>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="activity">
                                    <div class="row">
                                    <?php 
                                        $nomor=0;
                                        foreach ($galleryImage as $g) { 
                                            $nomor++;
                                            $full_url = base_url(). $g->media_url;
                                            list($width, $height) = getimagesize($full_url);
                                            if ($width > $height) {
                                                // Landscape
                                    ?>
                                    <a href="<?php echo $full_url; ?>" class="fancylight popup-btn col-md-6 linkGall" data-fancybox-group="light">
                                        <img class="imgGall" src="<?php echo $full_url; ?>" alt="<?= $talent['talent_nickname'] ?> #<?= $nomor ?>" >
                                    </a>
                                    <?php
                                            } else {
                                                // Portrait or Square
                                    ?>
                                    <a href="<?php echo $full_url; ?>" class="fancylight popup-btn col-md-3 linkGall" data-fancybox-group="light">
                                        <img class="imgGall" src="<?php echo $full_url; ?>" alt="<?= $talent['talent_nickname'] ?> #<?= $nomor ?>" >
                                    </a>
                                    <?php
                                            }
                                        }
                                    ?>
                                        
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane text-center" id="settings">
                                    <div class="container">
                                    <?php 
                                    $coverVideoURL = '';
                                    foreach ($galleryVideo as $v) { 
                                        
                                        $full_url = base_url(). $v->media_url;
                                        if($v->media_mime <>'video/embed'){
                                            if( !empty($coverVideo) ) {
                                                foreach ($coverVideo as $cv) { 
                                                    if($v->media_id == $cv->media_mime)
                                                    {
                                                        $coverVideoURL = base_url(). $cv->media_url;
                                                    }        
                                                }
                                            }
                                            
                                    ?>
                                    <video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto"  width="800" height="600"  data-setup='{}' style="margin: 0 auto;" poster="<?php echo $coverVideoURL; ?>">
                                        <source src="<?php echo $full_url; ?>" type='video/mp4'>
                                        <source src="<?php echo $full_url; ?>" type='video/webm'>
                                        <p class="vjs-no-js">
                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                        </p>
                                    </video>
                                    <?php
                                        }
                                        else{
                                        $media_url = stripslashes($v->media_url);
                                        echo $media_url.' <br/><br/>';
                                    ?>

                                    <?php
                                        }
                                    } 
                                    ?>
                                    </div>    
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="polaroid">
                                    <div class="row">
                                    <?php 
                                        $nomor=0;
                                        foreach ($polaroid as $g) { 
                                            $nomor++;
                                            $full_url = base_url(). $g->media_url;
                                            
                                    ?>
                                    <a href="<?php echo $full_url; ?>" class="fancylight popup-polaroid col-md-3 linkGall" data-fancybox-group="light">
                                        <img class="imgGall" src="<?php echo $full_url; ?>" alt="Polaroid <?= $talent['talent_nickname'] ?> #<?= $nomor ?>" >
                                    </a>
                                    <?php
                                        }
                                    ?>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="instagram">
                                    <div class="preloader" id="preloader"></div>
                                    <div class="agency-instagram" id="agency-instagram"></div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.card-body -->    
                    </div>
                    <!-- /.nav-tabs-custom -->        

                </div>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->