<style>
.col-filter{
    padding: 0 20px;
    
}
.col-filter h2 {
  padding: .5rem 1rem;
}
.btn-tags{
  margin: .5rem 1rem;
}
.card-filter, .card-tags {
  box-shadow: none;
}
.card-filter .card-header .card-title, .card-tags .card-header .card-title {
    font-weight: 700;
    font-size: 20px;
}

.card-filter .card-header, .card-tags .card-header{
  border-bottom: none;
  padding: .5rem 1rem;
}
.card-filter .nav.flex-column>li, .card-tags .nav.flex-column>li {
    border-bottom: none;
    margin: 0;
}
.card-filter .nav.flex-column>li:last-of-type {
    border-bottom: 1px solid rgba(0,0,0,.125);
    padding-bottom: 30px;
}
.btn-tags{
  width:200px;
}
.agency-catalog-wrapper {
      width: 90%;
      margin: 0 auto;
      display: grid;
      grid-gap: 10px;
      grid-template-columns: repeat(4, 25%);
}
.agency-catalog-box {
	  min-height: 400px;
	  height: 400px;
	  position: relative;
      overflow: hidden;
	  font-size: 150%;  	  
	    -webkit-transition: all 0.2s ease-in-out;
        -moz-transition: all 0.2s ease-in-out;
        -o-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
}
.agency-catalog-novideo::before {
  content: "";
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: inherit;
  transition: inherit;
  -webkit-transition: all 0.2s ease-in-out;
  -moz-transition: all 0.2s ease-in-out;
  -o-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.agency-catalog-novideo:hover::before {
  transform: scale(1.1);
}
.agency-catalog-box .agency-box-overlay {
  padding: 180px 20px 0px;
  box-sizing: border-box;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: 7;
  overflow: hidden;
  display: block;
  -webkit-transition: all 0.2s ease-in-out;
  -moz-transition: all 0.2s ease-in-out;
  -o-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.agency-catalog-box .agency-box-overlay .agency-talent-title {
  position: relative;
  z-index: 1;
  text-align: center;
  color: #fff;
  float: none;
  font-size: 32px;
  letter-spacing: 2px;
  font-weight: 300;
  font-family: "Nunito Sans";
  visibility: hidden;
  opacity: 0;
  -webkit-transition: visibility 0s, opacity 0.2s linear;
  -moz-transition: visibility 0s, opacity 0.2s linear;
  -o-transition: visibility 0s, opacity 0.2s linear;
  transition: visibility 0s, opacity 0.2s linear;
}
.agency-catalog-box:hover{
  cursor: pointer;
}
.agency-catalog-box:hover .agency-box-overlay {
  background-color: rgba(0, 0, 0, 0.2);
}
.agency-catalog-box:hover .agency-box-overlay .agency-talent-title, .agency-catalog-box:hover .agency-box-overlay .agency-talent-category {
  visibility: visible;
  opacity: 1;
}
.agency-catalog-box .agency-box-overlay .agency-talent-category {
    position: relative;
    text-align: center;
    z-index: 1;
    color: #fff;
    float: none;
    font-size: 16px;
    letter-spacing: 2px;
    text-transform: uppercase;
    font-weight: 500;
    visibility: hidden;
    font-family: "Nunito Sans";
    opacity: 0;
    -webkit-transition: visibility 0s, opacity 0.2s linear;
    -moz-transition: visibility 0s, opacity 0.2s linear;
    -o-transition: visibility 0s, opacity 0.2s linear;
    transition: visibility 0s, opacity 0.2s linear;
}
.agency-catalog-box .agency-box-button{
  padding: 0;
  margin: 0;
  position: absolute;
  right: -50px;
  top: 20px;
  transition: 0.2s;
  -webkit-transition: 0.2s;
  z-index: 20;
  display: block;
}
.agency-catalog-box:hover .agency-box-button{
  right: 20px;
}
.agency-catalog-box .agency-box-button a.selected-talent{
  color:#fff;
  background: #ff007d;
  border-radius: 50%;
  padding: 3px 7px;
  font-size: 18px;
}
.agency-catalog-box .agency-box-button a{
  color:#fff;
  background: #ffffff3b;
  border-radius: 50%;
  padding: 3px 7px;
  font-size: 18px;
  transition:0.5s ease;
  -moz-transition:0.5s ease;
  -webkit-transition:0.5s ease;
}
.agency-catalog-box video {
  top: 50%;
  left: 50%;
  max-width: 100%;
  max-height: 500px;
  width: 100%;
  height: 400px;
  z-index: -1000;
  overflow: hidden;
  object-fit: cover;
  -webkit-transition: all 400ms ease-out;
  -moz-transition: all 400ms ease-out;
  -o-transition: all 400ms ease-out;
  -ms-transition: all 400ms ease-out;
  transition: all 400ms ease-out;
  opacity: 0;
}
.agency-catalog-box:hover video {
  opacity: 1;
}

</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper content-website">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div><!-- /.col -->
          <div class="col-sm-6">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

<!-- Main content -->
<div class="content">
      <div class="">
      
        <div class="row">
            <div class="col-lg-12 text-center" style=" margin-bottom:50px;">
                <h1 class="m-0 "></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-filter">
                <h2 class="mb-5">Filters</h2>
                <div class="card card-filter"> 
                <div class="card-header mb-3">
                    <h3 class="card-title">Categories</h3>
                  </div>
                  <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column nav-categories" id="category_list">
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Model</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Photographer</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Hair & Make Up Artist</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Stylist</a>
                        </li>
                    </ul>
                  </div>
                  <!-- /.card-body -->
                </div>

                <div class="card card-filter"> 
                <div class="card-header mb-3">
                    <h3 class="card-title">Complexion</h3>
                  </div>
                  <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column nav-complexion" id="complexion_list">
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Black</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Brown</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Fair</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Olive</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Other</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Tanned</a>
                        </li>
                    </ul>
                  </div>
                  <!-- /.card-body -->
                </div>

                <div class="card card-filter"> 
                <div class="card-header mb-3">
                    <h3 class="card-title">Ethnicity</h3>
                  </div>
                  <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column nav-ethnicity" id="ethnicity_list">
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">African</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Asian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Caucasian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Indian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Indonesian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Multi Ethnic</a>
                        </li>
                    </ul>
                  </div>
                  <!-- /.card-body -->
                </div>

                <div class="card card-tags"> 
                <div class="card-header mb-3">
                    <h3 class="card-title">Tags</h3>
                  </div>
                  <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column nav-tags" id="tags_list">
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">African</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Asian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Caucasian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Indian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Indonesian</a>
                        </li>
                        <li class="nav-item ">
                            <a href="#" at="test" class="nav-link">Multi Ethnic</a>
                        </li>
                    </ul>
                    <button type="button" class="btn btn-block btn-outline-secondary btn-sm btn-tags mt-3">Secondary</button>
                  </div>
                  <!-- /.card-body -->
                </div>

            </div>
            <div class="col-md-9">
              <div class="agency-catalog-wrapper">
                <div class="agency-catalog-box agency-catalog-hasvideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-lente-headshot.png');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <video muted loop playsinline class="video">
                      <source src="<?php echo base_url(); ?>src/admin_assets/dist/img/video/headshot-LENTE_1.mp4" type="video/mp4" />
                      Your browser does not support video
                  </video>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Lente</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-hasvideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-andreas-b-balimodelagency.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <video muted loop playsinline class="video">
                      <source src="<?php echo base_url(); ?>src/admin_assets/dist/img/video/headshot-ANDREAS_2.mp4" type="video/mp4" />
                      Your browser does not support video
                  </video>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Andreas B</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-hasvideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-HANNA_K_BALIMODELAGENCY.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <video muted loop playsinline class="video">
                      <source src="<?php echo base_url(); ?>src/admin_assets/dist/img/video/headshot-HANNA_K.mp4" type="video/mp4" />
                      Your browser does not support video
                  </video>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Hanna K</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-hasvideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-SATIWAINE-bma.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <video muted loop playsinline class="video">
                      <source src="<?php echo base_url(); ?>src/admin_assets/dist/img/video/headshot-Santiwaine-1.mp4" type="video/mp4" />
                      Your browser does not support video
                  </video>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Santiwaine T</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>  
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-lente-headshot.png');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Lente</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-andreas-b-balimodelagency.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Andreas B</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-HANNA_K_BALIMODELAGENCY.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Hanna K</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-SATIWAINE-bma.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Santiwaine T</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>  
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-lente-headshot.png');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Lente</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-andreas-b-balimodelagency.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Andreas B</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-HANNA_K_BALIMODELAGENCY.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Hanna K</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-SATIWAINE-bma.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Santiwaine T</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>  
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-lente-headshot.png');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Lente</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-andreas-b-balimodelagency.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Andreas B</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-HANNA_K_BALIMODELAGENCY.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Hanna K</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-SATIWAINE-bma.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Santiwaine T</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>  
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-lente-headshot.png');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Lente</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-andreas-b-balimodelagency.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Andreas B</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-HANNA_K_BALIMODELAGENCY.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Hanna K</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>
                </div>
                <div class="agency-catalog-box agency-catalog-novideo" style="background-image: url('<?php echo base_url(); ?>src/admin_assets/dist/img/headshot-SATIWAINE-bma.jpg');background-position: 50% 60%;background-repeat: no-repeat;background-size: cover;" >
                  <div class="agency-box-button">
                      <a href="#" title="Add to Favorite" ><i class="fa fa-heart" aria-hidden></i></a>
                  </div>
                  <div class="agency-box-overlay">
                      <a href="#" >
                          <h3 class="agency-talent-title">Santiwaine T</h3>
                          <p class="agency-talent-category">Model</p>
                      </a>
                  </div>  
                </div>

              </div>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->