<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_setting extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
    }
    public function GetSetting(){
		$options = $this->db->get('agc_options');
		return $options;
	}
	
	public function update_setting($sitename,$companyname,$address,$zipcode,$phone,$email,$website,$textwebsite,$facebook,$ig,$youtube,$country,$twitter){
		
		$date = date("Y-m-d H:i:s");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$sitename',date_created='$date' WHERE option_name='sitename' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$companyname',date_created='$date' WHERE option_name='companyname' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$address',date_created='$date' WHERE option_name='companyaddress' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$zipcode',date_created='$date' WHERE option_name='companyzipcode' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$country',date_created='$date' WHERE option_name='companycountry' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$phone',date_created='$date' WHERE option_name='companyphone' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$email',date_created='$date' WHERE option_name='companymail' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$website',date_created='$date' WHERE option_name='companyweb' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$textwebsite',date_created='$date' WHERE option_name='companytextweb' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$facebook',date_created='$date' WHERE option_name='companyfb' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$facebook',date_created='$date' WHERE option_name='companyfb' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$ig',date_created='$date' WHERE option_name='companyig' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$youtube',date_created='$date' WHERE option_name='companyyt' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$twitter',date_created='$date' WHERE option_name='companytwitter' ");
		
		$desc='Update Setting Apps';
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);

		return $hsl;
	}
	public function update_email($stmphost,$stmpauth,$username,$password,$stmpsecure,$stmpport,$emailfrom,$emailreply,$sendgrid){
		
		$date = date("Y-m-d H:i:s");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$stmphost',date_created='$date' WHERE option_name='stmphost' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$stmpauth',date_created='$date' WHERE option_name='stmpauth' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$username',date_created='$date' WHERE option_name='stmpusername' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$password',date_created='$date' WHERE option_name='stmppassword' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$stmpsecure',date_created='$date' WHERE option_name='stmpsecure' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$stmpport',date_created='$date' WHERE option_name='stmpport' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$emailfrom',date_created='$date' WHERE option_name='emailfrom' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$emailreply',date_created='$date' WHERE option_name='emailreplyto' ");
		$hsl=$this->db->query("UPDATE agc_options SET option_value='$sendgrid',date_created='$date' WHERE option_name='sendgrid_api' ");
		
		
		$desc='Update Setting Email';
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);

		return $hsl;
	}
	
}

/* End of file M_user.php */
/* Location: ./application/models/m_login.php */