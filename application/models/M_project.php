<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_project extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function GetProject(){ 
        $this->db->select('agc_projects.*,agc_client.client_name');
        $this->db->from('agc_projects');
        $this->db->join('agc_client', 'agc_client.client_id = agc_projects.client_id');
        $query = $this->db->get();
		return $query;
		//$pengguna = $this->db->get('agc_package');
		//return $pengguna;
    }
    public function GetProjectActive(){ 
        $this->db->select('agc_projects.*,agc_client.client_name');
        $this->db->from('agc_projects');
        $this->db->join('agc_client', 'agc_client.client_id = agc_projects.client_id');
        $this->db->where('agc_projects.status_project', 1);
        $query = $this->db->get();
		return $query;
		//$pengguna = $this->db->get('agc_package');
		//return $pengguna;
	}
	public function project($id_project){
        //return $this->db->get_where('agc_package', array('package_id' => $package_id));
        $this->db->select('agc_projects.*,agc_client.client_name');
        $this->db->from('agc_projects');
        $this->db->join('agc_client', 'agc_client.client_id = agc_projects.client_id');
        $this->db->where('agc_projects.id_project', $id_project);
        $query = $this->db->get();
		return $query;
    }
    public function projectEN($id_project){
        //return $this->db->get_where('agc_package', array('package_id' => $package_id));
        $this->db->select('agc_projects.*,agc_client.client_name');
        $this->db->from('agc_projects');
        $this->db->join('agc_client', 'agc_client.client_id = agc_projects.client_id');
        $this->db->where('sha1(md5(agc_projects.id_project))', $id_project);
        $query = $this->db->get();
		return $query;
    }
    public function GetProjectTalent($id){
        $this->db->select('agc_projects.*,agc_talent.talent_id,CONCAT(agc_talent.talent_fname," ",agc_talent.talent_lname)as "talentname",agc_talent.talent_slug,agc_talent.talent_nickname,agc_talent_media.media_url');
        $this->db->from('agc_project_talent');
        $this->db->join('agc_projects', 'agc_project_talent.id_project = agc_projects.id_project');
        $this->db->join('agc_talent', 'agc_project_talent.talent_id = agc_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_project_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_projects.id_project', $id);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        $query = $this->db->get();
		return $query;
		
    }
    public function talentInProject($id_project,$talent_id){
        $this->db->select('agc_projects.*,agc_talent.talent_nickname');
        $this->db->from('agc_projects');
        $this->db->join('agc_project_talent', 'agc_projects.id_project = agc_project_talent.id_project');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_project_talent.talent_id');
        $this->db->where('agc_projects.id_project', $id_project);
        $this->db->where('agc_project_talent.talent_id', $talent_id);
        $query = $this->db->get();
		return $query; 
    }
    public function create_project($name,$customer,$location,$datestartproject,$dateendproject,$notes,$type)
    {
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_projects');
		$jml = $query->num_rows();
		$jml++;
		$kode = 'PJC'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		
		$dataProject = array(
                            'id_project' 		    => $kode,
                            'name_project' 		    => $name,
                            'type_project' 	        => $type,
                            'start_project' 	    => $datestartproject,
                            'end_project' 	        => $dateendproject,
                            'location_project' 	    => $location,
                            'notes_project' 	    => $notes,
                            'status_project' 	    => 1,
                            'client_id' 		    => $customer
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_projects', $dataProject);
		
		$desc='Add project '.$kode;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
		return $kode;
    }
    public function update_project($name,$customer,$location,$datestartproject,$dateendproject,$notes,$type,$status,$id)
    {
        $dataProject = array(
            'name_project' 		    => $name,
            'type_project' 	        => $type,
            'start_project' 	    => $datestartproject,
            'end_project' 	        => $dateendproject,
            'location_project' 	    => $location,
            'notes_project' 	    => $notes,
            'status_project' 	    => $status,
            'client_id' 		    => $customer
        );

        $this->db->where('id_project', $id);
        $this->db->update('agc_projects', $dataProject);

		$desc='Update project '.$id;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
    public function addTalent($talent,$id){
        //$query = $this->db->query("SELECT * FROM agc_project_talent WHERE id_project='$id' ");
		//$tot = $query->num_rows();
        //$tot = $tot +1;
        
        $dataProject = array(
            'id_project' 		    => $id,
            'talent_id' 		    => $talent
        ); 
        //print_r($dataPengguna);
        $this->db->insert('agc_project_talent', $dataProject);

        $desc='Add Talent '. $talent .' project '.$id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
    public function deleteTalentInProject($id,$talent_id)
    {
        $this->db->delete('agc_project_talent', array('id_project' => $id,'talent_id' => $talent_id));
        
        $desc='Delete Talent '. $talent_id .' project '.$id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
}

