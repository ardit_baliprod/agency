<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_talent extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
        $this->load->model('M_log');
        $this->load->model('M_tags');
        $this->load->model('M_catalog');
    }
    
	public function GetTalent(){
        /*
        $this->db->select('agc_talent.*,agc_gender.gender_name,agc_ethnicity.ethnicity_name');
		$this->db->from('agc_talent');
        $this->db->join('agc_gender', 'agc_talent.gender_id = agc_gender.gender_id');
        $this->db->join('agc_ethnicity', 'agc_talent.ethnicity_id = agc_ethnicity.ethnicity_id');
        $query = $this->db->get();
        return $query; */
        $hsl=$this->db->query("SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id 
        GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id WHERE a.talent_status != 2  ");
        return $hsl;
	}
	public function talent($talent_id){
        /*
		$this->db->select('agc_talent.*,agc_gender.gender_name,agc_ethnicity.ethnicity_name');
		$this->db->from('agc_talent');
        $this->db->join('agc_gender', 'agc_talent.gender_id = agc_gender.gender_id');
        $this->db->join('agc_ethnicity', 'agc_talent.ethnicity_id = agc_ethnicity.ethnicity_id');
        $this->db->where('agc_talent.talent_id', $talent_id);
        $query = $this->db->get();
        return $query; */
        $hsl=$this->db->query("SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id WHERE a.talent_id='$talent_id' ");
        return $hsl;
    }
    public function talentAPI($talent_id){
        /*
		$this->db->select('agc_talent.*,agc_gender.gender_name,agc_ethnicity.ethnicity_name');
		$this->db->from('agc_talent');
        $this->db->join('agc_gender', 'agc_talent.gender_id = agc_gender.gender_id');
        $this->db->join('agc_ethnicity', 'agc_talent.ethnicity_id = agc_ethnicity.ethnicity_id');
        $this->db->where('agc_talent.talent_id', $talent_id);
        $query = $this->db->get();
        return $query; */
        $hsl=$this->db->query("SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id WHERE a.talent_id='$talent_id' AND a.talent_status=1 ");
        return $hsl;
    }
    public function talentSlug($talent_slug){
        $hsl=$this->db->query("SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id WHERE a.talent_slug='$talent_slug' ");
        return $hsl;
    }
    public function talentSlugAPI($talent_slug){
        $hsl=$this->db->query("SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id WHERE a.talent_slug='$talent_slug' AND a.talent_status=1 ");
        return $hsl;
    }
    public function talentCategoryAPI($id = null,$tag= null,$gender= null)
    {
        $url = base_url();
        $query_hsl="SELECT a.*,b.gender_name,c.ethnicity_name,e.category_name,i.compelxionname,j.media_url AS urlimage,j.media_type AS typeimage,j.media_mime AS mimeimage,k.media_url AS urlvideo,k.media_type AS typevideo,k.media_mime AS mimevideo,'$url' AS 'baseurl'
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN agc_talent_category d ON a.talent_id = d.talent_id
        LEFT JOIN agc_category e ON d.category_id = e.category_id
        LEFT JOIN 
        ( 
        SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' 
        FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id 
        ) i ON a.talent_id = i.talent_id 
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'image%'
        ) j ON a.talent_id = j.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'video%'
        ) k ON a.talent_id = k.talent_id
        WHERE e.category_id = '$id' AND a.talent_status=1 
         ";
        if($tag <> ''){
            $query_hsl .= " AND  a.talent_tag LIKE '%$tag%' ";
        }
        if($gender <> ''){
            $query_hsl .= " AND  b.gender_slug = '$gender' ";
        }  
        $query_hsl .= " ORDER BY a.talent_fname";
        $hsl=$this->db->query($query_hsl);
        return $hsl;
    }
    public function talentCategorySlugAPI($id = null,$tag= null,$gender= null)
    {
        $url = base_url();
        $query_hsl="SELECT a.*,b.gender_name,c.ethnicity_name,e.category_name,i.compelxionname,j.media_url AS urlimage,j.media_type AS typeimage,j.media_mime AS mimeimage,k.media_url AS urlvideo,k.media_type AS typevideo,k.media_mime AS mimevideo,'$url' AS 'baseurl'
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN agc_talent_category d ON a.talent_id = d.talent_id
        LEFT JOIN agc_category e ON d.category_id = e.category_id
        LEFT JOIN 
        ( 
        SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' 
        FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id 
        ) i ON a.talent_id = i.talent_id 
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'image%'
        ) j ON a.talent_id = j.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'video%'
        ) k ON a.talent_id = k.talent_id
        WHERE e.category_slug = '$id' AND a.talent_status=1 ";
        if($tag <> ''){
            $query_hsl .= " AND  a.talent_tag LIKE '%$tag%' ";
        }
        if($gender <> ''){
            $query_hsl .= " AND  b.gender_slug = '$gender' ";
        } 
        $query_hsl .= " ORDER BY a.talent_fname ";
        $hsl=$this->db->query($query_hsl);
        return $hsl;
    }
    public function talentCatalogSlugAPI($id)
    {
        $url = base_url();
        $query_hsl="SELECT a.*,b.gender_name,c.ethnicity_name,e.category_name,i.compelxionname,j.media_url AS urlimage,j.media_type AS typeimage,j.media_mime AS mimeimage,k.media_url AS urlvideo,k.media_type AS typevideo,k.media_mime AS mimevideo,l.list_order,'$url' AS 'baseurl'
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN agc_talent_category d ON a.talent_id = d.talent_id
        LEFT JOIN agc_category e ON d.category_id = e.category_id
        LEFT JOIN 
        ( 
        SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' 
        FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id 
        ) i ON a.talent_id = i.talent_id 
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'image%'
        ) j ON a.talent_id = j.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'video%'
        ) k ON a.talent_id = k.talent_id
        LEFT JOIN agc_catalog_talent l on a.talent_id = l.talent_id
        LEFT JOIN agc_catalog m on m.catalog_id = l.catalog_id
        WHERE  m.catalog_slug = '$id' AND a.talent_status=1
        GROUP BY a.talent_id  
        ORDER BY l.list_order  ";
        $hsl=$this->db->query($query_hsl);
        return $hsl;
    }
    public function talentCatalogAPI($id)
    {
        $url = base_url();
        $query_hsl="SELECT a.*,b.gender_name,c.ethnicity_name,e.category_name,i.compelxionname,j.media_url AS urlimage,j.media_type AS typeimage,j.media_mime AS mimeimage,k.media_url AS urlvideo,k.media_type AS typevideo,k.media_mime AS mimevideo,l.list_order,'$url' AS 'baseurl'
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN agc_talent_category d ON a.talent_id = d.talent_id
        LEFT JOIN agc_category e ON d.category_id = e.category_id
        LEFT JOIN 
        ( 
        SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' 
        FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id 
        ) i ON a.talent_id = i.talent_id 
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'image%'
        ) j ON a.talent_id = j.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'video%'
        ) k ON a.talent_id = k.talent_id
        LEFT JOIN agc_catalog_talent l on a.talent_id = l.talent_id
        LEFT JOIN agc_catalog m on m.catalog_id = l.catalog_id
        WHERE  m.catalog_id = '$id' AND a.talent_status=1
        GROUP BY a.talent_id  
        ORDER BY l.list_order ";
        $hsl=$this->db->query($query_hsl);
        return $hsl;
		
    }
    public function talentCategory($id){
        $this->db->select('agc_talent.*,agc_category.category_id,agc_category.category_name ');
		$this->db->from('agc_talent');
        $this->db->join('agc_talent_category', 'agc_talent.talent_id = agc_talent_category.talent_id');
        $this->db->join('agc_category', 'agc_talent_category.category_id = agc_category.category_id');
        $this->db->where('agc_talent.talent_id', $id);
        $query = $this->db->get();
        return $query;
    }
    public function talentCompelxion($id){
        $this->db->select('agc_talent.*,agc_compelxion.compelxion_id,agc_compelxion.compelxion_name ');
		$this->db->from('agc_talent');
        $this->db->join('agc_talent_compelxion', 'agc_talent.talent_id = agc_talent_compelxion.talent_id');
        $this->db->join('agc_compelxion', 'agc_talent_compelxion.compelxion_id = agc_compelxion.compelxion_id');
        $this->db->where('agc_talent.talent_id', $id);
        $query = $this->db->get();
        return $query;
    } 
    public function talentPackagesEmail($id = null)
    {
        $this->db->select('agc_talent.*,agc_talent_media.media_url,agc_talent_media.media_order');
        $this->db->from('agc_package_talent');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_package_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->where('agc_package_talent.package_id', $id);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        //$this->db->order_by("agc_talent.talent_fname");
        $this->db->order_by("agc_package_talent.list_order");
        $query = $this->db->get();
		return $query;
    }
    public function talentPackage($id = null)
    {
        $hsl=$this->db->query(" SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,j.media_type,j.media_mime,j.media_url FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id LEFT JOIN agc_talent_media j ON a.talent_id = j.talent_id WHERE j.media_type = 'Headshot' AND j.media_mime LIKE 'image%' AND a.talent_status=1 AND a.talent_id NOT IN (SELECT talent_id FROM agc_package_talent WHERE package_id = '$id')  ");
        return $hsl;
    }
    public function talentPackageFilter($id = null,$gender = null,$ethnicity = null,$tag = null,$category = null,$compelxion = null)
    {
        $qry="SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,j.media_type,j.media_mime,j.media_url,f.categoryid,i.compelxionid
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname',GROUP_CONCAT(e.category_id SEPARATOR', ') AS 'categoryid' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id 
        LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname',GROUP_CONCAT(h.compelxion_id SEPARATOR', ') AS 'compelxionid' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id  GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id 
        LEFT JOIN agc_talent_media j ON a.talent_id = j.talent_id 
        WHERE j.media_type = 'Headshot' AND j.media_mime LIKE 'image%' AND a.talent_status=1
        AND a.talent_id NOT IN (SELECT talent_id FROM agc_package_talent WHERE package_id = '$id')  ";

        if($gender && $gender <> ''){
            $qry .=" AND a.gender_id='$gender' ";
        }
        if($ethnicity && $ethnicity <> ''){
            $qry .=" AND a.ethnicity_id='$ethnicity' ";
        }
        if($tag && $tag <> ''){
            $qry .=" AND a.talent_tag LIKE '%$tag%' ";
        }
        if($category && $category <> ''){
            $qry .=" AND f.categoryid LIKE '%$category%' ";
        }
        if($compelxion && $compelxion <> ''){
            $qry .=" AND i.compelxionid LIKE '%$compelxion%' ";
        }
        $qry .=" ORDER BY a.talent_nickname ";
        

        $hsl=$this->db->query($qry);
        return $hsl;
    }
    public function talentProjectFilter($id = null,$gender = null,$ethnicity = null,$tag = null,$category = null,$compelxion = null)
    {
        $qry="SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,j.media_type,j.media_mime,j.media_url,f.categoryid,i.compelxionid
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname',GROUP_CONCAT(e.category_id SEPARATOR', ') AS 'categoryid' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id 
        LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname',GROUP_CONCAT(h.compelxion_id SEPARATOR', ') AS 'compelxionid' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id  GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id 
        LEFT JOIN agc_talent_media j ON a.talent_id = j.talent_id 
        WHERE j.media_type = 'Headshot' AND j.media_mime LIKE 'image%' AND a.talent_status=1
        AND a.talent_id NOT IN (SELECT talent_id FROM agc_project_talent WHERE id_project = '$id')  ";

        if($gender && $gender <> ''){
            $qry .=" AND a.gender_id='$gender' ";
        }
        if($ethnicity && $ethnicity <> ''){
            $qry .=" AND a.ethnicity_id='$ethnicity' ";
        }
        if($tag && $tag <> ''){
            $qry .=" AND a.talent_tag LIKE '%$tag%' ";
        }
        if($category && $category <> ''){
            $qry .=" AND f.categoryid LIKE '%$category%' ";
        }
        if($compelxion && $compelxion <> ''){
            $qry .=" AND i.compelxionid LIKE '%$compelxion%' ";
        }
        $qry .=" ORDER BY a.talent_nickname ";
        

        $hsl=$this->db->query($qry);
        return $hsl;
    }
    public function talentCatalog($id = null)
    {
        $catalog = $this->M_catalog->GetUpdateCatalog($id)->row_array();
        $idCategory = $catalog['category_id'];
        $idGender = $catalog['gender_id'];
        $tags = $this->M_catalog->GetCatalogTags($id)->result();


        $rowqry = " SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,j.media_type,j.media_mime,j.media_url,f.categoryid  FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id LEFT JOIN ( SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname',GROUP_CONCAT(e.category_id SEPARATOR', ') AS 'categoryid' FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id ) f ON a.talent_id = f.talent_id LEFT JOIN ( SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id ) i ON a.talent_id = i.talent_id LEFT JOIN agc_talent_media j ON a.talent_id = j.talent_id WHERE j.media_type = 'Headshot' AND j.media_mime LIKE 'image%' AND a.talent_status=1 AND a.talent_id NOT IN (SELECT talent_id FROM agc_catalog_talent WHERE catalog_id = '$id')  ";

        if($idCategory && $idCategory <> '-'){
            $rowqry .= "AND f.categoryid LIKE '%$idCategory%' ";
        }
        if($idGender && $idGender <> '-'){
            $rowqry .= "AND a.gender_id ='$idGender' ";
        }
        if($tags){
            foreach ($tags as $t) {
                $rowqry .= "AND a.talent_tag LIKE '%$t->name_tags%'  ";
            }
        }


        $hsl=$this->db->query($rowqry);
        return $hsl;
    }
    public function create_talent($name,$s,$fname,$lname,$email,$phone,$birth,$age,$height,$bust,$waist,$hips,$shoe,$hair,$eye,$gender,$category,$ethnicity,$compelxion,$desc,$tags,$notes,$instagram,$portofolio,$dress,$chest,$suit,$collar,$bra,$shirt,$pants){
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_talent');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'TLT'. str_pad($jml, 4, '0', STR_PAD_LEFT);

        $dataTalent = array(
            'talent_id' 		        => $kode,
            'talent_fname' 		        => $fname,
            'talent_lname' 		        => $lname,
            'talent_nickname' 		    => $name,
            'talent_slug' 		        => $s,
            'talent_email' 		        => $email,
            'talent_phone' 		        => $phone,
            'talent_height' 		    => $height,
            'talent_bust' 		        => $bust,
            'talent_waist' 		        => $waist,
            'talent_hips' 		        => $hips,
            'talent_shoe' 		        => $shoe,
            'talent_datebirth' 		    => $birth,
            'talent_age' 		        => $age,
            'talent_bio' 		        => $desc,
            'talent_hairColor' 		    => $hair,
            'talent_eyeColor' 		    => $eye,
            'talent_tag' 		        => $tags,
            'talent_status' 		    => 1,
            'talent_notes' 		        => $notes,
            'talent_instagram'	        => $instagram,
            'talent_portofolio'	        => $portofolio,
            'talent_dress' 		        => $dress,
            'talent_chest' 		        => $chest,
            'talent_suit' 		        => $suit,
            'talent_collar' 		    => $collar,
            'talent_bra' 		        => $bra,
            'talent_shirt' 		        => $shirt,
            'talent_pants' 		        => $pants,
            'gender_id' 		        => $gender,
            'ethnicity_id' 		        => $ethnicity
            
        );

        //print_r($dataPengguna);
        $this->db->insert('agc_talent', $dataTalent);

        $this->M_tags->checkTag($tags);

        if(!empty($category)){
            foreach ($category as $cat)
            {
                //print "You are selected $names<br/>";
                $dataListing = array(
                            'talent_id'	            => $kode,
                            'category_id' 		    => $cat
                ); 
                //print_r($dataPengguna);
                $this->db->insert('agc_talent_category', $dataListing);
            }
        }

        if(!empty($compelxion)){
            foreach ($compelxion as $com)
            {
                //print "You are selected $names<br/>";
                $dataListing = array(
                            'talent_id'	            => $kode,
                            'compelxion_id' 		=> $com
                ); 
                //print_r($dataPengguna);
                $this->db->insert('agc_talent_compelxion', $dataListing);
            }
        }
        

        $desc='Add Talent '.$kode;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
        return $kode;
    }
    public function update_talent($name,$s,$fname,$lname,$email,$phone,$birth,$age,$height,$bust,$waist,$hips,$shoe,$hair,$eye,$gender,$category,$ethnicity,$compelxion,$desc,$tags,$id,$notes,$instagram,$portofolio,$dress,$chest,$suit,$collar,$bra,$shirt,$pants,$status){
        $dataTalent = array(
            'talent_fname' 		        => $fname,
            'talent_lname' 		        => $lname,
            'talent_nickname' 		    => $name,
            'talent_slug' 		        => $s,
            'talent_email' 		        => $email,
            'talent_phone' 		        => $phone,
            'talent_height' 		    => $height,
            'talent_bust' 		        => $bust,
            'talent_waist' 		        => $waist,
            'talent_hips' 		        => $hips,
            'talent_shoe' 		        => $shoe,
            'talent_datebirth' 		    => $birth,
            'talent_age' 		        => $age,
            'talent_bio' 		        => $desc,
            'talent_hairColor' 		    => $hair,
            'talent_eyeColor' 		    => $eye,
            'talent_tag' 		        => $tags,
            'talent_status' 		    => $status,
            'talent_notes' 		        => $notes,
            'talent_instagram'	        => $instagram,
            'talent_portofolio'	        => $portofolio,
            'talent_dress' 		        => $dress,
            'talent_chest' 		        => $chest,
            'talent_suit' 		        => $suit,
            'talent_collar' 		    => $collar,
            'talent_bra' 		        => $bra,
            'talent_shirt' 		        => $shirt,
            'talent_pants' 		        => $pants,
            'gender_id' 		        => $gender,
            'ethnicity_id' 		        => $ethnicity
            
        );

        $this->db->where('talent_id', $id);
        $this->db->update('agc_talent', $dataTalent);


        $this->M_tags->checkTag($tags);

        
        if(!empty($category)){
          $this->db->delete('agc_talent_category', array('talent_id' => $id));  
          foreach ($category as $cat)
          {
              //print "You are selected $names<br/>";
              $dataListing = array(
                          'talent_id'	            => $id,
                          'category_id' 		    => $cat 
              ); 
              //print_r($dataPengguna);
              $this->db->insert('agc_talent_category', $dataListing);
          }
        }

        $this->db->delete('agc_talent_compelxion', array('talent_id' => $id));
        if(!empty($compelxion)){
            foreach ($compelxion as $com)
            {
                //print "You are selected $names<br/>";
                $dataListing = array(
                            'talent_id'	            => $id,
                            'compelxion_id' 		=> $com
                ); 
                //print_r($dataPengguna);
                $this->db->insert('agc_talent_compelxion', $dataListing);
            }
        }

        $desc='Update Talent '.$id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
        //return $id;

    }
    public function delete_talent($id)
    {
        $hsl=$this->db->query("UPDATE agc_talent SET talent_status=2,talent_slug=CONCAT(talent_slug,'-delete') WHERE talent_id='$id' ");
		
		
		$desc='Delete Talent '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		//return $hsl;
    }

    
}