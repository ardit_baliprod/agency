<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_country extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function GetCountry(){
		$query = $this->db->get('tblcountries');
		return $query;
	}
    
}

/* End of file M_country.php */
/* Location: ./application/models/M_country.php */