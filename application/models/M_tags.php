<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_tags extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetTags(){
		$pengguna = $this->db->get('agc_tags');
		return $pengguna; 
	}
	public function tag($id_tags){
		return $this->db->get_where('agc_tags', array('id_tags' => $id_tags));
	}
	public function tagInput($name){
		$hsl=$this->db->query("SELECT name_tags FROM agc_tags WHERE name_tags LIKE '$name%' ORDER BY name_tags ASC ");
		return $hsl;
    } 
    public function create_tags($name){
		/*
        $date = date("Y-m-d H:i:s"); 
        $query = $this->db->query('SELECT * FROM agc_tags');
		$jml = $query->num_rows();
		$jml++;
		$kode = 'TGS'. str_pad($jml, 4, '0', STR_PAD_LEFT); */
		$tag = strtolower($name);
		
		
		$dataTags = array(
                            'name_tags' 		        => $tag
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_tags', $dataTags);

		$query = $this->db->query('SELECT * FROM agc_tags ORDER BY id_tags DESC');
		$tags  = $query->row_array();
		$kode  = $tags['id_tags']; 
		
		$desc='Add Tags '.$kode.' '.$tag;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
    }
    public function update_tags($name,$id){
        $hsl=$this->db->query("UPDATE agc_tags SET name_tags='$name' WHERE id_tags='$id' ");

        $desc='Update Tags '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
	}
	public function checkTag($tags){
        $tagsArr = explode(',', $tags);
        foreach($tagsArr as $t){
            //echo "Skill : ".$i."<br/>";
			$tag = strtolower($t);
			if ($tag == trim($tag) && strpos($tag, ' ') !== false) {
				$input = $tag;
			}else{
				$input =  preg_replace('/\s+/', '', $tag,1);
			}
            $query = $this->db->query("SELECT * FROM agc_tags WHERE name_tags = '$input' ");

            $jml = $query->num_rows();

            if($jml == 0){
				
                $insquery = $this->db->query("INSERT INTO agc_tags ( name_tags) VALUES ('$input')");
            }
        }
	}
	public function delete_tags($id=null)
	{
		$this->db->delete('agc_tags', array('id_tags' => $id));

		$desc='Add Tags '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
	}
}