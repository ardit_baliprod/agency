<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	var $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('captcha');
		//$this->load->library('encrypt');

	
		$this->data = array(
			'action' => site_url('account/do_login'),
			'username' => set_value('username'),
			'password' => set_value('password'),
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
			'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
		);
	}

	public function index()
	{
		if($this->session->userdata('bma-agc')){
            $this->session->set_userdata('page', '404 Error');
            $this->template->load('agency-admin/static','agency-admin/404');
        }else{
			$data = $this->data;
			$data['cityShow'] = '';
			$data['cityName'] = '';
			$data['categoryShow'] = '';
			$data['categoryName'] = '';
            $this->session->set_userdata('page', '404 Error');
            $this->template->load('catalog/static-template','catalog/404',$data);
        }
	}
}
