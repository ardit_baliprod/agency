<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_media extends CI_Model {

    public $variable;
    

	public function __construct()
	{
		parent::__construct();
        $this->load->model('M_log');
        
	}
	public function GetMedia(){
		$pengguna = $this->db->get('agc_talent_media');
		return $pengguna;
	}
	public function media($media_id){
		return $this->db->get_where('agc_talent_media', array('media_id' => $media_id));
    }
    public function GetHeadshot($id){
        $this->db->select('agc_talent_media.*');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent_media.media_type', 'headshot');
        $this->db->where('agc_talent_media.talent_id', $id);
        $query = $this->db->get();
        return $query;
    }
    public function GetGalleryVideos($id){
        $this->db->select('agc_talent_media.*');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent_media.talent_id', $id);
        $this->db->like('agc_talent_media.media_mime', 'video', 'after'); 
        $query = $this->db->get();
        return $query; 
    }
    public function GetGalleryVideosAPI($id){
        $url = base_url();
        /*
        $this->db->select('agc_talent_media.*,agc_talent.talent_nickname,"'.$url.'" as "baseurl" ');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent_media.talent_id', $id);
        $this->db->like('agc_talent_media.media_mime', 'video', 'after'); */
        $query = $this->db->query(" SELECT a.*, b.talent_nickname, '$url' AS 'baseurl' , c.media_url AS 'coverurl'
        FROM agc_talent_media a
        JOIN agc_talent b ON b.talent_id = a.talent_id
        LEFT JOIN agc_talent_media c ON a.media_id = c.media_mime
        WHERE b.talent_status = 1
        AND a.media_type = 'Gallery'
        AND b.talent_id = '$id'
        AND a.media_mime LIKE 'video%' ESCAPE '!' ");
        return $query; 
    }
    public function GetGalleryVideosSlug($slug){
        $this->db->select('agc_talent_media.*');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent.talent_slug', $slug);
        $this->db->like('agc_talent_media.media_mime', 'video', 'after'); 
        $query = $this->db->get();
        return $query; 
    }
    public function GetGalleryVideosSlugAPI($slug){
        $url = base_url();
        /*
        $this->db->select('agc_talent_media.*,agc_talent.talent_nickname,"'.$url.'" as "baseurl" ');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent.talent_slug', $slug);
        $this->db->like('agc_talent_media.media_mime', 'video', 'after'); */
        
        $query = $this->db->query(" SELECT a.*, b.talent_nickname, '$url' AS 'baseurl' , c.media_url AS 'coverurl'
        FROM agc_talent_media a
        JOIN agc_talent b ON b.talent_id = a.talent_id
        LEFT JOIN agc_talent_media c ON a.media_id = c.media_mime
        WHERE b.talent_status = 1
        AND a.media_type = 'Gallery'
        AND b.talent_slug = '$slug'
        AND a.media_mime LIKE 'video%' ESCAPE '!' ");
        return $query; 
    }
    public function GetGalleryImages($id){
        $this->db->select('agc_talent_media.*');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent_media.talent_id', $id);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetGalleryImagesAPI($id){
        $url = base_url();
        $this->db->select('agc_talent_media.*,agc_talent.talent_nickname,"'.$url.'" as "baseurl" ');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent_media.talent_id', $id);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetGalleryImagesSlug($slug){
        $this->db->select('agc_talent_media.*');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent.talent_slug', $slug);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetGalleryImagesSlugAPI($slug){
        $url = base_url();
        $this->db->select('agc_talent_media.*,agc_talent.talent_nickname,"'.$url.'" as "baseurl" ');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_talent_media.media_type', 'Gallery');
        $this->db->where('agc_talent.talent_slug', $slug);
        $this->db->like('agc_talent_media.media_mime', 'image', 'both'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetPolaroidImages($id){
        $this->db->select('agc_talent_media.*');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent_media.media_type', 'Polaroid');
        $this->db->where('agc_talent_media.talent_id', $id);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetPolaroidImagesAPI($id){
        $url = base_url();
        $this->db->select('agc_talent_media.*,"'.$url.'" as "baseurl" ');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_talent_media.media_type', 'Polaroid');
        $this->db->where('agc_talent_media.talent_id', $id);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetPolaroidImagesSlug($slug){
        $this->db->select('agc_talent_media.*');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent_media.media_type', 'Polaroid');
        $this->db->where('agc_talent.talent_slug', $slug);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetPolaroidImagesSlugAPI($slug){
        $url = base_url();
        $this->db->select('agc_talent_media.*,"'.$url.'" as "baseurl" ');
		$this->db->from('agc_talent_media');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_talent_media.media_type', 'Polaroid');
        $this->db->where('agc_talent.talent_slug', $slug);
        $this->db->like('agc_talent_media.media_mime', 'image', 'after'); 
        $this->db->order_by('agc_talent_media.media_order', 'ASC');
        $query = $this->db->get();
        return $query;
    }
    public function GetVideoUncover($id){
        $query=$this->db->query("SELECT * FROM agc_talent_media WHERE talent_id = '$id' AND media_type = 'Gallery' AND media_mime LIKE 'video%' AND media_mime != 'video/embed' AND media_id NOT IN (SELECT media_mime FROM agc_talent_media WHERE media_mime NOT LIKE '%image%' AND media_mime NOT LIKE '%video%' AND talent_id = '$id') ");
        return $query;
    }
    public function GetCoverVideo($id){
        $query=$this->db->query(" SELECT b.*,a.media_name AS 'videoname'
        FROM agc_talent_media a LEFT JOIN agc_talent_media b ON a.media_id = b.media_mime
        WHERE b.media_type='Cover' AND a.talent_id = '$id' ");
        return $query;
    }
    public function GetCoverVideoAPI($id){
        $url = base_url();
        $query=$this->db->query(" SELECT b.*,a.media_name AS 'videoname','".$url."' as 'baseurl' 
        FROM agc_talent_media a LEFT JOIN agc_talent_media b ON a.media_id = b.media_mime
        LEFT JOIN agc_talent c ON a.talent_id = c.talent_id
        WHERE b.media_type='Cover' AND c.talent_id = '$id' AND c.talent_status=1 ");
        return $query;
    }
    public function GetCoverVideoSlug($id){
        $query=$this->db->query(" SELECT b.*,a.media_name AS 'videoname'
        FROM agc_talent_media a LEFT JOIN agc_talent_media b ON a.media_id = b.media_mime
        LEFT JOIN agc_talent c ON a.talent_id = c.talent_id
        WHERE b.media_type='Cover' AND c.talent_slug = '$id' ");
        return $query;
    }
    public function GetCoverVideoSlugAPI($id){
        $url = base_url();
        $query=$this->db->query(" SELECT b.*,a.media_name AS 'videoname','".$url."' as 'baseurl'
        FROM agc_talent_media a LEFT JOIN agc_talent_media b ON a.media_id = b.media_mime
        LEFT JOIN agc_talent c ON a.talent_id = c.talent_id
        WHERE b.media_type='Cover' AND c.talent_slug = '$id' AND c.talent_status=1 ");
        return $query;
    }
    public function GetGalleryPDF($id = null)
    {
        $url = base_url();
        $query=$this->db->query(" SELECT b.*,a.package_id,c.media_id,c.media_mime,c.media_name,c.media_order,c.media_type,c.media_url
        FROM agc_package_talent a LEFT JOIN agc_talent b ON a.talent_id = b.talent_id
        LEFT JOIN agc_talent_media c ON b.talent_id = c.talent_id
        WHERE b.talent_status = 1 AND c.media_mime LIKE 'image%' ESCAPE '!' AND a.package_id = '$id' AND c.media_type = 'Gallery'
        ORDER BY b.talent_id,c.media_order ");
        return $query;
    }
    public function delete_data($id){
        $hasil=$this->db->delete('agc_talent_media', array('media_id' => $id));
        
        $desc='Delete Media '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		return $hasil;
	}
    public function UploadHeadshot($fileName,$type,$id){
        $include_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		/* Uncomment below to include symbols */
		/* $include_chars .= "[{(!@#$%^/&*_+;?\:)}]"; */
		$length=10;
		$charLength = strlen($include_chars);
		$randomString = ''; 
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $include_chars [rand(0, $charLength - 1)];
		} 
		

		$kode = $id .'-'.$randomString;

        if (strpos($type, 'image') !== false) {
            $url = 'uploads/images/talent/'.$id.'/' . $fileName;
            $rowhead = $this->db->query(" SELECT * FROM agc_talent_media WHERE talent_id='$id' AND media_type = 'Headshot' AND media_mime='%image%' ");
            $tablehead = $rowhead->row_array();
            $tothead = $rowhead->num_rows();

            if($tothead > 0){
                $idMedia=$tablehead['media_id'];
                $hasil=$this->db->delete('agc_talent_media', array('media_id' => $idMedia));
            }

        }elseif (strpos($type, 'video') !== false) {
            $url = 'uploads/videos/talent/'.$id.'/' . $fileName;
            $rowhead = $this->db->query(" SELECT * FROM agc_talent_media WHERE talent_id='$id' AND media_type = 'Headshot' AND media_mime='%video%' ");
            $tablehead = $rowhead->row_array();
            $tothead = $rowhead->num_rows();

            if($tothead > 0){
                $idMedia=$tablehead['media_id'];
                $hasil=$this->db->delete('agc_talent_media', array('media_id' => $idMedia));
            }

        }
        
        $name = pathinfo($fileName, PATHINFO_FILENAME);
        
		
		$dataListing = array(
                            'media_id'	                => $kode,
                            'media_name' 		        => $name,
							'media_url' 	            => $url,
                            'media_type' 		        => 'Headshot',
                            'media_mime' 		        => $type,
							'media_order' 		        => '1',
                            'talent_id' 		        => $id
		); 
		//print_r($dataPengguna);
        $this->db->insert('agc_talent_media', $dataListing);

            
       
		
		$desc='Add Media Headshot '.$kode;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		
    }
    public function UploadVideoGalleryEmbed($embed,$title,$id){

        $query = $this->db->query("SELECT * FROM agc_talent_media WHERE media_type='Gallery' AND media_mime LIKE 'video%' AND talent_id='$id' ");
		$tot = $query->num_rows();
        $tot = $tot +1;
        

        $include_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		/* Uncomment below to include symbols */
		/* $include_chars .= "[{(!@#$%^/&*_+;?\:)}]"; */
		$length=10;
		$charLength = strlen($include_chars);
		$randomString = ''; 
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $include_chars [rand(0, $charLength - 1)];
		} 
		

        $kode = $id.'-'.$randomString;
        
        $dataListing = array(
            'media_id'	                => $kode,
            'media_name' 		        => $title,
            'media_url' 	            => $embed,
            'media_type' 		        => 'Gallery',
            'media_mime' 		        => 'video/embed',
            'media_order' 		        => $tot,
            'talent_id' 		        => $id
        ); 
        //print_r($dataPengguna);
        $this->db->insert('agc_talent_media', $dataListing);




        $desc='Add Media Gallery embed '.$kode;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
    public function UploadVideoGallery($fileName,$fileType,$id){
        $query = $this->db->query("SELECT * FROM agc_talent_media WHERE media_type='Gallery' AND media_mime LIKE 'video%' AND talent_id='$id' ");
		$tot = $query->num_rows();
        $tot = $tot +1;
        

        $include_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		/* Uncomment below to include symbols */
		/* $include_chars .= "[{(!@#$%^/&*_+;?\:)}]"; */
		$length=10;
		$charLength = strlen($include_chars);
		$randomString = ''; 
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $include_chars [rand(0, $charLength - 1)];
		} 
		

        $kode = $id .'-'.$randomString;

        $url = 'uploads/videos/talent/'.$id.'/' . $fileName;
        $name = pathinfo($fileName, PATHINFO_FILENAME);
        
        $dataListing = array(
            'media_id'	                => $kode,
            'media_name' 		        => $name,
            'media_url' 	            => $url,
            'media_type' 		        => 'Gallery',
            'media_mime' 		        => $fileType,
            'media_order' 		        => $tot,
            'talent_id' 		        => $id
        ); 
        //print_r($dataPengguna);
        $this->db->insert('agc_talent_media', $dataListing);




        $desc='Add Media gallery video '.$kode;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
    public function uploadPhotoGallery($fileName,$file_type,$TalentID){

		$date = date("Y-m-d H:i:s");
		
        $query = $this->db->query("SELECT * FROM agc_talent_media WHERE media_type='Gallery' AND media_mime LIKE 'image%' AND talent_id='$TalentID' ");
		$tot = $query->num_rows();
		$tot = $tot +1;
		
		
		
		//$kode = generate_key();

		$include_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		/* Uncomment below to include symbols */
		/* $include_chars .= "[{(!@#$%^/&*_+;?\:)}]"; */
		$length=10;
		$charLength = strlen($include_chars);
		$randomString = ''; 
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $include_chars [rand(0, $charLength - 1)];
		} 
		 

		$kode = $TalentID.'-'.$randomString;

        $url = 'uploads/images/talent/'.$TalentID.'/' . $fileName;
        $name = pathinfo($fileName, PATHINFO_FILENAME);
        
		
		$dataMedia = array(
            'media_id'	                => $kode,
            'media_name' 		        => $name,
            'media_url' 	            => $url,
            'media_type' 		        => 'Gallery',
            'media_mime' 		        => $file_type,
            'media_order' 		        => $tot,
            'talent_id' 		        => $TalentID
        ); 
		//print_r($dataPengguna);
        $this->db->insert('agc_talent_media', $dataMedia);

            
       
		
		$desc='Add Media Gallery Images '.$kode;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
		

    }
    public function uploadPhotoPolaroid($fileName,$file_type,$TalentID){

		$date = date("Y-m-d H:i:s");
		
        $query = $this->db->query("SELECT * FROM agc_talent_media WHERE media_type='Polaroid' AND media_mime LIKE 'image%' AND talent_id='$TalentID' ");
		$tot = $query->num_rows();
		$tot = $tot +1;
		
		
		
		//$kode = generate_key();

		$include_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		/* Uncomment below to include symbols */
		/* $include_chars .= "[{(!@#$%^/&*_+;?\:)}]"; */
		$length=10;
		$charLength = strlen($include_chars);
		$randomString = ''; 
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $include_chars [rand(0, $charLength - 1)];
		} 
		

		$kode = $TalentID .'-'.$randomString;

        $url = 'uploads/images/talent/'.$TalentID.'/' . $fileName;
        $name = pathinfo($fileName, PATHINFO_FILENAME);
        
		
		$dataMedia = array(
            'media_id'	                => $kode,
            'media_name' 		        => $name,
            'media_url' 	            => $url,
            'media_type' 		        => 'Polaroid',
            'media_mime' 		        => $file_type,
            'media_order' 		        => $tot,
            'talent_id' 		        => $TalentID
        ); 
		//print_r($dataPengguna);
        $this->db->insert('agc_talent_media', $dataMedia);

            
       
		
		$desc='Add Media Polaroid Images '.$kode;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
		

    }
    public function uploadPhotoCover($fileName,$file_type,$video,$TalentID){
        $date = date("Y-m-d H:i:s");
		
        $tot = 1;
		
		
		
		//$kode = generate_key();

		$include_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		/* Uncomment below to include symbols */
		/* $include_chars .= "[{(!@#$%^/&*_+;?\:)}]"; */
		$length=10;
		$charLength = strlen($include_chars);
		$randomString = ''; 
		
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $include_chars [rand(0, $charLength - 1)];
		} 
		

		$kode = $TalentID .'-'.$randomString;

        $url = 'uploads/images/talent/'.$TalentID.'/' . $fileName;
        $name = pathinfo($fileName, PATHINFO_FILENAME);
        
		
		$dataMedia = array(
            'media_id'	                => $kode,
            'media_name' 		        => $name,
            'media_url' 	            => $url,
            'media_type' 		        => 'Cover',
            'media_mime' 		        => $video,
            'media_order' 		        => $tot,
            'talent_id' 		        => $TalentID
        ); 
		//print_r($dataPengguna);
        $this->db->insert('agc_talent_media', $dataMedia);

            
       
		
		$desc='Add Media Cover Video '.$kode;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);

    }
}