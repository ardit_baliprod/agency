<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_catalog extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetCatalog(){
		//$pengguna = $this->db->get('agc_category');
		$this->db->select('*');
		$this->db->from('agc_catalog');
        $this->db->where('catalog_status !=', 2);
		$row = $this->db->get();
		return $row; 
	}
	public function GetCatalogActive(){
		//return $this->db->get_where('agc_category', array('category_status' => '1'));
		$this->db->select('*');
		$this->db->from('agc_catalog');
		$this->db->where('catalog_status', 1);
		$this->db->order_by("catalog_name");
		$row = $this->db->get();
		return $row;
    }
    public function GetUpdateCatalog($id){
        /*
        $this->db->select('agc_catalog.*,agc_category.category_name,agc_gender.gender_name');
        $this->db->from('agc_catalog');
        $this->db->join('agc_category', 'agc_catalog.category_id = agc_category.category_id');
        $this->db->join('agc_gender', 'agc_catalog.gender_id = agc_gender.gender_id');
        $this->db->where('agc_catalog.catalog_status', 1);
        $this->db->where('agc_catalog.catalog_id', $id);
        $query = $this->db->get();
        return $query;
        */
        $this->db->select('agc_catalog.*');
        $this->db->from('agc_catalog');
        $this->db->where('agc_catalog.catalog_status', 1);
        $this->db->where('agc_catalog.catalog_id', $id);
        $query = $this->db->get();
        return $query;
    }
    public function GetCatalogTags($id){
        $this->db->select('agc_catalog.*,agc_tags.*');
        $this->db->from('agc_catalog_tags');
        $this->db->join('agc_tags', 'agc_tags.id_tags = agc_catalog_tags.id_tags');
        $this->db->join('agc_catalog', 'agc_catalog.catalog_id = agc_catalog_tags.catalog_id');
        $this->db->where('agc_catalog.catalog_status', 1);
        $this->db->where('agc_catalog_tags.catalog_id', $id);
        $query = $this->db->get();
		return $query;
    }
    public function GetCatalogTalent($id){
        $this->db->select('agc_catalog.*,agc_catalog_talent.list_order,agc_talent.talent_fname,agc_talent.talent_lname,agc_talent.talent_nickname,agc_talent.talent_slug');
        $this->db->from('agc_catalog_talent');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_catalog_talent.talent_id');
        $this->db->join('agc_catalog', 'agc_catalog.catalog_id = agc_catalog_talent.catalog_id');
        $this->db->where('agc_catalog.catalog_status', 1);
        $this->db->where('agc_catalog_talent.catalog_id', $id);
        $this->db->order_by("agc_catalog_talent.list_order");
        $query = $this->db->get();
		return $query;
    }
    public function GetCatalogPublic($id){
        $this->db->select('agc_catalog.*,agc_category.category_name,agc_gender.gender_name');
        $this->db->from('agc_catalog');
        $this->db->join('agc_category', 'agc_catalog.category_id = agc_category.category_id');
        $this->db->join('agc_gender', 'agc_catalog.gender_id = agc_gender.gender_id');
        $this->db->where('agc_catalog.catalog_status', 1);
        $this->db->where('sha1(md5(agc_catalog.catalog_id))', $id);
        $query = $this->db->get();
		return $query;
    }
    public function create_catalog($name,$s,$gender,$category,$tags)
    {
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_catalog');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'CTL'. str_pad($jml, 4, '0', STR_PAD_LEFT);

        $dataCatalog = array(
            'catalog_id'	            => $kode,
            'catalog_name' 		        => $name,
            'catalog_slug' 	            => $s,
            'catalog_status' 		    => 1,
            'category_id' 		        => $category,
            'gender_id' 		        => $gender
        ); 
        //print_r($dataPengguna);
        $this->db->insert('agc_catalog', $dataCatalog);


        if(!empty($tags)){
            foreach ($tags as $cat)
            {
                //print "You are selected $names<br/>";
                $dataListing = array(
                            'catalog_id'	    => $kode,
                            'id_tags' 		    => $cat
                ); 
                //print_r($dataPengguna);
                $this->db->insert('agc_catalog_tags', $dataListing);
            }
        }


        $desc='Add Showcase '.$kode;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
        return $kode;
    }
    public function update_catalog($name,$s,$gender,$category,$tags,$id)
    {
        $date = date("Y-m-d H:i:s");
        
        $dataCatalog = array(
            'catalog_name' 		        => $name,
            'catalog_slug' 	            => $s,
            'catalog_status' 		    => 1,
            'category_id' 		        => $category,
            'gender_id' 		        => $gender
        ); 
        //print_r($dataPengguna);
        $this->db->where('catalog_id', $id);
        $this->db->update('agc_catalog', $dataCatalog);


        if(!empty($tags)){
            $this->db->delete('agc_catalog_tags', array('catalog_id' => $id));
            foreach ($tags as $cat)
            {
                //print "You are selected $names<br/>";
                $dataListing = array(
                            'catalog_id'	    => $id,
                            'id_tags' 		    => $cat
                ); 
                //print_r($dataPengguna);
                $this->db->insert('agc_catalog_tags', $dataListing);
            }
        }


        $desc='Update Showcase '.$id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
        return $id;
    }
    public function GetTalentOutside($id){
        $this->db->select('agc_package.*,agc_talent.talent_id,CONCAT(agc_talent.talent_fname," ",agc_talent.talent_lname)as "talentname",agc_talent.talent_slug,agc_talent.talent_nickname,agc_talent_media.media_url');
        $this->db->from('agc_package_talent');
        $this->db->join('agc_package', 'agc_package_talent.package_id = agc_package.package_id');
        $this->db->join('agc_talent', 'agc_package_talent.talent_id = agc_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_package_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_package.package_id', $id);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        $query = $this->db->get();
		return $query;
		
    }
    public function addTalent($talent,$id){
        $query = $this->db->query("SELECT * FROM agc_catalog_talent WHERE catalog_id='$id' ");
		$tot = $query->num_rows();
		$tot = $tot +1;

        $dataCatalog = array(
            'catalog_id' 		    => $id,
            'talent_id' 		    => $talent,
            'list_order' 		    => $tot
        ); 
        //print_r($dataPengguna);
        $this->db->insert('agc_catalog_talent', $dataCatalog);

        $desc='Add Talent '. $talent .' Catalog '.$id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
    public function listTalent($id)
    {
        $url = base_url();
        $this->db->select('agc_catalog.*,agc_talent.talent_id,CONCAT(agc_talent.talent_fname," ",agc_talent.talent_lname)as "talentname",agc_talent.talent_slug,agc_talent.talent_nickname,agc_talent_media.media_url,"'.$url.'" as "baseurl"');
        $this->db->from('agc_catalog_talent');
        $this->db->join('agc_catalog', 'agc_catalog_talent.catalog_id = agc_catalog.catalog_id');
        $this->db->join('agc_talent', 'agc_catalog_talent.talent_id = agc_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_catalog_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_catalog.catalog_id', $id);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        $this->db->order_by("agc_catalog_talent.list_order");
        $query = $this->db->get();
		return $query;
		
    }
    public function listTalentSlug($id)
    {
        $url = base_url();
        $this->db->select('agc_catalog.*,agc_talent.talent_id,CONCAT(agc_talent.talent_fname," ",agc_talent.talent_lname)as "talentname",agc_talent.talent_slug,agc_talent.talent_nickname,agc_talent_media.media_url,"'.$url.'" as "baseurl"');
        $this->db->from('agc_catalog_talent');
        $this->db->join('agc_catalog', 'agc_catalog_talent.catalog_id = agc_catalog.catalog_id');
        $this->db->join('agc_talent', 'agc_catalog_talent.talent_id = agc_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_catalog_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_catalog.catalog_slug', $id);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        $this->db->order_by("agc_catalog_talent.list_order");
        $query = $this->db->get();
		return $query;
		
    }
    public function listTalentEN($id)
    {
        $url = base_url();
        /*
        $this->db->select('agc_catalog.*,agc_talent.talent_id,CONCAT(agc_talent.talent_fname," ",agc_talent.talent_lname)as "talentname",agc_talent.talent_slug,agc_talent.talent_nickname,agc_talent_media.media_url,"'.$url.'" as "baseurl"');
        $this->db->from('agc_catalog_talent');
        $this->db->join('agc_catalog', 'agc_catalog_talent.catalog_id = agc_catalog.catalog_id');
        $this->db->join('agc_talent', 'agc_catalog_talent.talent_id = agc_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_catalog_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('sha1(md5(agc_catalog.catalog_id))', $id);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        $this->db->order_by("agc_catalog_talent.list_order");
        $query = $this->db->get();
        return $query;
        */
        $hsl=$this->db->query(" SELECT m.*,a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,k.media_url,k.media_type,k.media_mime,l.media_url AS urlvideo,l.media_type AS typevideo,l.media_mime AS mimevideo 
        FROM agc_catalog_talent j LEFT JOIN agc_talent a ON j.talent_id = a.talent_id
        LEFT JOIN agc_catalog m ON j.catalog_id = m.catalog_id
        LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN 
        ( 
        SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' 
        FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id 
        ) f ON j.talent_id = f.talent_id 
        LEFT JOIN 
        ( 
        SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' 
        FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id 
        ) i ON j.talent_id = i.talent_id 
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'image%'
        ) k ON j.talent_id = k.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'video%'
        ) l ON j.talent_id = l.talent_id
        WHERE sha1(md5(j.catalog_id)) = '$id' AND a.talent_status=1  
        Order BY j.list_order 
        ");
        return $hsl;
		
    }
    public function talentInCatalog($catalog_id,$talent_id){
        $this->db->select('agc_catalog.*,agc_talent.talent_nickname');
        $this->db->from('agc_catalog');
        $this->db->join('agc_catalog_talent', 'agc_catalog.catalog_id = agc_catalog_talent.catalog_id');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_catalog_talent.talent_id');
        $this->db->where('agc_catalog.catalog_id', $catalog_id);
        $this->db->where('agc_catalog_talent.talent_id', $talent_id);
        $query = $this->db->get();
		return $query; 
    }
    public function deleteTalentInCatalog($catalog_id,$talent_id)
    {
        $this->db->delete('agc_catalog_talent', array('catalog_id' => $catalog_id,'talent_id' => $talent_id));
        //$query = $this->db->query("DELETE FROM agc_catalog_talent WHERE catalog_id='$catalog_id' AND talent_id='$talent_id' ");
        
        $desc='Delete Talent '. $talent_id .' Catalog '.$catalog_id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
}