<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_ethnicity extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetEthnicity(){
		//$pengguna = $this->db->get('agc_ethnicity');
		$this->db->select('*');
		$this->db->from('agc_ethnicity');
        $this->db->where('ethnicity_status !=', 2);
		$row = $this->db->get();
		return $row;
	}
	public function GetEthnicityActive(){
		//return $this->db->get_where('agc_ethnicity', array('ethnicity_status' => '1'));
		$this->db->select('*');
		$this->db->from('agc_ethnicity');
		$this->db->where('ethnicity_status', 1);
		$this->db->order_by("ethnicity_id = 'TGS0005' DESC");
		$this->db->order_by("ethnicity_name");
		$row = $this->db->get();
		return $row;
	}
	public function ethnicity($ethnicity_id){
		return $this->db->get_where('agc_ethnicity', array('ethnicity_id' => $ethnicity_id));
    }
    public function create_ethnicity($name){
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_ethnicity');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'TGS'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		 
		
		$dataCategory = array(
                            'ethnicity_id' 		            => $kode,
							'ethnicity_name' 		        => $name,
							'ethnicity_status' 		        => 1,
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_ethnicity', $dataCategory);
		
		$desc='Add Ethnicity '.$kode;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
    }
    public function update_ethnicity($name,$id){
        $hsl=$this->db->query("UPDATE agc_ethnicity SET ethnicity_name='$name' WHERE ethnicity_id='$id' ");

        $desc='Update Ethnicity '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
	}
	public function delete_ethnicity($id){
    
		$hsl=$this->db->query("UPDATE agc_ethnicity SET ethnicity_status=2,ethnicity_name=CONCAT(ethnicity_name,'-delete') WHERE ethnicity_id='$id' ");
		
		
		$desc='Delete Ethnicity '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		//return $hsl;
	}
}