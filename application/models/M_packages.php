<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_packages extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetPackages(){ 
        $this->db->select('agc_package.*,agc_client.client_name');
        $this->db->from('agc_package');
        $this->db->join('agc_client', 'agc_client.client_id = agc_package.client_id');
        $query = $this->db->get();
		return $query;
		//$pengguna = $this->db->get('agc_package');
		//return $pengguna;
	}
	public function package($package_id){
        //return $this->db->get_where('agc_package', array('package_id' => $package_id));
        $this->db->select('agc_package.*,agc_client.client_name');
        $this->db->from('agc_package');
        $this->db->join('agc_client', 'agc_client.client_id = agc_package.client_id');
        $this->db->where('agc_package.package_id', $package_id);
        $query = $this->db->get();
		return $query;
    }
    public function packageEN($package_id){
        //return $this->db->get_where('agc_package', array('package_id' => $package_id));
        $this->db->select('agc_package.*,agc_client.client_name');
        $this->db->from('agc_package');
        $this->db->join('agc_client', 'agc_client.client_id = agc_package.client_id');
        $this->db->where('sha1(md5(agc_package.package_id))', $package_id);
        $query = $this->db->get();
		return $query;
    }
    public function talentInPackage($package_id,$talent_id){
        $this->db->select('agc_package.*,agc_talent.talent_nickname');
        $this->db->from('agc_package');
        $this->db->join('agc_package_talent', 'agc_package.package_id = agc_package_talent.package_id');
        $this->db->join('agc_talent', 'agc_talent.talent_id = agc_package_talent.talent_id');
        $this->db->where('agc_package.package_id', $package_id);
        $this->db->where('agc_package_talent.talent_id', $talent_id);
        $query = $this->db->get();
		return $query; 
    }
    public function GetPackagesTalent($id){
        $this->db->select('agc_package.*,agc_talent.talent_id,CONCAT(agc_talent.talent_fname," ",agc_talent.talent_lname)as "talentname",agc_talent.talent_slug,agc_talent.talent_nickname,agc_talent_media.media_url');
        $this->db->from('agc_package_talent');
        $this->db->join('agc_package', 'agc_package_talent.package_id = agc_package.package_id');
        $this->db->join('agc_talent', 'agc_package_talent.talent_id = agc_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_package_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_package.package_id', $id);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        $query = $this->db->get();
		return $query;
		
    }
    public function EmailPackageTalent($id = null)
    {
        $hsl=$this->db->query(" SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,k.media_url,k.media_type,k.media_mime,l.media_url AS urlvideo,l.media_type AS typevideo,l.media_mime AS mimevideo 
        FROM agc_talent a LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN 
        ( 
        SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' 
        FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id 
        ) f ON a.talent_id = f.talent_id 
        LEFT JOIN 
        ( 
        SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' 
        FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id 
        ) i ON a.talent_id = i.talent_id 
        LEFT JOIN agc_package_talent j ON a.talent_id = j.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'image%'
        ) k ON j.talent_id = k.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'video%'
        ) l ON j.talent_id = l.talent_id
        WHERE j.package_id = '$id' AND a.talent_status=1 
        ORDER BY j.list_order 
        ");
        return $hsl;
    }
    public function EmailPackageTalentEN($id = null)
    {
        $hsl=$this->db->query(" SELECT a.*,b.gender_name,c.ethnicity_name,f.categoryname,i.compelxionname,k.media_url,k.media_type,k.media_mime,l.media_url AS urlvideo,l.media_type AS typevideo,l.media_mime AS mimevideo 
        FROM agc_package_talent j LEFT JOIN agc_talent a ON j.talent_id = a.talent_id
        LEFT JOIN agc_gender b ON a.gender_id = b.gender_id 
        LEFT JOIN agc_ethnicity c ON a.ethnicity_id = c.ethnicity_id 
        LEFT JOIN 
        ( 
        SELECT d.*,GROUP_CONCAT(e.category_name SEPARATOR', ') AS 'categoryname' 
        FROM agc_talent_category d LEFT JOIN agc_category e ON d.category_id = e.category_id GROUP BY d.talent_id 
        ) f ON j.talent_id = f.talent_id 
        LEFT JOIN 
        ( 
        SELECT g.*,GROUP_CONCAT(h.compelxion_name SEPARATOR', ') AS 'compelxionname' 
        FROM agc_talent_compelxion g LEFT JOIN agc_compelxion h ON g.compelxion_id = h.compelxion_id GROUP BY g.talent_id 
        ) i ON j.talent_id = i.talent_id 
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'image%'
        ) k ON j.talent_id = k.talent_id
        LEFT JOIN 
        (
        SELECT * FROM agc_talent_media WHERE media_type = 'Headshot' AND media_mime LIKE 'video%'
        ) l ON j.talent_id = l.talent_id
        WHERE sha1(md5(j.package_id)) = '$id' AND a.talent_status=1
        ORDER BY j.list_order
          ");
        return $hsl;
    }
    public function listTalent($id)
    {
        $url = base_url();
        $this->db->select('agc_package.*,agc_talent.talent_id,CONCAT(agc_talent.talent_fname," ",agc_talent.talent_lname)as "talentname",agc_talent.talent_slug,agc_talent.talent_nickname,agc_talent_media.media_url,"'.$url.'" as "baseurl"');
        $this->db->from('agc_package_talent');
        $this->db->join('agc_package', 'agc_package_talent.package_id = agc_package.package_id');
        $this->db->join('agc_talent', 'agc_package_talent.talent_id = agc_talent.talent_id');
        $this->db->join('agc_talent_media', 'agc_package_talent.talent_id = agc_talent_media.talent_id');
        $this->db->where('agc_talent.talent_status', 1);
        $this->db->where('agc_package.package_id', $id);
        $this->db->where('agc_talent_media.media_type', 'Headshot');
        $this->db->like('agc_talent_media.media_mime', 'image', 'after');
        $this->db->order_by("agc_package_talent.list_order");
        $query = $this->db->get();
		return $query;
		
    }
    public function create_package($name,$customer,$desc)
    {
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_package');
		$jml = $query->num_rows();
		$jml++;
		$kode = 'PCK'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		
		$dataPackages = array(
                            'package_id' 		    => $kode,
                            'package_name' 		    => $name,
							'package_description' 	=> $desc,
                            'client_id' 		    => $customer,
                            'package_status' 	    => 1
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_package', $dataPackages);
		
		$desc='Add package '.$kode;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
		return $kode;
    }
    public function update_package($name,$customer,$desc,$id,$status){
        
        $dataPackages = array(
            'package_name' 		    => $name,
            'package_description' 	=> $desc,
            'client_id' 		    => $customer,
            'package_status' 	    => $status
        );

        $this->db->where('package_id', $id);
        $this->db->update('agc_package', $dataPackages);

		$desc='Update package '.$id;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);

    }
    public function addTalent($talent,$id){
        $query = $this->db->query("SELECT * FROM agc_package_talent WHERE package_id='$id' ");
		$tot = $query->num_rows();
        $tot = $tot +1;
        
        $dataPackages = array(
            'package_id' 		    => $id,
            'talent_id' 		    => $talent,
            'list_order' 		    => $tot
        ); 
        //print_r($dataPengguna);
        $this->db->insert('agc_package_talent', $dataPackages);

        $desc='Add Talent '. $talent .' Package '.$id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
    public function deleteTalentInPackage($package_id,$talent_id)
    {
        $this->db->delete('agc_package_talent', array('package_id' => $package_id,'talent_id' => $talent_id));
        
        $desc='Delete Talent '. $talent_id .' Package '.$package_id;
        $iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
    }
    
}