<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_compelxion extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetCompelxion(){
		//$pengguna = $this->db->get('agc_compelxion');
		$this->db->select('*');
		$this->db->from('agc_compelxion');
        $this->db->where('compelxion_status !=', 2);
		$row = $this->db->get();
		return $row;
	} 
	public function GetCompelxionActive(){
		//return $this->db->get_where('agc_compelxion', array('compelxion_status' => '1'));
		$this->db->select('*');
		$this->db->from('agc_compelxion');
		$this->db->where('compelxion_status', 1);
		$this->db->order_by("compelxion_name");
		$row = $this->db->get();
		return $row;
	}
	public function compelxion($compelxion_id){
		return $this->db->get_where('agc_compelxion', array('compelxion_id' => $compelxion_id));
    }
    public function create_compelxion($name){
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_compelxion');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'CPN'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		
		
		$dataCategory = array(
                            'compelxion_id' 		        => $kode,
							'compelxion_name' 		        => $name,
							'compelxion_status'				=> 1
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_compelxion', $dataCategory);
		
		$desc='Add Complexion '.$kode;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
    }
    public function update_compelxion($name,$id){
        $hsl=$this->db->query("UPDATE agc_compelxion SET compelxion_name='$name' WHERE compelxion_id='$id' ");

        $desc='Update Complexion '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
	}
	public function delete_compelxion($id){
    
		$hsl=$this->db->query("UPDATE agc_compelxion SET compelxion_status=2,compelxion_name=CONCAT(compelxion_name,'-delete') WHERE compelxion_id='$id' ");
		
		
		$desc='Delete Complexion '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		//return $hsl;
	}
}