<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_gender extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetGender(){
		//$row = $this->db->get('agc_gender');
		$this->db->select('*');
		$this->db->from('agc_gender');
        $this->db->where('gender_status !=', 2);
		$row = $this->db->get();
		return $row;
	}
	public function GetGenderActive(){
		//return $this->db->get_where('agc_gender', array('gender_status' => '1'));
		$this->db->select('*');
		$this->db->from('agc_gender');
		$this->db->where('gender_status', 1);
		$this->db->order_by("gender_name");
		$row = $this->db->get();
		return $row;
	}
	public function gender($gender_id){
		return $this->db->get_where('agc_gender', array('gender_id' => $gender_id));
    }
    public function create_gender($name,$s,$desc){
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_gender');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'GDR'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		
		
		$dataGender = array(
                            'gender_id' 		        => $kode,
                            'gender_name' 		        => $name,
							'gender_slug' 	            => $s,
							'gender_desc' 		        => $desc,
							'gender_status' 		    => 1
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_gender', $dataGender);
		
		$desc='Add Gender '.$kode;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		
    }
    public function update_gender($name,$s,$desc,$status,$id){
        $hsl=$this->db->query("UPDATE agc_gender SET gender_name='$name',gender_slug='$s',gender_desc='$desc',gender_status='$status' WHERE gender_id='$id' ");

        $desc='Update Gender '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
	}
	public function delete_gender($id){
    
		$hsl=$this->db->query("UPDATE agc_gender SET gender_status=2,gender_slug=CONCAT(gender_slug,'-delete') WHERE gender_id='$id' ");
		
		
		$desc='Delete gender '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		//return $hsl;
	}
    
}