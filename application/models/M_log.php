<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_log extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	
    function add($u,$ket){
		date_default_timezone_set("Asia/Singapore");
		$date = date("Y-m-d H:i:s");
		$dataLog = array(   'id_log' => null,  
						    'username' => $u,
							'description' => $ket,
							'date_in' => $date
						);
		//print_r($dataPengguna);
		$hsl=$this->db->insert('agc_log', $dataLog);
		return $hsl;
	} 

	public function GetHistory($start=null,$end=null){
		if($start==null){
			$start='00000-00-00';
		}
		if($end==null){
			$end='00000-00-00';
		}
		$this->db->select('agc_log.*,agc_user.username,agc_userdetail.fname,agc_userdetail.lname'); 
		$this->db->from('agc_log');
		$this->db->join('agc_userdetail', 'agc_log.username = agc_userdetail.id_user');
		$this->db->join('agc_user', 'agc_log.username = agc_user.id_user');
		$this->db->where('agc_log.date_in >=', $start);
		$this->db->where('agc_log.date_in <=', $end);
        $query = $this->db->get();
		return $query;
    }
}

/* End of file M_history.php */
/* Location: ./application/models/m_login.php */