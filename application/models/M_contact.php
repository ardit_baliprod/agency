<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_contact extends CI_Model {

	public $variable;

	public function __construct()
	{
        parent::__construct();
        $this->load->model('M_log');
		
	}
	
	function ambil_detail($u){
		$hsl=$this->db->query("SELECT * FROM agc_contact where contact_id='$u' and 1=1 ");
		return $hsl;
	}
	public function GetContact(){
		$category = $this->db->get('agc_contact');
		return $category;
	}
	public function GetContactActive(){
		$status=1;
		return $this->db->get_where('agc_contact', array('status' => $status));
	}
	public function Contact($client_id){
        
        $this->db->select('agc_contact.*');
        $this->db->from('agc_contact');
        $this->db->join('agc_client', 'agc_contact.client_id = agc_client.client_id');
        $this->db->where('agc_contact.client_id', $client_id);
        $query = $this->db->get();
		return $query;
	} 
	public function ContactActive($client_id){
        $status=1;
        $this->db->select('agc_contact.*');
        $this->db->from('agc_contact');
        $this->db->join('agc_client', 'agc_contact.client_id = agc_client.client_id');
		$this->db->where('agc_contact.client_id', $client_id);
		$this->db->where('agc_contact.status', $status);
        $query = $this->db->get();
		return $query;
	} 
	public function simpan_contact($fname,$lname,$email,$phone,$position,$title,$primary,$clientid){

        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_contact');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'CTC'. str_pad($jml, 4, '0', STR_PAD_LEFT);
        $check=0;
        if($primary=='true'){
            $check=1;
        }
		
		$dataContact = array(
                            'contact_id' 		        => $kode,
                            'contact_fname' 		    => $fname,
							'contact_lname' 	        => $lname,
							'contact_email' 		    => $email,
							'contact_phone' 			=> $phone,
							'contact_title' 			=> $title,
							'contact_position' 			=> $position,
							'is_primary' 		        => $check,
							'datecreated' 		        => $date,
                            'status' 	                => 1,
                            'client_id' 	            => $clientid
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_contact', $dataContact);
		
		$desc='Add contact '.$kode;
		$iduser=$this->session->userdata('user');
		$this->M_log->add($iduser,$desc);
		

	}
	public function update_contact($fname,$lname,$email,$phone,$position,$title,$primary,$clientid,$status,$contactid){
		$check=0;
        if($primary=='true'){
            $check=1;
        }
		$hsl=$this->db->query("UPDATE agc_contact SET contact_fname='$fname',contact_lname='$lname',contact_email='$email',contact_email='$email',contact_phone='$phone',contact_title='$title',contact_position='$position',is_primary='$check',status='$status' WHERE contact_id='$contactid' ");
				
		$desc='Update contact '.$contactid;
		$iduser=$this->session->userdata('user');
		$this->M_log->add($iduser,$desc);
		return $hsl;
	}
	
}

/* End of file M_user.php */
/* Location: ./application/models/m_login.php */