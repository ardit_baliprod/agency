<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_category extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetCategory(){
		//$pengguna = $this->db->get('agc_category');
		$this->db->select('*');
		$this->db->from('agc_category');
        $this->db->where('category_status !=', 2);
		$row = $this->db->get();
		return $row; 
	}
	public function GetCategoryActive(){
		//return $this->db->get_where('agc_category', array('category_status' => '1'));
		$this->db->select('*');
		$this->db->from('agc_category');
		$this->db->where('category_status', 1);
		$this->db->order_by("category_name");
		$row = $this->db->get();
		return $row;
	}
	public function category($category_id){
		return $this->db->get_where('agc_category', array('category_id' => $category_id));
    }
    public function create_category($name,$s,$desc){
        $date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_category');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'CGY'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		
		
		$dataCategory = array(
                            'category_id' 		        => $kode,
                            'category_name' 		    => $name,
							'category_slug' 	        => $s,
							'category_desc' 		    => $desc,
							'category_status' 		    => 1
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_category', $dataCategory);
		
		$desc='Add Category '.$kode;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
    }
    public function update_category($name,$s,$desc,$status,$id){
        $hsl=$this->db->query("UPDATE agc_category SET category_name='$name',category_slug='$s',category_desc='$desc',category_status='$status' WHERE category_id='$id' ");

        $desc='Update Category '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
	}
	public function delete_category($id){
    
		$hsl=$this->db->query("UPDATE agc_category SET category_status=2,category_slug=CONCAT(category_slug,'-delete') WHERE category_id='$id' ");
		
		
		$desc='Delete Category '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		//return $hsl;
	}
}
