<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_client extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
		
	}
	public function GetClient(){
		$this->db->select('agc_client.*,tblcountries.short_name');
		$this->db->from('agc_client');
		$this->db->join('tblcountries', 'agc_client.client_country = tblcountries.country_id ');
		$query = $this->db->get();
		return $query;
	}
	public function GetClientActive(){
		$this->db->select('agc_client.*,tblcountries.short_name');
		$this->db->from('agc_client');
		$this->db->join('tblcountries', 'agc_client.client_country = tblcountries.country_id ');
		$this->db->where('agc_client.client_status', '1');
		$query = $this->db->get();
		return $query;
	}
	public function client($client_id){
        $this->db->select('agc_client.*,tblcountries.short_name');
		$this->db->from('agc_client');
        $this->db->join('tblcountries', 'agc_client.client_country = tblcountries.country_id ');
        $this->db->where('agc_client.client_id', $client_id);
		$query = $this->db->get();
		return $query;
	} 
	public function add_customer($name,$phone,$address,$city,$state,$zip,$website,$country){
		$date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_client');
		$jml = $query->num_rows();
		$jml++;
		$kode = 'CLT'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		
		$dataCustomer = array(
                            'client_id' 		    => $kode,
                            'client_name' 		    => $name,
							'client_phone' 	        => $phone,
							'client_address' 		=> $address,
							'client_city' 			=> $city,
							'client_state' 			=> $state,
							'client_zip' 			=> $zip,
							'client_country' 		=> $country,
							'client_website' 		=> $website,
                            'datecreated' 	        => $date,
                            'client_status' 	    => 1
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_client', $dataCustomer);
		
		$desc='Add client '.$kode;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);
	}
	public function update_customer($name,$phone,$address,$city,$state,$zip,$website,$country,$status,$id)
	{
		$dataCustomer = array(
			'client_name' 		    => $name,
			'client_phone' 	        => $phone,
			'client_address' 		=> $address,
			'client_city' 			=> $city,
			'client_state' 			=> $state,
			'client_zip' 			=> $zip,
			'client_country' 		=> $country,
			'client_website' 		=> $website,
			'client_status' 	    => $status
		);

		$this->db->where('client_id', $id);
        $this->db->update('agc_client', $dataCustomer);

		$desc='Update client '.$kode;
		$iduser=$this->session->userdata('iduser');
        $this->M_log->add($iduser,$desc);

	}
	public function ContactActive($customer)
	{
		
	}

}