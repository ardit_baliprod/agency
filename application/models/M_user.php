<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_log');
	}
	public function GetKategori(){
		$pengguna = $this->db->get('tbl_kategori');
		return $pengguna;
	}
	public function login($user_name){
		//return $this->db->get_where('agc_user', array('username' => $user_name,'type' => 'administrator'));
		$this->db->select('agc_user.*,agc_userdetail.fname,agc_userdetail.lname,agc_userdetail.position,agc_userdetail.phone,agc_userdetail.last_login,agc_userdetail.login');
		$this->db->from('agc_user');
		$this->db->join('agc_userdetail', 'agc_user.id_user = agc_userdetail.id_user ');
		$this->db->where('agc_user.status', '1');
		$this->db->where('agc_user.type', 'administrator');
		$this->db->where('agc_user.username', $user_name);
		$query = $this->db->get();
		return $query;
	}
	public function GetUser(){
		$this->db->select('agc_user.*,agc_userdetail.fname,agc_userdetail.lname,agc_userdetail.position,agc_userdetail.phone,agc_userdetail.last_login');
		$this->db->from('agc_user');
		$this->db->join('agc_userdetail', 'agc_user.id_user = agc_userdetail.id_user ');
		$this->db->where('agc_user.status', '1');
		$query = $this->db->get();
		return $query;
	  }
	  public function User($id){
		$this->db->select('agc_user.*,agc_userdetail.fname,agc_userdetail.lname,agc_userdetail.position,agc_userdetail.phone,agc_userdetail.last_login');
		$this->db->from('agc_user');
		$this->db->join('agc_userdetail', 'agc_user.id_user = agc_userdetail.id_user ');
		$this->db->where('agc_user.status', '1');
		$this->db->where('agc_user.id_user', $id);
		$query = $this->db->get();
		return $query;
	  }
	  public function create_user($username,$fname,$lname,$position,$telepon,$email,$pass){
		date_default_timezone_set("Asia/Singapore");
		$date = date("Y-m-d H:i:s");
        $query = $this->db->query('SELECT * FROM agc_user');
		$jml = $query->num_rows();
		$jml++;
        $kode = 'USR'. str_pad($jml, 4, '0', STR_PAD_LEFT);
		$pass_en = sha1(md5($pass));

		$dataUser = array(
							'id_user' 			=> $kode,
							'username' 			=> $username,
							'email' 			=> $email,
							'password' 			=> $pass_en,
							'type' 				=> 'administrator',
							'status' 			=> 1
						); 
		//print_r($dataPengguna);
		$date = date("Y-m-d H:i:s");
		$login=0;
		$this->db->insert('agc_user', $dataUser);
		$dataUser = array(
							'id_user' 			=> $kode,
							'fname' 			=> $fname,
							'lname' 			=> $lname,
							'position' 			=> $position,
							'phone' 			=> $telepon,
							'date_in' 			=> $date,
							'login' 			=> $login,
							'last_login' 		=> null,
							'last_login_ip' 	=> null
		); 
		//print_r($dataPengguna);
		$this->db->insert('agc_userdetail', $dataUser);
		
		$desc='Add User '.$username.' '.$kode;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);
		

	}
	public function check_user($username,$email){

		$query = $this->db->query("SELECT * FROM agc_user WHERE username='$username' OR email='$email' ");

		$tot = $query->num_rows();

		if($tot == 0){
			return true;
		}else{
			return false;
		}

	}
	public function check_user_update($u,$email,$id){

		$query = $this->db->query("SELECT * FROM agc_user WHERE (username='$u' OR email='$email') AND id_user !='$id' ");

		$tot = $query->num_rows();

		$function = false;

		if($tot == 0){
			$function = true;
			return $function;
		}else{
			$function = false;
			return $function;
		}

	}
	public function update_user($username,$fname,$lname,$position,$telepon,$email,$pass,$status,$id){
		if($pass == ''){
			$hsl=$this->db->query("UPDATE agc_user SET email='$email',status=$status,username='$username' WHERE id_user='$id' ");
			$hsl2=$this->db->query("UPDATE agc_userdetail SET fname='$fname',lname='$lname',position='$position',phone='$telepon' WHERE id_user='$id' ");
		}
		else{
			$pass_en = sha1(md5($pass));
			
			$hsl=$this->db->query("UPDATE agc_user SET email='$email',status=$status,password='$pass_en',username='$username' WHERE id_user='$id' ");
			$hsl2=$this->db->query("UPDATE agc_userdetail SET fname='$fname',lname='$lname',position='$position',phone='$telepon' WHERE id_user='$id' ");
		}
		
		$desc='Update User '.$id;
		$iduser=$this->session->userdata('iduser');
		$this->M_log->add($iduser,$desc);

		return $hsl;
	}
}

/* End of file m_pengguna.php */
/* Location: ./application/models/m_user.php */