-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2020 at 04:55 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agency`
--

-- --------------------------------------------------------

--
-- Table structure for table `agc_user`
--

CREATE TABLE `agc_user` (
  `id_user` varchar(10) NOT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agc_user`
--

INSERT INTO `agc_user` (`id_user`, `username`, `password`, `type`, `status`) VALUES
('USR0001', 'ardit-baliprod', 'bma123', 'administrator', 1);

-- --------------------------------------------------------

--
-- Table structure for table `agc_userdetail`
--

CREATE TABLE `agc_userdetail` (
  `id_user` varchar(100) NOT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `date_in` datetime DEFAULT NULL,
  `login` int(100) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agc_userdetail`
--

INSERT INTO `agc_userdetail` (`id_user`, `fname`, `lname`, `position`, `phone`, `date_in`, `login`, `last_login`, `last_login_ip`) VALUES
('USR0001', 'ardit', 'namaste', 'administrator', '054584165', '2020-07-06 18:42:10', 1, '2020-07-06 18:42:19', ' ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agc_user`
--
ALTER TABLE `agc_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `agc_userdetail`
--
ALTER TABLE `agc_userdetail`
  ADD PRIMARY KEY (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
